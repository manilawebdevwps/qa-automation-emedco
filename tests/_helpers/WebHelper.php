<?php
namespace Codeception\Module;
use \WebGuy;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class WebHelper extends \Codeception\Module
{
    /*function hasPopUp($page)
    {
        try {
            $this->getModule('WebDriver')->see($page, 'a');
        } catch (\PHPUnit_Framework_AssertionFailedError $f) {
            return false;
        }
        return true;
    }*/


    public function seeMyVar($var){                 /*to see the values of array */
        $this->debug($var);
    }


    public function waitForUserInput(){
        if(trim(fgets(fopen("php://stdin","r")))!= chr(13)) return;
    }


    public function seeLoginLink(){
        try {
            $this->getModule('WebDriver')->click('Login');
        }catch (\PHPUnit_Framework_AssertionFailedError $e){
            $this->getModule('WebDriver')->click('Logout');
        }
        return true;
    }

    public function canStillSeeInPageSource($e){
        try{
            $this->getModule('WebDriver')->seeInPageSource($e);
        }catch (\PHPUnit_Framework_AssertionFailedError $e) {
            return false;
        }
        return true;
    }

    /*
    //11/12/15
    public function shippingAdd($buyer){

        $n=8;
        for ($i=0; $i < $n ; $i++)       {
        try {
            $I = $this->getModule('WebDriver');
            $I->expectTo('see TRUE, #'.$i.'');
            $I->canSee('Lilybeth R.','.en_US.ng-scope>a');
        }catch (\PHPUnit_Framework_AssertionFailedError $e){
            $I = $this->getModule('WebDriver');
            $I->expectTo('see FALSE, #'.$i.'');
            $I->canSee('Lilybeth R.','li.en_US:nth-child(2)');
        }

            $I->click('.next');
        }
            return true;
    }*/


    public function getArrayFromDB($table, $column, $criteria = array()){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);


        //$query = 'select * from category1';
        $query.=' ORDER BY url ASC';
        $query.=' LIMIT 4442';
        $query.=' OFFSET 558';


        #start now with 1 to 337 : ok
        #next: 338 -> 558 : ok

        $dbh->debugSection('Query', $query, json_encode($criteria));
        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }



    public function getArrayFromDBRandom($table, $column, $criteria = array(), $count){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);
        //$query = 'select * from category1';
        if($count>0){
            $query.='ORDER BY RAND() LIMIT '.$count;
        }
        $dbh->debugSection('Query', $query, json_encode($criteria));

        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }

    public function getArrayFromDBOrderByWithLimit($table, $column, $criteria = array(), $order, $count=0){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);
        //$query = 'select * from category1';
        $query.=' ORDER BY '.$order;
        echo $query;
        if($count>0){
            $query.=' LIMIT '.$count;
        }
        //echo $query;
        $dbh->debugSection('Query', $query, json_encode($criteria));

        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }

     //newly added
    //SELECT * FROM `category2` ORDER BY RAND() LIMIT 0,10
    public function getArrayFromDBInRandom($table, $column, $criteria = array(), $order, $count=0){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);
        $query.=' ORDER BY RAND() LIMIT 0,10';
        $dbh->debugSection('Query', $query, json_encode($criteria));
        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");
        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }


    public function useIMAP($formValue){

        $host = '{imap.gmail.com:993/imap/ssl}INBOX';
        $username = 'bradyqatest@gmail.com';
        $password = 'webdevqa';


        //$message=array();
        $message='';

        /* try to connect */
        $inbox = imap_open($host,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

        /* grab emails */
        $emails = imap_search($inbox,'UNSEEN FROM Emedco');    //checking only the unread
        #$emails = imap_search($inbox,'ALL');
        #$emails = imap_search($inbox,'SUBJECT "Quote request" ');

        ///* if emails are returned, cycle through each...
        if($emails) {

            // /* put the newest emails on top
            rsort($emails);

            ///* for every email...
            foreach($emails as $email_number) {
                $message = quoted_printable_decode(imap_body($inbox,$email_number));
                //echo $message;
                $this->assertContains($formValue,$message);
            }
            //echo $output;
        }
        /* close the connection */
        imap_close($inbox);
        return $message;
    }


    public function changeBaseURL($url){
        $this->getModule('WebDriver')->_reconfigure(array('url' => $url));
        //$I->changeBaseURL('http://www.emedco.com/');
    }


    function truncate($url)
    {
        $text =  substr($url,strrpos($url,".com/")+4,strlen($url));
        return $text;
    }

    //Search Results for ghs ΓÇö 33 articles found
    function captureText($copy)
    {   //substr(string,start,length);
        //strrpos(string,find,start)
        $text = explode('—',$copy);
        //$text =  substr($copy,strrpos($copy,$startTxt),);
        //$text =  ;
        return $text[1];
    }


    public function LogResult($testType,$num,$result,$dateTimeTested,$dateTested){
              //$I->LogResult('',$n,$text,'','');
     /* 06-04-2015 05:09
        Homepage         > http://emedco.zeondemo.com/ : 8.34s
        06-04-2015 05:13
        Homepage         > http://www.emedco.com/ : 4.13s
        Category Level 1 > http://www.emedco.com/signs.html : 4.33s
        Category Level 2 > http://www.emedco.com/tape-barricades-cones/tapes.html : 4.28s
        Category Level 3 > http://www.emedco.com/labels/inventory-control-warehouse-id/color-coded.html : 3.98s
        Product page     > http://www.emedco.com/wash-hands-signs-32399br.html : 8.19s
        Cart page        > http://www.emedco.com/checkout/cart/ : 3.16s
     */
        try {
             $fp = fopen('tests\_data\LogFile_'.$testType.'_'.$dateTested.'.txt', 'a+');

             if ($dateTimeTested == ' '){
                #fwrite($fp, "\r\n");
                #fwrite($fp, $dateTimeTested.''."\r\n");
                //fwrite($fp, $testType." : ".$result.''."\r\n"); /* 1 : best/ways_to_break_padlocks  */
                fwrite($fp, $num." : ".$result.''."\r\n"); /* 1 : best/ways_to_break_padlocks  */
                //$I->LogResult('Category Level 1 > '.self::$link_Homepage.self::$link_Category1, $pageResult, '', $dateTested);
                fclose($fp);
            }else {
                //fwrite($fp, "\r\n");
                //fwrite($fp, $dateTimeTested.''."\r\n");  --->>>NA for bestPages
                fwrite($fp, $num." : ".$result.''."\r\n");
                //fwrite($fp, $testType." : here1".$result.''."\r\n");
                //Category Level 1 > http://www.emedco.com/signs.html : 4.33s  //pagetest
                //$result - $testType --> LogResult($url,$n)
                //#1 - best/yellow_tag_fasteners
                fclose($fp);
            }       //}
            //die();
        }catch (\PHPUnit_Framework_AssertionFailedError $e){
            $fp = fopen('tests\_data\TestLog_'.$testType.'_'.$dateTested.'.txt', 'a+');

            if ($dateTimeTested == ' '){
                #fwrite($fp, "\r\n");
                #fwrite($fp, $dateTimeTested.''."\r\n");
                fwrite($fp, $testType." : ".$result.''."\r\n");
                //Category Level 1 > http://www.emedco.com/signs.html : 4.33s
                fclose($fp);
            }else {
                //fwrite($fp, "\r\n");
                fwrite($fp, $dateTimeTested.''."\r\n");
                fwrite($fp, $testType." : ".$result.''."\r\n");
                //Category Level 1 > http://www.emedco.com/signs.html : 4.33s
                fclose($fp);
            }       //}

        }//try & catch
        return true;
    }

    public function captureCCType()
    { //CCtype
        $select_cc_type = array();
        $select_cc_type[] = array('American Express', '378282246310005', '1234');
        $select_cc_type[] = array('Discover', '6011111111111117', '123');
        $select_cc_type[] = array('Visa', '4111111111111111', '123');
        $select_cc_type[] = array('Mastercard', '5105105105105100', '123');
        //return $text[1];
        return $select_cc_type;
    }

    public function captureCCMonth()
    { //CCMonth
        $select_expMonth= array();
        //$select_expMonth[] = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $select_expMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        return $select_expMonth;
    }


    public function captureCCYear()
    {  //CC Year
        $select_expYear = array();
        $StartDate = date("Y");
        for ($i=0;$i<10;$i++) {
            $n = $i+1;
            $nextYears = date("Y", strtotime(date("Y", strtotime($StartDate)) . " +".$n." year"));
            $select_expYear[] = $nextYears;
        }
        return $select_expYear;
    }





}