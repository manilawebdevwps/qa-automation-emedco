<?php
//namespace regression;

class ProductPage
{

    //updated
    // include url of current page
    //for update of product page - 3/19/15(uplaod again)
    public static $URL = '';

    // Case 1
    static $URL_truckShipmentOnly = '/steel-approach-plate-for-dock-levelers-dckla.html';   //212.49
    static $product_truckShipmentOnly = 'Steel Approach Plate for Dock Levelers';
    static $productSKU_truckShipmentOnly = 'DCKLA';

    //168.95 + 19.45 + 26.38 (truck shipment + ground + tax)
    static $cartValue_price_truckRate = '168.95';

    static $valueTax = '26.38';
    static $valueGroundShip = '64.95';
    static $fieldProductPrice = '.price.firstChild';
    //static $fieldCartPrice = '#checkout-review-table.data-table tfoot tr.first td.a-right.last span.price';
    static $cartField_productPrice = '#checkout-review-table.data-table tfoot tr.first td.a-right.last span.price';
    static $cartField_shipPrice = '#checkout-review-table.data-table tfoot tr td.a-right.last span.price';
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Additional Product - Case 2
    static $URL_truckShipmentAndGround = '/pre-printed-lanyard-ppl31.html';          //3.59
    static $product_truckShipmentAndGround = 'Pre-Printed Lanyard';
    static $productSKU_truckShipmentAndGround = 'PPL31';
    static $cartValue_price_truckAndGroundRate = '233.90'; //168.95+64.95

    static $cartValue_useCase2_truckShipPrice = '233.90';     //168.95+64.95(ground cost)
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Additional Product - Case 3
    static $URL_truckShipmentAndGroundFree  = '/3-channel-cable-protector-v1029.html';          //363.79
    static $product_truckShipmentAndGroundFree = '3-Channel Cable Protector';
    static $productSKU_truckShipmentAndGroundFree = 'V1029';
    //static $cartValue_price_truckRate = '363.79';
    static $cartValue_price_truckAndGroundFreeRate = '168.95';     //168.95+free

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    static $validProduct_quickOrder = '68000';
    static $invalidProduct_quickOrder = '1111111';
    static $productPrice_quickOrder = '$42.70';


    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    static $validProduct_search = 'DT81369';
    static $validProductName_search = 'Accident Prevention Tags';
    static $validProductNameComp_search = 'Accident Prevention Tags - Danger Defective';
    static $URL_validProduct_search = '/accident-prevention-tags-danger-defective-dt81369.html';
    static $invalidProduct_search = '123abc';

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //public static $simpleProduct = '/western-enterprises-pipe-thread-hex-nipples-hh464.html';
    //public static $simpleProductName = 'Western Enterprises - Pipe Thread Hex Nipples';
    public static $simpleProductImage = 'http://emedco.zeondemo.com/media/catalog/product/accident-prevention-tags-danger-defective-dt81369-ba.jpg';
    public static $fiveStar = 'src="http://emedco.ugc.bazaarvoice.com/static/6420-en_us/translucent.gif"';
    public static $firstReview = 'Write the first review';
    public static $zoomImage = 'zoom-prod-img t-center';
    //public static $simpleProductSKU = 'HH464';
    public static $PLAProduct = '/pla/?sku=RE326';

    ////////////////////////  SITEWIDE FREE SHIPPING - Php100.00 ///////////////////////////////////////////
    public static $simpleProduct = '/ansi-lockout-tags-danger-do-not-start-work-on-machine-dt81421.html';
    public static $simpleProductName = 'ANSI Lockout Tags - Danger Do Not Start Work On Machine';
    public static $simpleProductSKU = 'DT81421';




    /**
     *
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }


}
