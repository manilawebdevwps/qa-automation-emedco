<?php

class LoginPage
{
    // include url of current page

    #naming convention

    #$<Stg/Live><admin/prod><Field/Value>_username

    //admin Staging - https://emedconewstg.perficientdcsdemo.com/zpanel
    static $urlStg_admin = '/zpanel';
    static $stgAdminField_username = '#username';
    static $stgAdminField_password = '#login';
    static $stgAdminValue_username = 'lranada';
    static $stgAdminValue_password = 'lilybeth6';

    static $URL = '/customer/account/login/';

    //Fields
    static $checkoutField_username = '#login-email';
    static $checkoutField_password= '#login-password';
    //value
    static $checkoutValue_username = 'lilybeth_ranada@bradycorp.com';
    static $checkoutValue_password = 'lilybeth';
    static $checkoutButton_continue = '.button.btn-login.firstChild';


    /* Checkout */
    static $URL_checkout = '/checkout/onepage/';
    static $checkoutMethod_guest = '#login\:guest';
    static $checkoutMethodBtn_guest = '.button.btn-login.firstChild';
    //new login page
    static $checkOutValue_newCustomerEmail = 'qatest@bradycorp.com';
    static $checkOutValue_newCustomerPwd = 'qatest';
    //value
    static $checkOutElement_newCustomerEmail = '#email_address';
    static $checkOutElement_newCustomerPwd = '#password';
    static $checkOutElement_newCustomerPwdConf = '#confirmation';


    //amazon credentials
    static $amazonField_username = '//input[@id="ap_email"]';
    static $amazonField_password= '//input[@id="ap_password"]';
    //static $amazonBtn_signIn = 'button.button-text.signin-button-text';
    //static $amazonBtn_signIn = '.button-text';
    static $amazonBtn_signIn = './/*[@id="signInSubmit"]/span/button';

    //value
    static $amazonVal_username = 'payment-test@amazon.com';
    static $amazonVal_password = 'test123';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */
     public static function route($param)
     {
        return static::$URL.$param;
     }


}