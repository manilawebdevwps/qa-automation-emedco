<?php

class AdminPage
{


    //navigation

    #nav > li:nth-child(1) > a > span
    #nav > li:nth-child(2) > a > span
    #nav > li:nth-child(3) > a > span
    #nav > li:nth-child(4) > a > span
    #nav > li:nth-child(5) > a > span


    static $stgAdminField_dashboard = '';
    static $stgAdminField_sale = '';
    static $stgAdminField_catalog = '';
    static $stgAdminField_mobile = '';
    static $stgAdminField_customer = '';
    static $stgAdminField_promotions = '#nav > li:nth-child(6) > a > span';
    static $stgAdminField_catPriceRule = '#nav > li:nth-child(6) > ul > li:nth-child(1) > a > span';
    static $stgAdminField_addNewRule = 'div.content-header:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > button:nth-child(2)';
    static $stgAdminField_addNewRuleConditions = '#promo_catalog_edit_tabs_conditions_section > span:nth-child(1)';
    static $stgAdminField_ruleParam = '.rule-param-add';


    public static $urlStg_admin = '/zpanel';
    public static $stgAdminField_username = '#username';
    public static $stgAdminField_password = '#login';
    public static $stgAdminValue_username = 'lranada';
    public static $stgAdminValue_password = 'lilybeth1';

    //public static $stgAdminField_system = 'li.level0:nth-child(10)';
    public static $stgAdminField_system = 'html/body/div[1]/div[1]/div[3]/ul/li[10]/a/span';
    public static $stgAdminField_configuration = 'html/body/div[1]/div[1]/div[3]/ul/li[10]/ul/li[19]/a/span';
    public static $stgAdminField_shippingMethod = 'html/body/div[1]/div[3]/div/div/div[1]/ul/li[11]/dl/dd[8]/a/span'; //shipping methods
    public static $stgAdminField_auctionmaidMatrixRates = 'html/body/div[1]/div[3]/div/div/div[2]/div/form/div[2]/div[2]/a';

    public static $stgAdminField_freeShipping_withMinOrderAmtActive = 'groups.[matrixrate].[fields].[enable_free_shipping_threshold].[value]';
    public static $stgAdminValue_freeShipping_withMinOrderAmtActive = 'Enable';

    public static $stgAdminField_freeShipping_amount = '#carriers_freeshipping_free_shipping_subtotal';
    public static $stgAdminField_freeShipping_button = 'html/body/div[1]/div[3]/div/div/div[2]/div/div[2]/table/tbody/tr/td[2]/button';










    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */



    public static function route($param)
    {
        return static::$URL.$param;
    }


}