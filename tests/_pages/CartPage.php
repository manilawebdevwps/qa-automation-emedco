<?php

class CartPage
{
    //Product Details
    static $numOfItems_Element = 'h1';
    static $numOfItems_value = 'Item in your cart';
    static $prodDetailsElement_title = '#shopping-cart-table.cart-table';
    static $prodDetailsValue_title = 'Product Details';
    static $prodDetailsElement_table = 'table#shopping-cart-table';
    static $prodDetailsValue_qty = 'QTY';
    static $prodDetailsValue_price = 'Unit Price';
    static $prodDetailsValue_total = 'Total';
    static $cartSummElement_table = '.cart-summary';
    static $cartSummValue_title = 'Summary';
    static $cartSummValue_prodTotal = 'Product Total';
    static $cartSummValue_estTax = 'Estimate Tax';
    static $cartSummValue_estShip = 'Estimate Shipping';
    static $orderTotalValue_title = 'Order Total';
    static $offCodeValue = 'Offer Code';
    static $offCodeValue_text = 'Promotional offers cannot be combined with other offers or discounts, including those in a sales quote';


    // include url of current page
    public static $URL_checkout = '/checkout/onepage/';

    //Product Page
    //static $cartButton_newProductPage = '#btnaddtocart'
    static $ProductPageElement_addToCart = '#btnaddtocart';
    static $ProductPageElement_viewCart = '#view_cart';

    //cart - begin secure checkout
    #static $cartButton_beginSecureCheckout = '.button.btn-begin-secure-checkout.btn-checkout';
    #static $cartButton_beginSecureCheckout = '.btn.btn-begin-checkout';
    static $cartButton_beginSecureCheckout = '.btn.btn-begin-checkout';



    //cart message
    static $cartValue_truckShipOnly_message = 'Your order contains items that can only be shipped by truck. Flat rate truck shipping will apply.';
    static $cartValue_truckAndGroundShipNoFreeShip_message = 'Your order contains items that can only be shipped by truck. Truck shipping will apply in addition to regular rate.';
    static $cartField_Message = '.success-msg';

    static $cartField_ShipEstimateLink = '.estimate-links a.ship-tax';

    static $cartField_ShipEstimate_opts = '#co-shipping-method-form label';
    static $cartValue_method_truckRate = 'FLAT RATE TRUCK SHIPPING';

    static $cartValue_ShipEstimate_bestWay = 'BEST WAY GROUND';
    static $cartValue_ShipEstimate_2ndDay = 'BEST WAY 2ND DAY AIR - DOMESTIC';
    static $cartValue_ShipEstimate_nextDay = 'BEST WAY NEXT DAY AIR - DOMESTIC';
    //static $cartButton = 'td.scart-order.a-right.last ul.checkout-types li button.button.btn-begin-secure-checkout.btn-checkout';
    static $cartButton = './/*[@id="shopping-cart-table"]/tfoot/tr[2]/td[2]/ul/li/button';

    static $cartValue_truckShipAndGround_message = 'Your order contains items that can only be shipped by truck. Truck shipping will apply in addition to regular rate';
    static $cartValue_method_truckAndGroundRate = 'TRUCK SHIPPING';

    static $cartValue_truckShipAndGroundFree_message = 'Your order contains items that can only be shipped by truck. Flat rate truck shipping will apply – ground ship items will still ship for free.';
    static $cartValue_method_truckAndGroundFreeRate = 'FLAT RATE TRUCK SHIPPING';
//////////////////////////////////////////////////////////////////////////////////////////////////////
    //Returning Customer
    static $chkOutElement = 'div.opcheckout-login';
    static $chkOutValue_txt = 'Begin Secure Checkout';
    static $chkOutElement_txt = 'h3';

    static $chkOutElement_retCustomerTable = '.login1';
    static $chkOutElement_newCustomerTable = '.login2';
    static $chkOutElement_guestTable = '.login3';

    static $chkOutValue_retCustomer = 'Returning Customer';
    static $chkOutValue_retCustomer_txt = 'To checkout faster, sign in to your Emedco account.';
    static $chkOutValue_retCustomer_email = 'Email Address';
    static $chkOutElement_retCustomer_email = '#login-email';
    static $chkOutValue_retCustomer_pwd = 'Password';
    static $chkOutElement_retCustomer_pwd = '#login-password';
    static $chkOutValue_retCustomer_forgotPwd = 'Forgot your password?';
    //static $chkOutValue_retCustomer_button = 'btn.btn-nsignin.firstChild'; //.btn-nsignin
    static $chkOutValue_retCustomer_button = '.btn-nsignin'; //


    //New Customer
    static $chkOutValue_newCustomer = 'New Customers';
    static $chkOutValue_newCustomer_txt = 'For faster checkouts and easier order tracking, create an acount.';
    static $chkOutValue_newCustomer_fn = 'First Name';
    static $chkOutElement_newCustomer_fn = '#firstname';
    static $chkOutValue_newCustomer_ln = 'Last Name';
    static $chkOutElement_newCustomer_ln = '#lastname';
    static $chkOutValue_newCustomer_email = 'Email Address';
    static $chkOutElement_newCustomer_email = '#email_address';
    static $chkOutValue_newCustomer_pwd = 'Password';
    static $chkOutElement_newCustomer_pwd = '#password';
    static $chkOutValue_newCustomer_cnfirmpwd = 'Confirm Password';
    static $chkOutElement_newCustomer_cnfirmpwd = '#confirmation';
    static $chkOutValue_newCustomer_button = 'button.btn:nth-child(3)';
    //Guest
    static $chkOutValue_guest_txt = 'No time to create an account?';
    static $chkOutValue_guest_link = '.btn-ncheckout-guest';

//////////////////////////////////////////////////////////////////////////////////////////////////////
    //billingMethod_asGuest-Fields
    static $billingMethodFld_firstName = '#billing\:firstname';
    static $billingMethodFld_lastName = '#billing\:lastname';
    static $billingMethodFld_comp = '#billing\:company';
    static $billingMethodFld_email = '#billing\:email';
    static $billingMethodFld_add = '#billing\:street1';
    static $billingMethodFld_city = '#billing\:city';
    static $billingMethodFld_state = '//form/select[@name=billing[region_id]';
    static $billingMethodFld_zip = '#billing\:postcode';
    static $billingMethodFld_country = '//form/select[@name=billing:country_id]';
    static $billingMethodFld_tel = '#billing\:telephone';
    static $billingMethodFld_shipSameAdd = '//form/select[@name=billing[use_for_shipping]';
    static $billingMethodBtn = '.button.btn-continue.firstChild';
    //billingMethod_asGuest-Values
    static $billingMethodVal_firstName = 'QA AUTOMATED';
    static $billingMethodVal_lastName = 'TEST';
    static $billingMethodVal_comp = 'BRADY';
    static $billingMethodVal_email = 'lilybeth_ranada@bradycorp.com';
    static $billingMethodVal_add = '3F WCC, Shaw Boulevard';
    static $billingMethodVal_city = 'New York';
    static $billingMethodVal_state = 'New York';
    static $billingMethodVal_zip = '123';
    static $billingMethodVal_country = 'United States';
    static $billingMethodVal_tel = '1234567890';
    static $billingMethodVal_shipSameAdd = '#billing\:use_for_shipping_yes';

    //billingMethod_returningCustomer

    //static $chkOutValue_newCustomer = 'New Customers';

///////////// SHIPPING /////////////////////////////////////////////////////////////////////////////////

    static $chkOutValue_shippingInfo = 'Shipping Info';
    static $chkOutValue_shippingAdd = 'Shipping address';
    static $chkOutValue_shippingMethod = 'Shipping Method';


    //shippingMethod-Field
    static $shippingMethod = '//form/select[@name=shipping_method]';
    static $shippingMethod_options = '//form/select[@name=shipping_method]';
    static $shippingMethod_info = '#additional_shipping_info';
    static $shippingMethodBtn = '.button.btn-continue';
    //shippingMethod-Values
    static $shippingMethodVal = 'Yes';
    static $shippingMethodVal_options = 'BEST WAY GROUND';
    static $shippingMethodVal_info = 'Test order...do not process';
    //AMAZON
    static $chkOutValueAmazon_shippingInfo = 'Shipping Information';
    static $chkOutValueAmazon_shippingAdd = 'Shipping address';


///////////////// PAYMENT INFO (RETURNING CUSTOMER ) //////////////////////////////////////////////////////

    //payment Info - Field

    //static $paymentAdd_retCustomer = 'Returning Customer';
    static $chkOutValue_paymentInfo = 'Payment Info';
    static $chkOutValue_billingAdd = 'Billing address';

    static $chkOutValue_billingAdd_Opt1 = 'Billing address';

    //$I->selectOption('//form/select[@id=shipping_address_id_148934]', 'Lilybeth Ranada');

    //payment Method - Field
    static $chkOutValue_paymentMethod = 'Payment Method';
    static $paymentMethod = 'p_method_paymetric_xisecure';
    static $paymentMethod_ccOwner = 'payment[cc_owner]';
    static $paymentMethod_ccType = 'payment[cc_type]';
    static $paymentMethod_ccNumber = 'payment[cc_number]';
    static $paymentMethod_ccExpMonth = 'payment[cc_exp_month]';
    static $paymentMethod_ccExpYear = 'payment[cc_exp_year]';
    static $paymentMethod_ccID = 'payment[cc_cid]';
    //static $paymentMethodBtn = '.button.btn-continue';
    static $paymentMethodBtn = '.btn.btn-ncontinue';
    //payment Method Field-Value
    static $paymentMethodVal_ccOwner = 'QA Automation Test';
    static $paymentMethodVal_ccExpMonth = '01 - January';
    static $paymentMethodVal_ccExpYear = '2025';

    //Amazon
    static $chkOutFldAmazon_paymentInfoStep = '.step-title.firstChild.h2.firstChild';
    static $chkOutValueAmazon_paymentInfoStep = 'Payment Information';
    static $chkOutFldAmazon_paymentInfoContent = '.step-content.firstChild.h2';
    static $chkOutValueAmazon_paymentInfoContent = 'Payment Information';
    static $chkOutFldAmazon_paymentInfoMethod = '.step-method.firstChild.h2';
    static $chkOutValueAmazon_paymentInfoMethod = 'Payment Method';






//////////////////////////////////////////////////////////////////////////////////////////////////////
    //order review
    static $checkoutField_orderReviewSKU = '.first.last.odd';
    static $checkoutButton_placeOrder = '.button.btn-place-order';
//////////////////////////////////////////////////////////////////////////////////////////////////////



    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }


}