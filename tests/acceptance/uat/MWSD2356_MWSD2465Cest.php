<?php
namespace uat;
use \WebGuy;

class MWSD2356_MWSD2465Cest
{
    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function findQuickOrder(WebGuy $I)
    {
        $I->wantTo('check catalog quick order');
        $I->expectTo('see the catalog quick order ');
        $I->amOnPage('/');
        $I->canSee('Catalog Quick Order','.block-title.firstChild');
        $I->canSeeElement('//*[@id="sku_0"]');
        $I->canSeeElement('//*[@id="qty_0"]');
        $I->canSee('Want to order more catalog items?','.last a.firstChild');
        $I->canSeeElement('button.btn.submit-btn.firstChild');
    }
/*
  //$I->seeLink('Clear',"javascript:document.getElementById('quickorder-validate-detail').reset();");
    //submit without input
    //$I->click('.btn.addtocart-btn.firstChild input[type=submit]');
    $I->click('.btn.addtocart-btn.firstChild');
    $I->switchToWindow();
    $I->canSee('Please enter an item number and quantity.');
    $I->click('.btn input[type=submit]');

    Temp disabled due to cassi and Damon's request's of removing the ITEM keyword

    //submit correct input
    $I->fillField("#sku_0", "HH464");
    $I->fillField("#qty_0", "2");
    $I->click('.btn.addtocart-btn.firstChild');
    $I->seeInCurrentUrl('/endecasearch/result/index/Ntt/');
    $I->see('Item','h1.firstChild');
    $I->click('.topcart-img.right a.firstChild img.firstChild');
    $I->seeInCurrentUrl('/checkout/cart/');
    $I->see('Western Enterprises - Pipe Thread Hex Nipples','.product-shop h2.product-name a');
    $I->see('.qty','2');
    $I->click('.header-logo.clearf a.logo img');

    //submit WRONG input
    $I->click('.btn.addtocart-btn.firstChild input[type=submit]');
    $I->switchToWindow();
    $I->canSee('Please enter an item number and quantity.');
    $I->click('.btn input[type=submit]');

    //submit for 1 input -> Simple
    $I->fillField("#sku_0", "DKL30-BL-H-B");
    $I->fillField("#qty_0", "2");
    $I->click('.btn.addtocart-btn.firstChild input[type=submit]');
    $I->seeInCurrentUrl('/checkout/cart/');
    $I->see('.product-name > a:nth-child(1)','Western Enterprises - Pipe Thread Hex Nipples');
    $I->see('.qty','1');

    //submit for 2 inputs -> Simple
    $I->fillField("#sku_1", "hh464");
    $I->fillField("#qty_1", "2");
    $I->click('#quickorder-validate-detail input[type=submit]');
    $I->seeInCurrentUrl('/checkout/cart/');
    $I->see('.product-name > a:nth-child(1)','Western Enterprises - Pipe Thread Hex Nipples');
    $I->see('.qty','2');

    //submit for 1 input -> Configurable
    $I->fillField("#sku_0", "DKL30-BL-H-B");
    $I->fillField("#qty_0", "2");
    $I->click('.btn.addtocart-btn.firstChild input[type=submit]');
    $I->seeInCurrentUrl('color-coded-master-message-padlock-rm78.html/');

    //submit 2 inputs
    $I->fillField("#sku_0", "DKL30-BL-H-B");
    $I->fillField("#qty_0", "2");
    $I->fillField("#sku_1", "hh464");
    $I->fillField("#qty_1", "2");
    $I->click('#quickorder-validate-detail input[type=submit]');
    $I->seeInCurrentUrl('/checkout/cart/');
    $I->see('/html/body/div[1]/div/div/div/div[3]/div[1]/div/div/div/div/fieldset/table/tbody/tr[2]/td[1]/div/div[2]/span/label','Item #:');
*/

}