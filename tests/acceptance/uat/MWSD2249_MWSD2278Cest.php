<?php
namespace uat;
use \WebGuy;

class MWSD2249_MWSD2278Cest
{

    /* Emedco - Add product recommendations (RR) to No Search Results page */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }


    public $rrProdrec = array(
        'var R3_COMMON = new r3_common();',
        'R3_COMMON.setApiKey(\'f968776e629626b2\');',
        'R3_COMMON.setBaseUrl(window.location.protocol+\'//integration.richrelevance.com/rrserver/\');',
        //'R3_COMMON.setClickthruServer(window.location.protocol+"//"+window.location.host);',
        //'R3_COMMON.setSessionId(\'5c80tt98i13cht2498abohhfg1\');',
        //'R3_COMMON.setUserId(\'5c80tt98i13cht2498abohhfg1\');',
        'R3_COMMON.addPlacementType(\'search_page.no_hit\');',
        //'var R3_SEARCH  = new r3_search();',
        'rr_flush_onload();',
        'r3();'
    );

    public function _shopByCategory(WebGuy $I){
        $I->canSee('Shop by Category','h5.firstChild');
        $I->canSee('Custom Products','div.shopbycat-col1.f-left.firstChild span.firstChild a.firstChild');
        $I->canSeeLink('Custom Products','/custom-products.html');
        //Tags
        $I->canSee('Tags','div.shopbycat-col1.f-left.firstChild span a.firstChild');
        $I->canSeeLink('Tags','/tags.html');
        //Lockout & Electrical
        $I->canSee('Lockout & Electrical','div.shopbycat-col1.f-left.firstChild span a.firstChild');
        $I->canSeeLink('Lockout & Electrical','/lockout-electrical.html');
        //Workplace Safety
        $I->canSee('Workplace Safety','div.shopbycat-col1.f-left.firstChild span a.firstChild');
        $I->canSeeLink('Workplace Safety','/safety-compliance.html');
        //Signs
        $I->canSee('Signs','div.shopbycat-col2.f-left span.firstChild a.firstChild');
        $I->canSeeLink('Signs','/signs.html');
        //Traffic Cones, Barricades, Tapes
        $I->canSee('Traffic Cones, Barricades, Tapes','div.shopbycat-col2.f-left span a.firstChild');
        $I->canSeeLink('Traffic Cones, Barricades, Tapes','/tape-barricades-cones.html');
        //Warehouse Safety & Inventory Control
        $I->canSee('Warehouse Safety & Inventory Control','div.shopbycat-col2.f-left span a.firstChild');
        $I->canSeeLink('Warehouse Safety & Inventory Control','/warehouse-safety-inventory.html');
        //Facility & Site Maintenance
        $I->canSee('Facility & Site Maintenance','div.shopbycat-col2.f-left span a.firstChild');
        $I->canSeeLink('Facility & Site Maintenance','/facility-site-maintenance.html');
        //Labels
        $I->canSee('Labels','div.shopbycat-col3.f-left span.firstChild a.firstChild');
        $I->canSeeLink('Labels','/labels.html');
        //Parking Lot and Grounds
        $I->canSee('Parking Lot and Grounds','div.shopbycat-col3.f-left span a.firstChild');
        $I->canSeeLink('Parking Lot and Grounds','/parking-lot-grounds.html');
        //Security Solutions
        $I->canSee('Security Solutions','div.shopbycat-col3.f-left span a.firstChild');
        $I->canSeeLink('Security Solutions','/security-aids.html');
    }

    public function _needAnswers(WebGuy $I){
        $I->canSeeElement('.f-right');
        $I->canSeeElement('//img[@src="http://emedco.zeondemo.com//skin/frontend/enterprise/emedco/images/emedco-search-result-right-img.png"]');
        $I->canSeeLink('Live Chat','https://server.iad.liveperson.net/hc/29107782/?cmd=file&file=visitorWantsToChat&site=29107782&SESSIONVAR!skill=Emedco');
        $I->canSeeInPageSource('<a href="http://emedco.zeondemo.com/contactus/">');
    }


    // tests
    public function RRonSearchPage(WebGuy $I)
    {
        $I->wantTo('test the RR on No Search Page Results ');
        $I->expectTo('see RR on search result page');
        $I->amOnPage('/search/dfasfdf?search_category_id=0&search_category_label=&Ntt=dfasfdf&/');
        $I->wait(5);
        $I->canSeeElement('.rtcrn-rpt');
        $I->canSee('We found \'0\' search results for \'dfasfdf\'','h4.firstChild');
        $I->canSee('Search again','.pbsearch-label > strong:nth-child(1)');
        $I->canSeeInField('#search_mini_form input','Enter product name or item number');
        //functionality
        $I->fillField('111111111111111','input.input-text-edit:nth-child(1)');
        $I->click('.sus-search-btn');
        $I->wait(10);
        $I->canSee('111111111111111',' h4.firstChild');
        $this->_shopByCategory($I);
        $this->_needAnswers($I);
        //RR for product recommendations
        $I->canSee('You Might Be Interested In','');
        $I->expectTo('see the RR script for product recommendation to no search result page');
        foreach ($this->rrProdrec as $value){
            $I->seeInPageSource($value);
        }


    }


}


