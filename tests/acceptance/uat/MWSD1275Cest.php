<?php
namespace uat;
use \WebGuy;
use \Codeception\Util\Locator;

class MWSD1345Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function checkConfigurableProduct(WebGuy $I)
    {
        $I->wantTo('Check Configurable Product');
        $I->amOnPage('mining-site-traffic-warning-signs-beware-of-vehicles-80276.html');
        $I->amGoingTo('Select a Material');
        $I->selectOption('#attribute169','104');
        $I->amGoingTo('Select a Size');
        $I->selectOption('#attribute170','7806');
        $I->amGoingTo('Select a Topcoat');
        $I->selectOption('#attribute353','7755');
        $I->amGoingTo('Select a Reflectivity');
        $I->selectOption('#attribute181','122');
        $I->fillField('qty',4);
        $I->waitForUserInput();
        $I->amGoingTo('Add to cart');
        $I->click('//*[@id="btnaddtocart"]');
        $I->waitForElementVisible('.cartpopup',5);
        $I->click('//*[@id="popup-added"]/div/div[1]/a/img');
        $I->waitForUserInput();

    }
    public function checkLinks(WebGuy $I){
        $I->canSee('Material');
        $I->canSee('Size');
        $I->canSee('Topcoat');
        $I->canSee('Reflectivity');
        //$I->amOnPage('mining-site-traffic-warning-signs-beware-of-vehicles-80276.html');
        $I->click('Email Page');
        $I->canSeeInCurrentUrl('sendfriend/product/send/id/215331/');
        $I->amOnPage('mining-site-traffic-warning-signs-beware-of-vehicles-80276.html');

        $I->click('Facebook');
        $I->switchToWindow();
        $I->click('Pinterest');
        $I->switchToWindow();
        $I->click('Twitter');
        $I->switchToWindow();
        $I->click('Add to Wishlist');
//        $I->switchToWindow();
        //$I->waitForElement(3);
//        $I->click('Print Page');
        //$I->acceptPopup();
    }
}