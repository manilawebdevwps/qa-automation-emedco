<?php
namespace uat;
use \WebGuy;

class MWSD1423Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function EditLiveChat(WebGuy $I)
    {
        $I->amOnPage('/');
        $I->wantTo('See LivePerson links in the Live Chat header');
        $I->canSeeLink('Live Chat');
        $I->click('Live Chat');
        $I->wait(5);
        $I->switchToWindow('Live Chat');
        $I->expectTo('First Name Field');
        $I->canSeeElement('#txt_1348439');
        $I->fillField('#txt_1348439','Test');
        $I->expectTo('Email Address Field');
        $I->canSeeElement('#txt_1357015');
        $I->fillField('#txt_1357015','bradytestqa@gmail.com');
        $I->expectTo('Question Box Field');
        $I->canSeeElement('#txtar3543448');
        $I->fillField('#txtar3543448','This is for test purposes, please disregard.');
        $I->click('Begin chat');


    }
}