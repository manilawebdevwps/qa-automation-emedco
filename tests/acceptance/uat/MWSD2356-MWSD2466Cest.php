<?php
namespace uat;
use \WebGuy;

class MWSD2356_MWSD2466Cest
{

    /*Emedco - Update homepage - RR, Quick Order, Shopper Approved*/

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    public $rr3 = array(
        'var R3_COMMON = new r3_common();',
        'R3_COMMON.setApiKey(\'f968776e629626b2\');',
        'R3_COMMON.setBaseUrl(window.location.protocol+\'//integration.richrelevance.com/rrserver/\');',
        'R3_COMMON.setClickthruServer(window.location.protocol+"//"+window.location.host);',
        //'R3_COMMON.setSessionId(\'5c80tt98i13cht2498abohhfg1\');',
        //'R3_COMMON.setUserId(\'5c80tt98i13cht2498abohhfg1\');',
        'R3_COMMON.addPlacementType(\'home_page.rr1\');',
        'R3_COMMON.addPlacementType(\'home_page.rr2\');',
        'R3_COMMON.addPlacementType(\'home_page.rr3\');',
        'var R3_HOME = new r3_home();',
        'rr_flush_onload();',
        'r3();'
    );

    public $rrRecProd = array(
            'function setRecProducts() {',
            'setTimeout(function() {',
            //'if (RR.data !== undefined && RR.data.JSON.placements[0] !== undefined) {',
            'var rrArr = RR.data.JSON.placements[0].recs;',
            //'var rrMsg = \'<h2 class="hp-block__title">\'+RR.data.JSON.placements[0].strat_message+\'</h2>\';',
            '$$(\'.rec_prod_head\')[0].replace(rrMsg);',
            'rrArr.each(function(d, i){',
            //'$(\'rr_home_ul\').insert(\'<li class="favorites_1"> <a href="\'+d.ct_url+\'"><div class="hp-prodimg a-center"><img src="\'+d.image+\'" border="0" width="100" height="100" /></div><div class="hp-prodtitle"><h3>\'+d.name+\'</h3></div></a><div class="hp-prodprice">Starting at <strong>$\'+d.price+\'</strong></div></li>\');',
            '});',
            'decorateGeneric($$(\'.favorites_1\'), [\'last\', \'\', \'\',\'first\']);',
            '} else {',
            'setRecProducts(); //keep calling til rrdata has data',
            '}',
            '}, 1000);'
    );

    public $rrTopSel = array(
        'function setTopSelling() {',
        'setTimeout(function() {',
        //'if (RR.data !== undefined && RR.data.JSON.placements[1] !== undefined) {',
        'var rrArr = RR.data.JSON.placements[1].recs;',
        //'var rrMsg = \'<h2 class="hp-block__title">\'+RR.data.JSON.placements[1].strat_message+\'</h2>\';',
        '$$(\'.top_sell_head\')[0].replace(rrMsg);',
        'rrArr.each(function(d, i){',
        //'$(\'rr_top_sell\').insert(\'<li class="favorites_2"> <a href="\'+d.ct_url+\'"><div class="hp-prodimg a-center"><img src="\'+d.image+\'" border="0" width="100" height="100" /></div><div class="hp-prodtitle"><h3>\'+d.name+\'</h3></div></a><div class="hp-prodprice">Starting at <strong>$\'+d.price+\'</strong></div></li>\');',
        '});',
        'decorateGeneric($$(\'.favorites_2\'), [\'last\', \'\', \'\',\'first\']);',
        '} else {',
        'setTopSelling(); //keep calling til rrdata has data',
        '}',
        '}, 1000);',
        '}'
    );


    public $rrMostPop = array(
        'function setMostPopular() {',
        'setTimeout(function() {',
        //'if (RR.data !== undefined && RR.data.JSON.placements[2] !== undefined) {',
        'var rrArr = RR.data.JSON.placements[2].recs;',
        //'var rrMsg = \'<h2 class="hp-block__title">\'+RR.data.JSON.placements[2].strat_message+\'</h2>\';',
        '$$(\'.most_pop_head\')[0].replace(rrMsg);',
        'rrArr.each(function(d, i){',
        //'$(\'rr_most_pop\').insert(\'<li class="favorites_3"> <a href="\'+d.ct_url+\'"><div class="hp-prodimg a-center"><img src="\'+d.image+\'" border="0" width="100" height="100" /></div><div class="hp-prodtitle"><h3>\'+d.name+\'</h3></div></a><div class="hp-prodprice">Starting at <strong>$\'+d.price+\'</strong></div></li>\');',
        '});',
        'decorateGeneric($$(\'.favorites_3\'), [\'last\', \'\', \'\',\'first\']);',
        '} else {',
        'setMostPopular(); //keep calling til rrdata has data',
        '}',
        '}, 1000);',
        '}'
    );


// tests
    public function rr3(WebGuy $I)
    {
    $I->wantTo('check script of RR in homepage');
    $I->expectTo('see the RR3 script');
    $I->amOnPage('/');
  //RR3
    foreach ($this->rr3 as $value){
        $I->seeInPageSource($value);
    }


    //Recommended Product
     $I->wantTo('check script of RR in homepage recommended products ');
     $I->expectTo('see the RR3 script');
     foreach ($this->rrRecProd as $value){
        $I->seeInPageSource($value);
     }

     //Top Selling Product
     $I->wantTo('check script of RR in homepage top selling products ');
     $I->expectTo('see the RR script for top selling products');
     foreach ($this->rrTopSel as $value){
        $I->seeInPageSource($value);
     }

        //Most Popular  Product
     $I->wantTo('check script of RR in homepage most popular products ');
     $I->expectTo('see the RR script for most popular products');
     foreach ($this->rrMostPop as $value){
         $I->seeInPageSource($value);
     }


     }






}