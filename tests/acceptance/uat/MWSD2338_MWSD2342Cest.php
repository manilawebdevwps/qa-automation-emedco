<?php
namespace uat;
use \WebGuy;

class MWSD2338_MWSD2342Cest
{

    /* QA-Testing:  Create the Header part (Staging) */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function header_emedcoBlog(WebGuy $I)
    {
        $I->wantTo('check new  domain of Emedco blog');
        $I->expectTo('Complete blog migration with new creative template implementation');
        $I->amOnPage('/blog');
        $I->canSeeElement('.page-head');
        $I->canSeeElement('//img[@src="http://emedco.zeondemo.com/blog/wp-content/themes/emedco/images/emedco-logo.png"]');
        $I->canSeeElement('input', ['name' => 's']);
        //navigation
        $I->canSeeElement('html body div.container div.wrapper nav.main-nav.cf ul#menu-primary.menu');
        $I->canSeeLink('About Emedco');
        $I->canSeeLink('Shop Emedco');
        $I->canSeeLink('Contact Us');
        //functionality
        $I->click(['link' => 'About Emedco']);
        $I->wait(3);
        //$I->seeInCurrentUrl('About Emedco');
        $I->canSee('Blog Home','.bread-link');
        $I->canSee('About Emedco','.bread-current');
        $I->canSee('About Emedco','h2');
        $I->click('//img[@src="http://emedco.zeondemo.com/blog/wp-content/themes/emedco/images/emedco-logo.png"]');
        $I->seeInCurrentUrl('/blog/');

    }
}