<?php
namespace uat;
use \WebGuy;

class MWSD1747Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }


    //NOT UPDATED FOR ACTUAL ELEMENTS


    // tests
    public function pageCheck(WebGuy $I)
    {
        $I->wantToTest('link of Catalog Quick Order');
        $I->expectTo('see new features ---> Catalog Quick Order');
        //$I->amOnPage('/');
        //$I->click('Catalog Quick Order','page div.header-container.firstChild div.header-stripe.firstChild div.left.firstChild span.header-quickorder.firstChild');
        //$I->seeInCurrentUrl('/catalog-quick-order');
        $pageForTestPurpose =
        $I->changeBaseURL('http://www.seton.com');  //to disble if emedco landing page is done
        $I->amOnPage('/quick-shop.html');           //to disble if emedco landing page is done
        $I->wait(10);
        //6800
        #product-item-0
        //html.wf-opensans-n4-active.wf-active body.andromeda-quickorder-index-index div.wrapper div.page div.main.col2-right-layout div.col-main div.cart form#quickorder_addtocart_form fieldset table#quickorder-table.data-table.cart-table tbody#quickorder-table-body tr#table-item-0.even td input#product-item-0.input-text.product-autocomplete-field.validation-passed
        $I->seeElement('.col-main');
        $I->see('Quick Order','div.col-main div.cart div.page-title.title-buttons h1');
        $I->see('Enter the catalog number and quantity of each desired product','.col-main div.cart div.page-title.title-buttons p');
        $I->see('Add to Cart','.col-main div.cart button.button.btn-cart');
        $I->seeElement('form#quickorder_addtocart_form fieldset table#quickorder-table.data-table.cart-table');
        $I->see('Catalog number','#quickorder-table > thead > tr > th');
        $I->see('Quantity','#quickorder-table > thead > tr > th');
        $I->see('Unit Price','#quickorder-table > thead > tr > th');
        $I->see('Subtotal','#quickorder-table > thead > tr > th');

       for ($i = 0; $i < 10; $i++) {
            //$I->seeElement('div.col-main div.cart form#quickorder_addtocart_form fieldset table#quickorder-table.data-table.cart-table tbody#quickorder-table-body tr#table-item-0.even td input#product-item-0.input-text.product-autocomplete-field.validation-passed');
            if ($I->seeElement('div.col-main div.cart form#quickorder_addtocart_form fieldset table#quickorder-table.data-table.cart-table tbody#quickorder-table-body tr#table-item-0.even td input#product-item-'. $i .'')){
                $I->expectTo('see textbox#'.$i.'');
           }
        }

        $I->seeElement('.col-main');
        $I->see('Add more rows','div.col-main div.cart form#quickorder_addtocart_form fieldset table#quickorder-table.data-table.cart-table tfoot tr td.a-left button#btn-add-item.button.btn-add');
        $I->see('TOTAL AMOUNT:',' div.col-main div.cart form#quickorder_addtocart_form fieldset table#quickorder-table.data-table.cart-table tfoot tr td.a-right div');
        $I->seeElement('TOTAL AMOUNT:',' div.col-main div.cart form#quickorder_addtocart_form button.button.btn-cart');
    }

    public function functionValidSKU(WebGuy $I)
    {
        //functionality
        $I->changeBaseURL('http://www.seton.com');  //to disble if emedco landing page is done
        $I->amOnPage('/quick-shop.html');           //to disble if emedco landing page is done
        $I->wait(10);
        $I->fillField('#product-item-0',\ProductPage::$validProduct_quickOrder);
        $I->wait(3);
        $I->click('#product-autocomplete-container-0');
        $I->see(\ProductPage::$validProduct_quickOrder,'form#quickorder_addtocart_form fieldset.highlight table#quickorder-table.data-table.cart-table tbody#quickorder-table-body tr#table-item-0.even td');
        $I->see(\ProductPage::$productPrice_quickOrder,'form#quickorder_addtocart_form fieldset.highlight table#quickorder-table.data-table.cart-table tbody#quickorder-table-body tr#table-item-0.even td div.price-box');
        $I->see('1','form#quickorder_addtocart_form fieldset.highlight table#quickorder-table.data-table.cart-table tbody#quickorder-table-body tr#table-item-0.even td input#qty-item-0');
        $I->see(\ProductPage::$productPrice_quickOrder,'form#quickorder_addtocart_form fieldset.highlight table#quickorder-table.data-table.cart-table tbody#quickorder-table-body tr#table-item-0.even td div.price-box');
    }


        public function functionInvalidSKU(WebGuy $I)
    {
        //functionality
        $I->changeBaseURL('http://www.seton.com');  //to disble if emedco landing page is done
        $I->amOnPage('/quick-shop.html');           //to disble if emedco landing page is done
        $I->wait(10);
        $I->fillField('#product-item-0',\ProductPage::$invalidProduct_quickOrder);
        $I->see(\ProductPage::$validProduct_quickOrder,'form#quickorder_addtocart_form fieldset.highlight table#quickorder-table.data-table.cart-table tbody#quickorder-table-body tr#table-item-0.even td');
        $I->seeElement('.validation-error');

    }



















}