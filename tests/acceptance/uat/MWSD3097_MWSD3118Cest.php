<?php
namespace uat;
use \WebGuy;

class MWSD3097_MWSD3118Cest
{



    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    static $url = 'http://www.emedco.com/';
    #static $url = 'http://emedconewstg.perficientdcsdemo.com';

    // tests
    public function tryToTest(WebGuy $I)
    {
        $I->wantTo('test revised PLA page');
        #$I->changeBaseURL('http://www.emedco.com/');
        $I->changeBaseURL(''.self::$url.'');
        $I->amOnPage(\ProductPage::$PLAProduct);
        $I->canSeeElement(".landing-pdp.firstChild");
        $I->canSeeElement("div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__content.firstChild");
        $I->canSeeElement("div.rtcrn-rpt.firstChild div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__similar.clearf");
        $I->canSee('3M Particulate Respirator 8511, N95','h3');
        #$I->canSeeElement('//img[@src="http://emedconewstg.perficientdcsdemo.com/media/catalog/product/3M-Particulate-Respirator-8511-N95-RE326-ba.jpg"]');
        #$I->canSeeElement('//img[@src="'.self::$url.'/media/catalog/product/3M-Particulate-Respirator-8511-N95-RE326-ba.jpg"]');
        $I->canSeeElement('//img[@src="http://www.emedco.com/media/catalog/product/3M-Particulate-Respirator-8511-N95-RE326-ba.jpg"]');
        $I->canSeeElement(".f-left.landing-pdp_desc");
        $I->canSee('3M Particulate Respirator 8511, N95','div.clearf div.f-left.landing-pdp_desc div.firstChild strong.firstChild');
        $I->canSee('$','div.landing-pdp__content.firstChild div.clearf div.f-left.landing-pdp_desc div.landing-pdp__priceuom span.price.firstChild');
        $I->canSeeLink('Next Step','div.landing-pdp__content.firstChild div.clearf div.f-left.landing-pdp_desc div p.firstChild');
        #$I->canSee('Plus, save more when you buy more!','div.f-left.landing-pdp_desc div p.firstChild a.firstChild');
        $I->canSeeLink('Next Step','div.landing-pdp__content.firstChild div.clearf div.f-left.landing-pdp_desc div a.next-step-btn');
        $I->canSeeElement('div.col-main div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__similar.clearf');
        $I->canSee('Similar Products','h3');
        $I->canSeeElement('div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__similar.clearf div.landing-pdp__side');
        #$I->canSeeElement('//img[@src="http://emedconewstg.perficientdcsdemo.com/skin/frontend/enterprise/emedco/images/emedco-landing-sidebar-img.png"]');
        #$I->canSeeElement('//img[@src="http://www.emedco.com/skin/frontend/enterprise/emedco/images/emedco-landing-sidebar-img.png"]');
        $I->canSeeElement('//img[@src="'.self::$url.'skin/frontend/enterprise/emedco/images/emedco-landing-sidebar-img.png"]');
        $I->canSee('For over 60 years, Emedco has been the leading manufacturer and distributor of safety signs and identification products for the workplace',
                    'div.main.col1-layout div.col-main div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__similar.clearf div.landing-pdp__side');
        $I->canSee('Emedco offers same day shipping on all signs, tags and labels in stock',
                    'div.main.col1-layout div.col-main div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__similar.clearf div.landing-pdp__side');
        $I->canSee('Emedco has easy customization designed to provide safe and secure solutions for businesses',
                    'div.main.col1-layout div.col-main div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__similar.clearf div.landing-pdp__side');
        $I->canSee('With over 200,000 products available, the majority of our products are backed with a lifetime guarantee',
            'div.main.col1-layout div.col-main div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__similar.clearf div.landing-pdp__side');
        $I->canSeeElement('div.main.col1-layout div.col-main div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.landing-pdp.firstChild div.landing-pdp__similar.clearf ul.horizontal-list.f-left');
        $I->canSeeElement('ul.horizontal-list:nth-child(3)');
        $I->canSeeElement('ul.horizontal-list:nth-child(4)');
        $I->canSeeElement('ul.horizontal-list:nth-child(5)');
        $I->canSeeElement('ul.horizontal-list:nth-child(6)');
        $I->canSeeElement('ul.horizontal-list:nth-child(7)');
        $I->click('Next Step');
        $I->wait('5');
        $I->canSeeInCurrentUrl('/3m-particulate-respirator-8511-n95-re326.html');
        $I->moveBack();
        $I->wait('5');
        $I->canSeeInCurrentUrl('pla/?sku=RE326');
        $I->click('.next-step-btn');
        $I->wait('5');
        $I->canSeeInCurrentUrl('/3m-particulate-respirator-8511-n95-re326.html');

    }
}