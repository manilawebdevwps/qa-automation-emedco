<?php
namespace uat;
use \WebGuy;

class MWC1850Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function tryToTest(WebGuy $I)
    {
        $I->wantTo('get data from DB');
        $I->expectTo('see the URLs of sitemap ');
        $I->changeBaseURL('http://www.emedco.com/');
        $results = $I->getArrayFromDB('removed-but-not-present-in-round2','*',array());
        foreach ($results as $key=>$val) {
            $text = $I->truncate($val['url']);
                                       //FOR TESTING ONLY
            $I->amOnPage($text);
            //$I->seeInPageSource('content="No-Follow/No-Index"');                  //ENABLE THIS WHEN DEV IS FINISHED
            $I->canSeeInPageSource('meta name="robots" content="INDEX, FOLLOW"');  //FOR TESTING ONLY - OK
        }
    }
}