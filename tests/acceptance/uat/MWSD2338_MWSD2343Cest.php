<?php
namespace uat;
use \WebGuy;

class MWSD2338_MWSD2343Cest
{
    /* QA-Testing:  Create the Content part (Staging) */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    //GHS
    public static $title_GHS = 'Countdown to Compliance: GHS What You Need to Know';
    #public static $title_GHS = 'Countdown to Compliance: GHS What...';
    public static $date_GHS = 'February 5, 2015';
    public static $fb = '.facebook';
    public static $twitter = '.twitter';
    public static $google= '.google';
    public static $Email= '.WP-EmailIcon';
    #static $footer_GHS = 'safety-security-solutions';
    //BURN
    static $title_Burn = 'Burn Awareness: Top 6 Most Frequent Burn Types';
    static $date_Burn = 'February 3, 2015';
    static $footer_Burn = 'safety-security-solutions';
    //Harmful Chemicals
    static $title_harmfulChemicals = 'Youâ€™ll Never See Them Coming: The Importance of Permissible Exposure Limits on Harmful Chemicals';
    static $date_harmfulChemicals = 'January 29, 2015';
    static $footer_harmfulChemicals = 'safety-security-solutions';
    static $tag_harmfulChemicals ='toxic-chemicals';
    //WORKPLACE SAFETY
    static $title_workplaceSafety = 'Workplace Safety News Roundup';
    static $date_workplaceSafety = 'January 27, 2015';
    static $footer_workplaceSafety = 'safety-security-solutions';
    static $tag_workplaceSafety ='workplace-safety';
    //WORKING OUTDOORS
    static $title_workingOutdoors = '5 Types of Cold Stress Left Untreated';
    static $date_workingOutdoors = 'January 22, 2015';
    static $footer_workingOutdoors = 'safety-security-solutions';
    static $tag_workingOutdoors ='working-outdoors';
    //WORKPLACE SAFETY IMPROVEMENT
    static $title_workplaceSafety_improvement = 'Evaluating Your Current Safety Plans: Leading and Lagging Indicators of Safety Performance';
    static $date_workplaceSafety_improvement = 'January 22, 2015';
    static $footer_workplaceSafety_improvement = 'safety-security-solutions';
    static $tag_workplaceSafety_improvement ='workplace-safety-improvement';
    //WORKPLACE SAFETY CONTROLS
    static $title_workplaceSafety_controls = 'The Hierarchy of Controls for Making Safety Changes';
    static $date_workplaceSafety_controls = 'January 15, 2015';
    static $footer_workplaceSafety_controls = 'safety-security-solutions';
    static $tag_workplaceSafety_controls ='workplace-safety';
    //OSHA VIOLATORS
    static $title_oshaViolators = 'A List You Donâ€™t Want to Be On: OSHAâ€™s Severe Violators Program';
    static $date_oshaViolators = 'January 8, 2015';
    static $footer_oshaViolators = 'safety-security-solutions';
    static $tag_oshaViolators ='workplace-safety';
    //SAFETY COMMITTEE
    static $title_safetyCommittee = 'Create an Effective Safety Committee';
    static $date_safetyCommittee = 'January 6, 2015';
    static $footer_safetyCommittee = 'safety-security-solutions';
    static $tag_safetyCommittee ='workplace-safety';
    //OSHA REGULATIONS
    static $title_oshaRegulation = 'New Year, New OSHA Regulations';
    static $date_oshaRegulation = 'January 2, 2015';
    static $footer_oshaRegulation = 'safety-security-solutions';
    static $tag_oshaRegulation ='workplace-safety';

    public $GHS = array(
        'What is it and what’s it got to do with me? These are the questions most people ask themselves when they are being told they have to do something. In the case of the upcoming GHS changes, its relevance is quite simple. The highlights that make GHS important include it being a Globally Harmonized System that establishes agreed hazard classification and communication provisions with explanatory information on how to apply the system.',
        'The significant changes that all facilities should be aware of are the new classification criteria for health, physical, and environmental chemical hazards, the 6 standardized label elements for hazard classes and categories, the appropriate signal words, pictograms, and hazard and precautionary statements, and the standardized order of information for the new 16 section Safety Data Sheets.',
        'The importance of the adoption of GHS in the United States and abroad hinges on making information about potentially hazardous chemicals available to all that come in contact with it. This GHS system will make it easier to identify this important information through the standardization of protocol. The changes will increase quality and consistency of information provided to workers across the globe. This will ideally make it possible for any worker to walk into any facility and easily recognize all hazards present that are marked with GHS labeling.',
        'The benefits of this program will be felt throughout your facility with better comprehension for employees. Better understanding of work processes and hazards will lead to fewer workplace illnesses and injuries which in turn will lead to a better workflow and interaction.',
        'Two U.S. deadlines on the horizon in 2015:',
            'June 1, 2015',
            'Chemical manufacturers and distributors must complete hazard reclassification and produce GHS styled labels and safety data sheets. Distributors get an additional 6 months to complete shipments of old inventory.',
            'December 1, 2015',
            'Distributors must comply fully with HCS requirements. (Grace period for shipments of old inventory ends.)',
        'Make sure that your facility is on the right track with full compliance. Feel like you might need some more compliance help around your facility, get some help here.'
    );

    public $Burn = array(
        'In Honor of the recent burn awareness week that just passed we ask you our readers if you are aware of the 6 popular types of burns that occur in the workplace? While all burns may not directly apply to all workplaces it is important to cover all your bases to make sure that you are providing your workers with the proper protection for any burn hazard they may come into contact with.',
        'Thermal burns are caused by fire, steam, hot objects, or hot liquids. These may be the most often thought of when the word burn is brought into conversation. Anything from a sun burn affecting usually only the first layer of skin to more severe damage and blistering to all layers of skin and tissue, 1st degree to 3rd degree burns should not be taken lightly and may require  immediate emergency response and care.',
        'Cold temperature burns are caused by skin exposure to wet, windy, or cold conditions. These are burns that are seasonal and most directly affect those working outdoors in cold temperatures for extended periods of time. Not to be taken lightly, cold burns also known as frostbite if not taken care of properly can cause numbness, blistering or even gangrene to exposed areas, causing damage to tendons, muscles and nerves and in most severe cases resulting in amputation.',
        'Electrical burns are caused by contact with electrical sources or by lightning. These types of burns may not always show their damage on the outside of your body. In some cases severe damage and long lasting effects can be felt on the inside through heart rhythm disturbance or cardiac arrest. Sometimes the jolt associated with the electrical burn can also cause you to be thrown or to fall, resulting in fractures or other associated injuries. There are many types of electrical burns each coming with their own steps for prevention and protection and treatment from arc flash, to high voltage to oral burns, it is important to determine the exact hazard before formulating a safety program.',
        'Chemical burns are caused by contact with household or industrial chemicals in a liquid, solid, or gas form. If a chemical comes in contact with your nose mouth skin or eyes it may cause internal damage if chemical is swallowed. Most commonly chemical burns are caused by contact with acids and bases. The severity of burn may depends on many factors including, length of contact, concentration of chemical, the strength of chemical, and whether it was in a solid liquid or gas form',
        'Radiation burns are caused by the sun, tanning booths, sunlamps, X-rays, or radiation therapy for cancer treatment. Perhaps one of the least likely for most workplace outside of the health care industry it is still very important to protect yourself from them if in contact with radiation.',
        'Friction burns are caused by contact with any hard surface such as roads ("road rash"), carpets, or gym floor surfaces. The easiest way to prevent against a friction burn is to properly cover your skin with protective gear or clothing if there is a chance there may be a chance for repetitive scrapping or dragging motion.',
        'Now that you have gotten a crash course in the 6 most popular types of burns that your employees could face in your company, evaluate your workplace to see which may cause the most hazards and establish the proper protection plan for prevention !'
    );

    public $harmfulChemicals = array(
        'Permissible exposure limits (PELs) have been all over the news with countless incidents being reported showing the debilitating affects lifelong exposure without any proper precautions can have on a loyal worker. Of the thousands of chemicals used in workplaces, OSHA has PELs for less than 500, 95% of which have not been updated since 1971. Recognizing the shortcomings in PEL standards OSHA is putting the responsibility back in the hands of individual companies to do the right thing and ensure the safety of the hardworking individuals.',
        'With many types of harmful chemical and gases found in workplaces across America, the effects can be vast and grave. Exposure symptoms can start as a simple nose or eye irritation but can quickly build into chronic light headedness, difficulty breathing, cancer, paralysis or even death. It is an unfortunate state of affairs when it comes to PELs. Workers are being exposed to limits that are legal but in no way healthy.',
        'In October 2014 OSHA launched a nation dialogue in hopes of giving stakeholders a forum to develop innovative, effective approaches to improve the health of workers. OSHA has also been trying to find new ways of ensuring the safety of workers where they feel companies arenâ€™t doing enough on their own to protect. For example in a recent visit highlighted in the wall street journal OSHA cited dangerous environment not under the PELs but rather the General Duty Clause to correct a situation that was having a negative effect on the well being of its employees.',
        'Chemicals have warnings on them for a reason, the workers who have to interact with them shouldnâ€™t have to risk their own well being to do so. While giving a voice to this long term issue is the first step, hopefully not only OSHA but facilities on an individual basis will start taking safety into their own hand while formal legislation is being passed if for nothing else than the future wellbeing of their own company.',
        'If your workplace is looking for safer chemical alternatives, visit the OSHA toolkit for transitioning here https://www.osha.gov/dsg/safer_chemicals/index.html'
    );

    public $workplaceSafety = array(
        'Keeping track of new OSHA regulations and taking advantage of supplemental safety training and reading materials makes a large difference in the success of the programs you implement in your workplace. Seeing what others are doing both for the better and worse help mold an all encompassing safety initiative. Here is a sampling of some of the news buzzing around workplace safety this month.',
        'State of Safety 2015',
        'What is the state of safety? Itâ€™s a question Safety+Health explores every year by speaking with experts and looking at the most recent data. The largest national source of occupational injury and illness data comes from the Bureau of Labor Statisticsâ€™ annual Survey of Occupational Injuries and Illnesses. This survey provides an estimate of nonfatal injuries nationwide, allowing stakeholders to get a clearer picture of workplace safety in the United States.',
        'To read more about these findings click here: http://www.safetyandhealthmagazine.com/articles/11572-state-of-safety-2015',
        'Workplace psychological injury on radar, but more â€˜work to be doneâ€™',
        'An increasing number of workers comp claims for psychological injuries is a factor causing HR teams to examine the possible workplace safety implications. â€œA lot of companies are talking about psychological injuries and the types of systems they need to put in place to deal with that. It is more of a recent challenge.â€',
        'Learn more about what this may mean for your workplace click herehttp://www.hcamag.com/hr-news/workplace-psychological-injury-on-radar-but-more-work-to-be-done-194766.aspx',
        'OSHA Will Put Workplace Safety Data Online as \'Nudge\' to Employers',
        'The site already includes information on worker fatalities and catastrophes. The hope, David Michaels Assistant Secretary of Labor for Occupational Safety and Health says, is that additional information will embarrass companies into being more careful. â€œWe believe that the possibility of public reporting of serious injuries will encourageâ€”or, in the behavioral economics term, nudgeâ€”employers to take steps to prevent injuries so theyâ€™re not seen as unsafe places to work,â€ says Michaels. â€œAfter all, if you had a choice of applying for a job at a place where a worker had just lost a hand, vs. one where no amputation has occurred, which would you choose?â€',
        'To find out more information about this click here: http://www.businessweek.com/articles/2014-09-18/osha-will-put-workplace-safety-data-online-as-nudge-to-employers',
        'OSHA Violations',
        'OSHA fines company more than $76,000 for willfully putting employees at risk',
        'An OSHA inspection resulted in one willful and 17 serious health and safety violations for not conducting noise testing or providing protective equipment and not monitoring worker exposure to noise at a Weston foundry. The company faces proposed penalties of $76,200. Read More here',
        'Cited for Exposing workers to trench cave-ins for the 8th time',
        'For the eighth time, the U.S. Department of Labor\'s Occupational Safety and Health Administration has cited an Excavating Co. for allowing its employees to work at great risk in trenches without cave-in protection and a safe means to exit the trench.',
        'OSHA inspectors witnessed two employees repairing a valve on a city water line in an 8-foot trench. An investigation followed, and the agency cited the company for two willful and one serious violation with penalties of $147,000. For its continual failure to protect workers from cave-in hazards, the Jamestown-based company has been placed in OSHA\'s Severe Violator Enforcement Program*. The company has been cited eight times since 1997, and failed to pay its most recent penalties from a 2011 inspection. Read more here',
        'Employers face more than $110K in fines for failing to provide protections',
        'Workers doing renovations faced potentially fatal falls of up to 40 feet because their employers failed to provide proper protection. In all, four contractors were cited and fined $110,670 by OSHA. OSHA found several fall hazards; no fall protection for employees working on the roof; unguarded floor holes; insufficient anchorage for fall protection; and employees untrained to recognize fall hazards. Read more here'
    );

    public $workingOutdoors = array(
        'Exposure to wide ranging temperatures can be taxing on the human body. While much attention is given to the importance of keeping workers out of the sun and properly hydrated during the summer, there is just as much danger when it comes to workers who are exposed to extreme cold conditions, resulting in cold stress.',
        'There are 5 types of cold stress that if ignored and left untreated could result in life threatening consequences. These types include hypothermia, frostbit, cold water immersion, trench foot, and chilblains. If you have workers that are required to remain out in the elements all winter long make sure that they are provided with the proper gear to protect against the bitter temperatures and increasing wind speeds.',
        'Prolonged exposure to cold will eventually use up your body\'s stored energy. A body temperature that is too low affects the brain, making the victim unable to think clearly or move well. This is particularly dangerous because a person may not know it is happening and will not be able to do anything about it. This is hypothermia setting in. It is important to note that during cold water immersion this process is accelerated up to 25 times the normal rate.',
        'When freezing to the body occurs the loss of feeling and color from the affected areas sets in most commonly on the nose, ears, cheeks, chin, fingers, or toes. These are the feelings of the beginning stages of frostbite. If there has been prolonged exposure of damp condition such as snow to the feet as well, trench foot is likely to develop.  Left ignored this can result in permanent body tissue damage and amputation.',
        'Repeated skin exposure to sudden and drastic fluctuations in temperatures could result in painful inflammation of small blood vessels in your skin known as Chilblains. Redness, itching, blistering and skin discoloration with additional exposure. Permanent damage to capillary beds is possible.',
        'The best action is preventative when it comes to protecting against all forms of cold stress. Make sure your workers are wearing appropriate clothing including layering loose fitting garments and protecting the ears, face hands and feet. Allow them to cold weather gear including extra socks, gloves, jackets, blankets and a change of clothes and hot liquids and always monitor their condition limiting time outside on extremely cold days.',
        'For more information on cold stress and the breakdown of all the types visit http://www.cdc.gov/niosh/topics/coldstress/'
    );

    public $workplaceSafetyImprovement = array(
        'Do you take the time out to evaluate your workplace from time to time to see if your procedures have been driving the most beneficial outcomes? When it comes to safety practices it is important to evaluate your current plans to see where improvements can be made, one way of doing this is by starting to take a look at the leading and lagging indicators of performance within your facility.',
        'Some might say that leading indicators drive lagging indicators. This means that leading indicators are what drive the direction of your program and correlate with safety performance such as activities, participation, perception, behavior and conditions. For example, the layout and organization of your building, the presence of safety training, hazard alerts and follow-ups, audits, the pace of work and how people navigate and interact with each other to do their jobs. These would all be leading indicators to make note of.',
        'Lagging indicators are what comes out of the leading indicators and are more data oriented around injury reporting, and workers compensation costs, lost time injuries and near misses. The tangible data that can be broken down and analyzed based on the history of your facility and its safety record.',
        'Sorting the actions and reactions of what makes up your facility currently and how they have resulted in your past workplace safety performance is a good place to start to see what you could change or be doing better. By dividing your workplace into leading and lagging indicators this may be a great first step in coming up with the "road ahead" in your facilities safety plan for 2015.'
    );

    public $workplaceSafetyControls = array(
        'Controlling exposure to hazards around your facility while getting the job done is a constant struggle that many business owners face. While every business comes with its own unique situations, balancing the safest solution while still being able to perform the necessary tasks is not always clear. There no universally acceptable answer.',
        'Many businesses often turn to the hierarchy of controls of develop a safety protocol that will work best for their environment. The hierarchy of controls is a system used in many industries to minimize or eliminate exposure of hazard and is a widely accepted system promoted by many safety organizations including OSHA and the CDC.',
        'There are 5 controls that are listed from most effective to least effective and differ on the type of solution that each one provides. The 5 controls are as follows.',
        'Elimination Is at the top of the diagram and involves physically removing the hazard from the workplace. For example removing a dangerous machine. While this is the most effective means of dealing with a hazard it is also often time the most difficult to implement in an existing process and can have high upfront costs associated with it.',
        'Substitution, like Elimination is at the top of the diagram as a highly effective solution to and existing safety hazard and also requires major changes in equipment and procedures. For example replacing a dangerous machine with a safer alternative. Ideally for Elimination and Substitution controls it is easiest to introduce them in the design and development process before any procedures are already in place.',
        'Engineering controls are used to remove a hazard or place a barrier between workers and a hazard requiring physical change to the workplace like elimination and substitution. For example developing and attaching a machine guard. Well-designed engineering controls can be highly effective in protecting workers and will typically be independent of worker interactions to provide this high level of protection. Initial costs of engineering controls can be higher than administrative of PPE but have high long term return on investment that could include lower operating costs and higher productivity.',
        'Administrative controls are all about identifying and implementing procedures to help your workers conduct their job in a safe manner. This may often include reduction in time a worker is exposed to the hazard, increasing safety signage, performing a risk assessment and making sure all employees are properly trained.',
        'PPE controls are the last line of defense. When all other controls have been considered and ruled out PPE should be used. For example when a job requires working at heights make sure to provide the proper fall protection, make sure it fits the worker properly, train them on how to properly use the fall protection, and make sure that they use it to prevent injury should a fall occur.',
        'All of these controls should be properly weighted in order to find the right solution to fit your working environment before a decision is made. For More Information visit the CDC webpage at http://www.cdc.gov/niosh/topics/engcontrols/'
    );

    public $oshaViolators = array(
        'Fool me once shame on you fool me twice and Iâ€™ll be watching you. In an effort to cut down on the amount of companies that continue to willfully and repeatedly abate violations, putting their workers at risk, OSHA has implemented the Severe Violators Program. This program was established to concentrate resources on inspecting employers who have not held up their end of the workplace safety bargain and continue to show indifference in their safety obligations. This program replaced the Enhanced Enforcement Program from 2008.',
        'Enforcement actions for severe violatorâ€™s cases include mandatory follow-up inspections, increased company/corporate awareness of OSHA enforcement, corporate-wide agreements, where appropriate, enhanced settlement provisions, and federal court enforcement under Section 11(b) of the OSH Act. In addition, this Instruction provides for nationwide referral procedures, which includes OSHAâ€™s State Plan States.',
        'Criteria for a Severe Violator Enforcement Case are as follows:',
        'Fatality/Catastrophe Criterion: A fatality/catastrophe inspection in which OSHA finds one or more willful or repeated violations or failure-to-abate notices based on a serious violation related to a death of an employee or three or more hospitalizations.',
        'Non-Fatality/Catastrophe Criterion Related to High-Emphasis Hazards: An inspection in which OSHA finds two or more willful or repeated violations or failure-to-abate notices (or any combination of these violations/notices), based on high gravity serious violations related to a High-Emphasis Hazard as defined in Section XII.',
        'Non-Fatality/Catastrophe Criterion for Hazards Due to the Potential Release of a Highly Hazardous Chemical (Process Safety Management): An inspection in which OSHA finds three or more willful or repeated violations or failure-to-abate notices (or any combination of these violations/notices), based on high gravity serious violations related to hazards due to the potential release of a highly hazardous chemical, as defined in the PSM standard.',
        'Egregious Criterion: All egregious (e.g., per-instance citations) enforcement actions will be considered SVEP cases.',
        'Any inspection that meets one or more of these criteria can be considered as a severe violatorâ€™s case. High emphasis hazards include Fall hazards, amputation hazards, combustible dust hazards, crystalline silica hazards, lead hazards, evacuation/trenching hazards, and shipbreaking hazards. If OSHA has already cited you for one of the following be sure to take the proper steps to rid your workplace of the hazard so you donâ€™t land your company on the OSHA Severe Violators List.',
        'To view the OSHA Severe Violators Directive in its entirety click here: https://www.osha.gov/dep/svep-directive.pdf'
    );
    public $safetyCommittee = array(
        'Encouraging workers to take an interest in the administrative side of their work is not always an easy sell. Many have the mentality of wanting change but having someone else make it happen. Getting a workplace to come together to make a facility a more enjoyable and safe environment is important to its success long term and a safety committee is a great place to start.',
        'Creating a safety committee in your business brings together all departments for the common goal of increasing communication and improving working conditions. A complete safety committee should consist of representatives from areas of your business such as upper management, safety managers, workers on the floor, workers in the field, shipping, facility maintenance, and administration.',
        'A safety committee should: encourage employees to take ownership of how the business runs, help reduce the risk of workplace injuries and illnesses and ensure compliance. When first starting a committee it can be a source of frustration if not handled correctly, donâ€™t let hostility and failure of management occur in your committee.',
            'Set an agenda',
            'Meet monthly same time, same day, same place',
            'Provide a forum for healthy issue recognition discussion and resolution in a collaborative environment',
            'Set goals and apply duties that fall within the committees responsibilities, duties could include training, safety inspections, reviewing safety policies',
            'Have members come to the meeting with questions or topics of discussion to jumpstart interaction',
            'Some state-plan OSHA states may require safety committees with specific provisions; company execs should verify with their regional OSHA office whether employers must have a safety committee.',
        'The end goal of every member should be the same; to make their workplace safety and more productive. Use this goal as a solid foundation to keep everyone on the same page. Safety committees are a proven way to increase awareness communication and safety in a workplace. If your program has been derailed try implementing a committee to get it headed in the right direction for 2015.'
        );
    public $oshaRegulation = array(
        'Whatâ€™s new and changing with OSHA regs in 2015? As with any New Year comes time for reflection, new goals and areas for improvement. And like anyone else OSHA has some plans for 2015. This year will bring with it new regulations in the beginning middle and end and a Regulatory Agenda full of industry pain points, gaps and severe violator enforcement. It will be an important year to keep up with OSHA.',
        'Once the clock hits 12 on New Yearâ€™s the new standards for injury & illness reporting go into effect. What you need to know about the updates to this record keeping rule is simple.',
        'What needs to be reported to OSHA?',
        'All employers must report: all work-related fatalities within 8 hours, all work-related inpatient hospitalizations, all amputations and all losses of an eye within 24 hours.',
        'Reports to OSHA can be made by calling OSHAâ€™s free and confidential number at 1-800-321-OSHA (6742), calling your closest Area Office during normal business hours or using the new online form that will soon be available.',
        'Only fatalities occurring within 30 days of the work-related incident must be reported to OSHA. Further, for an in-patient hospitalization, amputation or loss of an eye, these incidents must be reported to OSHA only if they occur within 24 hours of the work-related incident.',
        'Who is exempt from keeping records?',
        'There are two classes of employers that are partially exempt from routinely keeping records. Employers with ten or fewer employees at all times during the previous calendar year which remains unchanged from the previous regulation. Also, establishments in certain low-hazard industries are also exempt from routinely keeping OSHA injury and illness records. For this newly updated list visit: https://www.osha.gov/recordkeeping/ppt1/RK1exempttable.html.',
        'Attached is also a list of newly added industries that must keep records: https://www.osha.gov/recordkeeping2014/reporting_industries.html',
        'GHS deadlines will also greet industries in 2015. June 1, 2015 will require all manufacturers and distributors to have complied with all modified provisions of the final rule, including:',
        'Have SDS (Safety Data Sheets) on file for each hazardous chemical available',
        'Train employees about new elements and formatting for potential hazards',
        'Have a written hazard communication program for employees and visitors',
        'Properly label hazardous chemical containers',
        'Maintain reports, records and logs about the entire program',
        'Report this information to a state agency (if required by your state)',
        'December will round out the year for GHS deadlines, starting on the first of the month Distributors will no longer be able to ship products with old system labels, and all new shipments must be up to date with the new standards.',
        'Other agenda areas to look out from OSHA in 2015 include a few final rules on confined spaces in construction in the month of March where they currently only exist for general industry, and further into the year in June OSHA will look to try and finalize Walking Working Surfaces and Personal Fall Protection Systems rules that have been open since 1990.',
        'OSHA has taken an ambitious stance for the upcoming year which should send the message to all about how they are making worker safety their main priority in all industries. With an agenda full of final rules, proposed rules and strengthened enforcement efforts, 2015 will not be a good year to fall behind on regulatory information that applies to your business.',
        'Think you might need some help sorting through all the information? Contact Emedco about our client services today, and as always follow the Emedco blog for safety news notifications and important information on workplace safety.'
        );



    // tests
    public function content_emedcoBlog(WebGuy $I)
    {
        $I->wantTo('check that content of the blog is migrated');
        $I->expectTo('see emedco blog content only ');
        $I->amOnPage('/blog');
        $I->canSeeElement('.content.cf');
        $I->canSeeElement('.main-content.f-left');
        //GHS
        $I->canSeeElement('#post-2140.post-2140.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions');
        $I->canSeeElement('//img[@src="http://emedco.zeondemo.com/blog/wp-content/uploads/2015/02/eme15_news01-hero.jpg"]');
        $I->canSeeInPageSource(self::$title_GHS,'#post-2140 > div:nth-child(1) > div:nth-child(2) > h2:nth-child(1) > a:nth-child(1)');
        $I->canSee('comments','#post-2140 > div:nth-child(1)');
        $I->canSeeInPageSource('Share on Facebook.','#post-2140 > div:nth-child(1) > div:nth-child(2) > footer:nth-child(4) > span:nth-child(3) > a:nth-child(1)');
        $I->click('#post-2140 > div:nth-child(1) > div:nth-child(2) > footer:nth-child(4) > a:nth-child(1)');
        $I->wait(5);
        $I->seeInCurrentUrl('/blog/?p=2140');
        #$I->canSee('Posted','#post-2140 > div:nth-child(1)');
        #$I->canSee(self::$date_GHS,'#post-2140 > div:nth-child(1)');
        #$I->canSee('emedco','#post-2140 > div:nth-child(1)');
        foreach ($this->GHS as $value){
            #$I->seeInPageSource($value);
            $I->see($value);
        };
        $I->canSeeElement(self::$fb);
        $I->canSeeElement(self::$twitter);
        $I->canSeeElement(self::$google);
        $I->canSeeElement(self::$Email);

  /*      //BURN
        $I->canSeeElement('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2014/10/news.jpg"]');
        $I->canSeeElement('#post-2141.post-2141.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions div.post-entry div.post-entry__content');
        $I->canSee('#post-2140.post-2140.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions',self::$title_Burn);
        $I->canSee('#post-2141.post-2141.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions','Posted');
        $I->canSee('#post-2141.post-2141.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions',self::$date_Burn);
        $I->canSee('#post-2141.post-2141.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions','emedco');
        foreach ($this->Burn as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2141.post-2141.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions footer.postmetadata','Posted in');
        $I->canSee('#post-2141.post-2141.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions footer.postmetadata',self::$footer_Burn);

        //Harmful Chemicals
        $I->canSeeElement('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals div.post-entry div.post-entry__content');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2015/01/eme15_news02-neverseecoming.jpg"]');
        $I->canSeeElement('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content');

        $I->canSee('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals div.post-entry div.post-entry__content',self::$title_harmfulChemicals);
        $I->canSee('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals div.post-entry div.post-entry__content','Posted');
        $I->canSee('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals div.post-entry div.post-entry__content',self::$date_harmfulChemicals);
        $I->canSee('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals div.post-entry div.post-entry__content','emedco');
        foreach ($this->harmfulChemicals as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals footer.postmetadata','Posted in');
        $I->canSee('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals footer.postmetadata',self::$footer_harmfulChemicals);
        $I->canSee('#post-2139.post-2139.post.type-post.status-publish.format-standard.has-post-thumbnail.hentry.category-safety-security-solutions.tag-toxic-chemicals footer.postmetadata',self::$tag_harmfulChemicals);

        //WORKPLACE SAFETY
        $I->canSeeElement('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2014/10/news.jpg"]');
        $I->canSeeElement('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content div.post-entry__maincontent');

        $I->canSee('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content',self::$title_workplaceSafety);
        $I->canSee('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content','Posted');
        $I->canSee('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content',self::$date_workplaceSafety);
        $I->canSee('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content','emedco');
        foreach ($this->workplaceSafety as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata','Posted in');
        $I->canSee('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata',self::$footer_workplaceSafety);
        $I->canSee('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata',self::$tag_workplaceSafety);

        //WORKING OUTDOORS
        $I->canSeeElement('#post-2137.post-2137.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-working-outdoors');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2015/01/shutterstock_93849034.jpg"]');
        $I->canSeeElement('#post-2138.post-2138.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content div.post-entry__maincontent');

        $I->canSee('#post-2137.post-2137.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-working-outdoors div.post-entry div.post-entry__content h2.post-entry__title',self::$title_workingOutdoors);
        $I->canSee('#post-2137.post-2137.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-working-outdoors div.post-entry div.post-entry__content','Posted');
        $I->canSee('#post-2137.post-2137.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-working-outdoors div.post-entry div.post-entry__content',self::$date_workingOutdoors);
        $I->canSee('#post-2137.post-2137.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-working-outdoors div.post-entry div.post-entry__content','emedco');
        foreach ($this->workingOutdoors as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2137.post-2137.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-working-outdoors footer.postmetadata','Posted in');
        $I->canSee('#post-2137.post-2137.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-working-outdoors footer.postmetadata',self::$footer_workingOutdoors);

        //WORKPLACE SAFETY IMPROVEMENT
        $I->canSeeElement('#post-2136.post-2136.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety-improvement');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2015/01/shutterstock_151358939.jpg"]');
        $I->canSeeElement('#post-2136.post-2136.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety-improvement div.post-entry div.post-entry__content');

        $I->canSee('#post-2136.post-2136.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety-improvement div.post-entry div.post-entry__content h2.post-entry__title',self::$title_workplaceSafety_improvement);
        $I->canSee('#post-2136.post-2136.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety-improvement div.post-entry div.post-entry__content','Posted');
        $I->canSee('#post-2136.post-2136.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety-improvement div.post-entry div.post-entry__content',self::$date_workplaceSafety_improvement);
        $I->canSee('#post-2136.post-2136.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety-improvement div.post-entry div.post-entry__content','emedco');
        foreach ($this->workplaceSafetyImprovement as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2136.post-2136.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety-improvement footer.postmetadata','Posted in');
        $I->canSee('#post-2136.post-2136.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety-improvement footer.postmetadata',self::$footer_workplaceSafety_improvement);

        //WORKPLACE SAFETY CONTROLS
        $I->canSeeElement('#post-2134.post-2134.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2015/01/shutterstock_77002051.jpg"]');
        $I->canSeeElement('#post-2134.post-2134.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content');

        $I->canSee('#post-2134.post-2134.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content h2.post-entry__title',self::$title_workplaceSafety_controls);
        $I->canSee('#post-2134.post-2134.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content div.post-entry__posted','Posted');
        $I->canSee('#post-2134.post-2134.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content div.post-entry__posted',self::$date_workplaceSafety_controls);
        $I->canSee('#post-2134.post-2134.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content div.post-entry__posted','emedco');
        foreach ($this->workplaceSafetyControls as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2134.post-2134.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata','Posted in');
        $I->canSee('#post-2134.post-2134.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata',self::$footer_oshaViolators);

        //OSHA VIOLATORS
        $I->canSeeElement('#post-2133.post-2133.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2015/01/shutterstock_135106391.jpg"]');
        $I->canSeeElement('#post-2133.post-2133.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content');

        $I->canSee('#post-2133.post-2133.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content h2.post-entry__title',self::$title_oshaViolators);
        $I->canSee('#post-2133.post-2133.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content','Posted');
        $I->canSee('#post-2133.post-2133.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content',self::$date_oshaViolators);
        $I->canSee('#post-2133.post-2133.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content','emedco');
        foreach ($this->oshaViolators as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2133.post-2133.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata','Posted in');
        $I->canSee('#post-2133.post-2133.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata',self::$footer_oshaViolators);

        //SAFETY COMMITTEE
        $I->canSeeElement('#post-2132.post-2132.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2015/01/shutterstock_104473637.jpg"]');
        $I->canSeeElement('#post-2132.post-2132.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content');

        $I->canSee('#post-2132.post-2132.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content h2.post-entry__title',self::$title_safetyCommittee);
        $I->canSee('#post-2132.post-2132.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content','Posted');
        $I->canSee('#post-2132.post-2132.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content',self::$date_safetyCommittee);
        $I->canSee('#post-2132.post-2132.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content','emedco');
        foreach ($this->safetyCommittee as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2132.post-2132.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata','Posted in');
        $I->canSee('#post-2132.post-2132.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata',self::$footer_safetyCommittee);

        //OSHA REGULATION
        $I->canSeeElement('#post-2131.post-2131.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety');
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2014/12/shutterstock_114196144.jpg"]');
        $I->canSeeElement('#post-2131.post-2131.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content');

        $I->canSee('#post-2131.post-2131.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content h2.post-entry__title',self::$title_oshaRegulations);
        $I->canSee('#post-2131.post-2131.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content','Posted');
        $I->canSee('#post-2131.post-2131.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content',self::$date_oshaRegulations);
        $I->canSee('#post-2131.post-2131.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety div.post-entry div.post-entry__content','emedco');
        foreach ($this->oshaRegulation as $value){
            $I->seeInPageSource($value);
        };
        $I->canSee('#post-2131.post-2131.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata','Posted in');
        $I->canSee('#post-2131.post-2131.post.type-post.status-publish.format-standard.hentry.category-safety-security-solutions.tag-workplace-safety footer.postmetadata',self::$footer_safetyCommittee);
  */
    }



}