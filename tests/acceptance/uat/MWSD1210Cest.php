<?php
namespace uat;
use \WebGuy;

class MWSD1210Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CheckRel(WebGuy $I)
    {
        $I->wantToTest('Rel for prev and next product list');
        $I->amOnPage('/signs/traffic-parking-signs/traffic-signs.html');
        $I->expect('Canonical to see next and do not see prev');
        $I->canSeeInPageSource('href="http://emedco.zeondemo.com/signs/traffic-parking-signs/traffic-signs.html?p=2"');
        $I->cantSeeInPageSource('<link rel="prev"');
        $I->amOnPage('/signs/traffic-parking-signs/traffic-signs.html?p=2');
        //$I->fillField('#page','2');
        //$I->pressKey('#page',\WebDriverKeys::ENTER);
        $I->expect('Canonical to see next p3 and see prev');
        $I->wait(4);
        $I->canSeeInPageSource('href="http://emedco.zeondemo.com/signs/traffic-parking-signs/traffic-signs.html"');
        $I->canSeeInPageSource('href="http://emedco.zeondemo.com/signs/traffic-parking-signs/traffic-signs.html?p=3"');
        $I->amOnPage('/signs/traffic-parking-signs/traffic-signs.html?p=11');
//        $I->fillField('#page','11');
//        $I->pressKey('#page',\WebDriverKeys::ENTER);
        $I->expect('Canonical to do not see next p3 and see prev p10');
        $I->wait(4);
        $I->canSeeInPageSource('href="http://emedco.zeondemo.com/signs/traffic-parking-signs/traffic-signs.html?p=10"');
        $I->cantSeeInPageSource('href="http://emedco.zeondemo.com/signs/traffic-parking-signs/traffic-signs.html?p=12"');
    }
}