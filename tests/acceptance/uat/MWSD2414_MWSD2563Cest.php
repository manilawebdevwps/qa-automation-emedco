<?php
namespace uat;
use \WebGuy;

class MWSD2414_MWSD2563Cest
{
    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function bestPagesRound4(WebGuy $I)
    {
        $I->wantTo('get url from DB');
        $I->expectTo('see the URLs with noindex, nofollow meta tag');
        $I->changeBaseURL('http://www.emedco.com/');                            //FOR TESTING ONLY

        $filename = 'DeIndex_TestResult';

        $results = $I->getArrayFromDB('best_pages_remove_batch4','*',array());

        $dateTimeTested = date("m-d-Y H:i");
        $arr = explode(" ",$dateTimeTested);
        $dateTested = $arr[0];

        foreach ($results as $key=>$val) {
            $sn = ($val['sn']);
            $url = $I->truncate($val['url']);
            $I->amOnPage($url);
            $I->wait(5);
            //$I->seeInPageSource('content="index, follow" ');      //FOR TESTING ONLY - OK
            $I->canSeeInPageSource('content="noindex, nofollow"');  //FOR TESTING ONLY - OK
            $I->LogTestResult($filename,$url,$sn,$dateTested);
        }
    }

}