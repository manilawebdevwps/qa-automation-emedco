<?php
namespace uat;
use \WebGuy;

class MWSD2462Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function findRRhome(WebGuy $I)
    {
        $I->wantTo('check rr recommended products');
        $I->expectTo('see the implementation of RR recommended products ');
        $I->seeInField('//*[@id="sku_0"]','Item #');
        $I->seeInField('//*[@id="sku_1"]','Item #');
        $I->seeInField('//*[@id="qty_0"]','1');
        $I->seeInField('//*[@id="qty_1"]','1');
        $I->seeLink('Clear',"javascript:document.getElementById('quickorder-validate-detail').reset();");
        $I->fillField("#sku_0", "DKL30-BL-H-B");
        $I->fillField("#qty_0", "2");
        $I->click('#quickorder-validate-detail input[type=submit]');
        $I->seeInCurrentUrl('/checkout/cart/');
        $I->see('/html/body/div[1]/div/div/div/div[3]/div[1]/div/div/div/div/fieldset/table/tbody/tr[2]/td[1]/div/div[2]/span/label','Item #:');

    }
}

//*[@id="rr_home_ul"]