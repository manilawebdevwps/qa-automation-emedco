<?php
namespace uat;
use \WebGuy;

class MWSD2724_MWSD3198Cest
{

    /*Emedco Blog: Issues stopping us from going in front of customers - Staging only*/

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function blog_resultCount(WebGuy $I)
    {
        $I->wantTo('check the count of search result');
        $I->amOnPage('/blog');
        $I->fillField('#s','ghs');
        $I->click('#searchsubmit');
        $I->wait('5');
        $I->canSeeInCurrentUrl('blog/?s=ghs');
        $resultCount = $I->grabTextFrom('h2');
        $I->see($resultCount);
        //Search Results for ghs ΓÇö 33 articles found
        $cleanedText = $I->captureText($resultCount);
        $I->see($cleanedText);

    }
}