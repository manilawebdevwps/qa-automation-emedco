<?php
namespace uat;
use \WebGuy;

class MWSD1822Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function tryToTest(WebGuy $I)
    {
        $I->wantToTest('test that TRUSTGUARD is removed from Shopping cart');
        $I->expectTo('see that shopping cart has no TRUSTGUARD visible in the page');
        $I->amOnPage('checkout/cart/');
        $I->seeElement('img[alt="VeriSign Secured"]');
        $I->seeElement('img[alt="BBB Accredited"]');
        $I->dontSeeElement('img[alt="Privacy Verified"]');
        $I->dontSeeElement('img[alt="Security Scanned"]');
    }
}