<?php
namespace uat;
use \WebGuy;

class MWSD3308_MWSD3388Cest
{

    /*  Emedco - Sweepstakes form page  */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }


    public function _errorMessage(WebGuy $I)
    {
        $I->click('Enter Now');
        $I->canSee('All fields must be completed to enter','');
        //First name
        $I->fillField('#lastname','Ranada');
        $I->fillField('#companyname','Brady');
        $I->fillField('#phonenumber','1234567890');
        $I->fillField('#email','lilybeth_ranada@bradycorp.com');
        $I->fillField('#customernumber','1234567890');
        $I->click('Enter Now');
        $I->canSee('All fields must be completed to enter','');
        //Last name
        $I->fillField('#firstname','Lilybeth');
        $I->fillField('#lastname','');
        $I->click('Enter Now');
        $I->canSee('All fields must be completed to enter','');
        //company name
        $I->fillField('#lastname','Ranada');
        $I->fillField('#companyname','');
        $I->click('Enter Now');
        $I->canSee('All fields must be completed to enter','');
        //Phone number
        $I->fillField('#phonenumber','');
        $I->fillField('#companyname','Brady');
        $I->click('Enter Now');
        $I->canSee('All fields must be completed to enter','');
        //Email
        $I->fillField('#email','');
        $I->fillField('#phonenumber','1234567890');
        $I->click('Enter Now');
        $I->canSee('All fields must be completed to enter','');
        //Customer number
        $I->fillField('#email','lilybeth_ranada@bradycorp.com');
        $I->fillField('#customernumber','');
        $I->click('Enter Now');
        $I->canSee('All fields must be completed to enter','');
    }

    public function _privacyPolicy(WebGuy $I)
    {
        $I->click('Privacy Policy');
        $I->canSeeCurrentUrlMatches('/privacy-policy');
    }

    public function _whereDoIFindThis(WebGuy $I)
    {
        $I->click('Where do I find this?');
        $I->switchToWindow();
        $I->canSee('Where do I find my customer number?');
        $I->canSeeElement('#customerNumber');
        $I->canSee('Customer Number');
    }

    // tests
    public function sweepstakes(WebGuy $I)
    {
        $I->wantTo('enter in the sweepstakes form');
        $I->expectTo('see input information in sweepstakes form');
        //$I->changeBaseURL('http://www.emedco.com/');
        $I->amOnPage('/entertowin');
        $I->wait('5');
        $I->canSee('Enter to Win!','h1');
        $I->canSee('Sign up for our FREE safety newsletter and be entered to win a MacBook Air.','');
        $I->canSee('First Name','');
        $I->canSee('Last Name','');
        $I->canSee('Company Name','');
        $I->canSee('Phone Number','');
        $I->canSee('Email','');
        $I->canSee('Customer Number','');
        $I->canSee('Where do I find this?','');
        $I->canseeOptionIsSelected('','By entering you will also be signing up to receive the Emedco Newsletter.','');
        $I->cantSeeOptionIsSelected ('#age','I confirm that I am 18 or older(see official rules below).');
        $I->checkOption('I confirm that I am 18 or older(see official rules below).');
        //Thank You For Entering
        $I->click('Enter Now');
        $I->canSee('Thank You For Entering!');
        $I->canSee('One winner will be selected on 3/1/16 and will be notified by email');
        //Start Shopping
        $I->click('Start Shopping');
        $I->wait(5);
        $I->canSeeCurrentUrlMatches('/');
        //Privacy Policy
        $I->$this->_privacyPolicy();
        $I->moveBack();
        $I->wait(5);
        $I->canSeeCurrentUrlMatches('/');
        //Where Do I Find this?
        $I->$this->_whereDoIFindThis();
        $I->moveBack();
        $I->wait(5);
        $I->canSeeCurrentUrlMatches('/');
        //Function Test
        $I->$this->_errorMessage();

    }


    //check database created tru webhelper - Asked Cian





}