<?php
namespace uat;
use \WebGuy;
use \Codeception\Util\Locator;
/**
 * @guy WebGuy\UserSteps
 */

class MWSD1066Cest
{

    public function _before()
    {

    }

    public function _after()
    {
    }

    // tests
    public function HeaderContainerSigns(WebGuy\UserSteps $I) {
        $I->wantTo('To check Header Container for Signs');
        $I->check404page('maxymiser/level1_signs_labels/preview-signs.html');
        $I->expectTo('See Level 1 Header');
        $I->canSee('Signs','h1');
        $I->expectTo('See Category Banner');
        $I->canSeeElement('.category-banner-max');
        $I->expectTo('See Sign Finder');
        $I->canSeeElement('.sign-finder');
        $I->expectTo('See Category List View');
        $I->canSeeElement('.category-view-max');

    }

    public function CategoryLinksSignsLevel(WebGuy $I) {

        $I->wantTo('To check Sub Category Links');
        $I->expectTo('See Level 2 & 3 Category Links');
        $I->canSeeLink('SAFETY SIGNS','signs/safety-signs.html');
        $I->canSeeLink('Chemicals, Flammables & Hazcom Signs','signs/safety-signs/chemicals-flammables-hazcom.html');
        $I->canSeeLink('Protective Wear (PPE) Signs','signs/safety-signs/protective-wear-ppe.html');
        $I->canSeeLink('Machine, Equipment & Hazard Zone Signs','signs/safety-signs/machine-equipment-hazard-zones.html');
        $I->canSeeLink('Warehouse Safety Signs','signs/safety-signs/warehouse-safety.html');

        $I->canSeeLink('TRAFFIC AND PARKING SIGNS','signs/traffic-parking-signs.html');
        $I->canSeeLink('Traffic Signs','signs/traffic-parking-signs/traffic-signs.html');
        $I->canSeeLink('Parking Signs','signs/traffic-parking-signs/parking-signs.html');

        $I->canSeeLink('CUSTOM SIGNS','signs/custom.html');
        $I->canSeeLink('Custom Safety Signs','signs/custom/safety.html');
        $I->canSeeLink('Custom Parking & Traffic Signs','signs/custom/custom-parking-traffic-signs.html');
        $I->canSeeLink('Other Custom Signs','signs/custom/other-custom.html');

        $I->canSeeLink('EXIT, FIRE AND EMERGENCY SIGNS','signs/exit-fire-and-emergency.html');
        $I->canSeeLink('Non-Glow Exit & Fire Signs','signs/exit-fire-and-emergency/non-glow-exit-fire.html');
        $I->canSeeLink('Glow in the Dark Exit & Fire Signs','signs/exit-fire-and-emergency/glow-in-the-dark-exit-fire.html');
        $I->canSeeLink('Electrical Exit Signs','signs/exit-fire-and-emergency/electrical-exit-signs.html');
        $I->canSeeLink('Evacuation & Shelter Signs','signs/exit-fire-and-emergency/evacuation-shelter.html');

        $I->canSeeLink('PROPERTY AND SECURITY SIGNS','signs/property-security-signs.html');
        $I->canSeeLink('Standard Grade Security Signs (Indoor & Outdoor)','signs/property-security-signs/standard-grade-security-signs-indoor-outdoor.html');
        $I->canSeeLink('Outdoor & Heavy-Duty Security Signs','signs/property-security-signs/outdoor-heavy-duty-security-signs.html');
        $I->canSeeLink('Specialty Security Signs','signs/property-security-signs/specialty-security-signs.html');
        $I->canSeeLink('Interior Decor, Indoor Security Signs','signs/property-security-signs/interior-decor-indoor-security-signs.html');

        $I->canSeeLink('INTERIOR AND OFFICE SIGNS','signs/interior-office.html');
        $I->canSeeLink('Engraved Signs & Nameplates','signs/interior-office/engraved-signs-and-nameplates.html');
        $I->canSeeLink('Interior Sign Systems','signs/interior-office/interior-sign-systems.html');
        $I->canSeeLink('ADA & Braille Signs','signs/interior-office/ada-braille.html');

        $I->canSeeLink('SIGN POSTS, STANCHIONS AND ACCESSORIES','signs/sign-posts-stanchions-accessories.html');
        $I->canSeeLink('Sign Stanchions & Bases','signs/sign-posts-stanchions-accessories/sign-stanchions.html');
        $I->canSeeLink('Sign Posts','signs/sign-posts-stanchions-accessories/sign-posts.html');
        $I->canSeeLink('Post Mounting & Installation','signs/sign-posts-stanchions-accessories/post-mounting-and-installation.html');

        $I->canSeeLink('SMOKING SIGNS','signs/smoking-designation.html');
        $I->canSeeLink('Standard No Smoking Signs','signs/smoking-designation/standard-no-smoking-signs.html');
        $I->canSeeLink('State Regulatory Smoking Law Signs','signs/smoking-designation/state-regulatory-smoking-laws.html');
        $I->canSeeLink('No Smoking Labels for Glass Doors & Windows','signs/smoking-designation/glass-door-window-signs.html');
        $I->canSeeLink('Other Smoking Signs & Posters','signs/smoking-designation/other-smoking-signs-posters.html');

        $I->canSeeLink('OTHER SIGNS','signs/other-signs.html');
        $I->canSeeLink('Floor Signs & Markers','signs/other-signs/floor-signs-markers.html');
        $I->canSeeLink('Safety Posters & Wallcharts','signs/other-signs/posters-wallcharts.html');
        $I->canSeeLink('Banners','signs/other-signs/banners.html');
    }

    public function HeaderContainerLabels(WebGuy\UserSteps $I) {
        $I->wantTo('To check Header Container for Labels');
        $I->check404page('maxymiser/level1_signs_labels/preview-labels.html');
        $I->expectTo('See Level 1 Header');
        $I->canSee('Labels','h1');
        $I->expectTo('See Category Banner');
        $I->canSeeElement('.category-banner-max');
        $I->expectTo('See Category List View');
        $I->canSeeElement('.category-view-max');

    }
    public function CategoryLinksLabels(WebGuy $I) {

        $I->wantTo('To check Sub Category Links');
        $I->expectTo('See Level 2 & 3 Category Links');
        $I->canSeeLink('SAFETY LABELS','labels/safety-labels.html');
        $I->canSeeLink('Machine, Equipment, & Hot Hazard Labels','labels/safety-labels/machine-equipment-hot-hazards.html');
        $I->canSeeLink('Electrical Safety, Lockout & Arc Flash Labels','labels/safety-labels/electrical-safety-lockout-arc-flash.html');
        $I->canSeeLink('Chemical Hazards & Right-to-Know Labels','labels/safety-labels/chemical-hazards-right-to-know.html');

        $I->canSeeLink('CUSTOM LABELS','labels/custom-labels-design-on-line.html');
        $I->canSeeLink('Custom Roll Labels (Paper & Vinyl)','labels/custom-labels-design-on-line/roll-labels-paper-vinyl.html');
        $I->canSeeLink('Custom Safety Labels','labels/custom-labels-design-on-line/safety-labels/html');

        $I->canSeeLink('PIPE MARKERS AND PIPE LABELS','labels/pipe-markers-pipe-labels.html');
        $I->canSeeLink('Self-Adhesive Pipe Markers & Labels','labels/pipe-markers-pipe-labels/self-adhesive.html');
        $I->canSeeLink('Snap-Around Pipe Markers','labels/pipe-markers-pipe-labels/snap-around-markers.html');
        $I->canSeeLink('Custom Pipe Markers & Labels','labels/pipe-markers-pipe-labels/custom-markers.html');
        $I->canSeeLink('Banding Tapes','labels/pipe-markers-pipe-labels/banding-tapes.html');

        $I->canSeeLink('OTHER LABELS','labels/other-labels.html');
        $I->canSeeLink('Hard Hat, Safety Slogan & Certification Labels','labels/other-labels/hard-hat-safety-slogan-certification.html');
        $I->canSeeLink('Recycling & Trash Disposal Labels','labels/other-labels/recycling-trash-disposal.html');
        $I->canSeeLink('Label Makers & Accessories','labels/other-labels/label-makers-accessories.html');

        $I->canSeeLink('INVENTORY CONTROL AND WAREHOUSE LABELS','labels/inventory-control-warehouse-id.html');
        $I->canSeeLink('Warehouse Bar Code Labels','labels/inventory-control-warehouse-id/warehouse-bar-code-labels.html');
        $I->canSeeLink('Number & Letter Labels','labels/inventory-control-warehouse-id/number-letters.html');
        $I->canSeeLink('Bin, Cabinet & Shelf ID Labels','labels/inventory-control-warehouse-id/bin-cabinet-shelf-id.html');

        $I->canSeeLink('SHIPPING LABELS','labels/shipping-labels.html');
        $I->canSeeLink('Hazardous Waste Labels','labels/shipping-labels/hazardous-waste.html');
        $I->canSeeLink('D.O.T. Compliance Labels','labels/shipping-labels/d-o-t-compliance.html');
        $I->canSeeLink('Package Handling Labels','labels/shipping-labels/package-handling.html');
        $I->canSeeLink('Specialty Shipping Labels','labels/shipping-labels/specialty-shipping-labels.html');

        $I->canSeeLink('ASSET ID LABELS','labels/asset-id.html');
        $I->canSeeLink('Aluminum Asset Labels & Plates','labels/asset-id/aluminum-labels-plates.html');
        $I->canSeeLink('Polyester & Vinyl Asset Labels','labels/asset-id/polyester-vinyl-labels.html');
        $I->canSeeLink('Tamper Evident Asset Labels','labels/asset-id/tamper-evident-labels.html');

        $I->canSeeLink('CALIBRATION LABELS & QUALITY CONTROL LABELS','labels/calibration-inspection-quality-control.html');
        $I->canSeeLink('Calibration, Maintenance & Quality Control Labels','labels/calibration-inspection-quality-control/calibration-maintenance-quality-control.html');
        $I->canSeeLink('Production & Machine Status Labels','labels/calibration-inspection-quality-control/production-and-machine-status.html');
        $I->canSeeLink('Inspection Record Labels','labels/calibration-inspection-quality-control/inspection-record.html');
        $I->canSeeLink('Void Labels','labels/calibration-inspection-quality-control/void.html');

        $I->canSeeLink('NO SMOKING LABELS','labels/no-smoking-smoking-designation.html');

        $I->canSeeLink('PARKING PERMITS','labels/parking-permits.html');
        $I->canSeeLink('Custom Parking Permits','labels/parking-permits/custom-permits.html');
        $I->canSeeLink('Parking Warning Labels','labels/parking-permits/parking-warning-labels.html');
        $I->canSeeLink('Specialty Parking Permits','labels/parking-permits/specialty-permits.html');
    }
}