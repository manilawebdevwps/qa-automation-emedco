<?php
namespace uat;
use \WebGuy;

class MWSD2486_MWSD2585Cest
{
    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }


    /* SAMPLE PRODUCT & PDF */
    /*
     * http://www.emedco.com/media/staticImages/pdfs/W_9_Emedco.pdf?v=040115
     * http://www.personnelconcepts.com/form_pdfs/W9.pdf
     */

    // tests
    public function PDF(WebGuy $I)
    {
        $I->wantTo('test PDF merchandising icon in product page ');
        $I->expectTo('see container of the merchandising PDF in frontend');
        $I->amOnPage('/return-to-vendor-inventory-tag-pti50.html');
        //if PDF is added in the admin
        $I->canSeeElement('.prod-pdf');
        $I->canSee('Additional Product Information','.prod-pdf > strong:nth-child(1)');
        $I->canSeeInPageSource('http://www.emedco.com/media/staticImages/pdfs/W_9_Emedco.pdf?v=040115');
    }
}