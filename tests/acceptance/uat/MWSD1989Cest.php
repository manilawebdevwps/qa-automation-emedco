<?php
namespace uat;
use \WebGuy;

class MWSD1989Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function newL1CategoryPage(WebGuy $I)
    {
        $I->wantToTest('L1 category pages bring bacl to previous look');
        $I->expectTo('see that customer favorites/best sellers in L1, L2 category on higher page, installed SEO text');
        $I->amOnPage('/signs.html');
        $I->canSee('SIGNS','h1');
        //$I->see('seo placeholder','');                //FOR UPDATE OF THE CONTENT AFTER THE DEV FINISHED THE PAGE
        $I->canSeeElement('//img[@src="/skin/frontend/enterprise/emedco/images/finding-right-sign.png"]');
        $I->canSeeElement('div.content.firstChild > div.sign-finder.cf');
        $I->canSeeElement('button[type=submit] ');
        $I->canSee('SAFETY SIGNS');
        $I->canSee('TRAFFIC AND PARKING SIGNS');
        $I->canSee('EXIT, FIRE AND EMERGENCY SIGNS');
        $I->canSee('CUSTOM SIGNS');
        $I->canSee('OTHER SIGNS');
        $I->canSee('PROPERTY AND SECURITY SIGNS');
        $I->canSee('SIGN POSTS, STANCHIONS AND ACCESSORIES');
        $I->canSee('INTERIOR AND OFFICE SIGNS');
        $I->canSee('SMOKING SIGNS');
        $I->canSee('Customer Favorites');

        $I->dontSee('Chemicals, Flammables & Hazcom Signs','li.item.first > div.cat-lvl-sub-link > ul.firstChild > li.firstChild > a.add.firstChild');
        $I->dontSee('Protective Wear (PPE) Signs','li.item.first div.cat-lvl-sub-link ul.firstChild li a.add.firstChild');
        $I->dontSee('Machine, Equipment & Hazard Zone Signs','li.item.first div.cat-lvl-sub-link ul.firstChild li a.add.firstChild');

        $I->dontSee('Traffic Signs','li.item div.cat-lvl-sub-link ul.firstChild li.firstChild a.add.firstChild');
        $I->dontSee('Parking Signs','li.item div.cat-lvl-sub-link ul.firstChild li a.add.firstChild');

        $I->dontSee('Non-Glow Exit & Fire Signs','div.cat-lvl-sub-link ul.firstChild li.firstChild a.add.firstChild');
        $I->dontSee('Glow in the Dark Exit & Fire Signs','div.cat-lvl-sub-link ul.firstChild li a.add.firstChild');
        $I->dontSee('Electrical Exit Signs','');
        $I->dontSee('Evacuation & Shelter Signs','');

        $I->dontSee('Custom Safety Signs','');
        $I->dontSee('Custom Parking & Traffic Signs','');
        $I->dontSee('Other Custom Signs','');

        $I->dontSee('Floor Signs & Markers','');
        $I->dontSee('Safety Posters & Wallcharts','');
        $I->dontSee('Banners','');

        $I->dontSee('Standard Grade Security Signs (Indoor & Outdoor)','');
        $I->dontSee('Outdoor & Heavy-Duty Security Signs','');
        $I->dontSee('Specialty Security Signs','');
        $I->dontSee('Interior Decor, Indoor Security Signs','');


        $I->dontSee('Sign Stanchions & Bases','');
        $I->dontSee('Sign Posts','');
        $I->dontSee('Post Mounting & Installation','');

        $I->dontSee('Engraved Signs & Nameplates','');
        $I->dontSee('Interior Sign Systems','');
        $I->dontSee('ADA & Braille Signs','');

    }
}