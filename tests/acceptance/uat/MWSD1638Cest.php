<?php
namespace uat;
use \WebGuy;

class MWSD1638Cest
{

    /* Emedco - Update HP */

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function UpdateHP(WebGuy $I)
    {
        $I->wantTo('To check link and descendants of the elements');
        $I->expectTo('see structure of the new homepage');
        //$I->changeBaseURL('http://www.emedco.com');
        $I->amOnPage('/');
        //banner
        $I->seeElement('.main.col1-layout.version3 .banner-area.clearf.firstChild');
        $I->seeElement('.main.col1-layout.version3 .banner-area.clearf.firstChild .hero-banner.left.firstChild .firstChild img');
        $I->seeElement('.main.col1-layout.version3 .banner-area.clearf.firstChild .hero-banner.left.firstChild .firstChild .firstChild');
        //List of Products Row 1 - Left
        $I->seeLink('No Smoking Sign','.col-main .std.firstChild .hp-grid2.clearf.firstChild .left.right-line.firstChild .hp-product.first /no-smoking-sign-40750.html');
        $I->seeLink('No Smoking Sign','.col-main .std.firstChild .hp-grid2.clearf.firstChild .left.right-line.firstChild .hp-product.first /no-smoking-sign-mqs2.html');
        $I->seeLink('SHOP ALL','.col-main .std.firstChild .hp-grid2.clearf.firstChild .left.right-line.firstChild .title-head.firstChild /signs/smoking-designation/standard-no-smoking-signs.html/');
        $I->see('Top Products in No Smoking Signs','.col-main .std.firstChild .hp-grid2.clearf.firstChild .left.right-line.firstChild .title-head.firstChild');
        //Row 1 - Right
        $I->seeLink('Bathroom Closed for Cleaning Sign','.col-main .std.firstChild .hp-grid2.clearf.firstChild .right .hp-product /restroom-closed-for-cleaning-sign-40179.html/');
        $I->seeLink('Bilingual Wash Hands Before Leaving Restroom Sign','.col-main .std.firstChild .hp-grid2.clearf.firstChild .right .hp-product /wash-hands-before-leaving-restroom-sign-bilingual-64304.html/');
        $I->seeLink('SHOP ALL','.col-main .std.firstChild .hp-grid2.clearf.firstChild .right .title-head.firstChild /signs/interior-office/restroom-lunchroom.html');
        $I->see('Top Products in Bathroom Signs','.col-main .std.firstChild .hp-grid2.clearf.firstChild .right .title-head.firstChild');
        //List of Products Row 2 - Left
        $I->seeLink('Best Atlas® Hi-Vis Grip Gloves','.col-main .std.firstChild .hp-grid2.clearf .left.right-line.firstChild .hp-product.first /atlasr-hi-vis-grip-gloves-529.html');
        $I->seeLink('Tillman™ 1414 Drivers Gloves','.col-main .std.firstChild .hp-grid2.clearf .left.right-line.firstChild .hp-product.first /1414-drivers-gloves-741.html');
        $I->seeLink('SHOP ALL','.mm_grid2 mm_row1 .mm_cell2 /safety-compliance/personal-protective-equipment-fall-protection/personal-protective-equipment/hand-protection.html');
        $I->see('Top Products in Gloves & Hand Protection','.col-main .std.firstChild .hp-grid2.clearf .left.right-line.firstChild .title-head.firstChild');
        //Row 2 - Right
        $I->seeLink('International Symbol Labels - Pinch Point','.col-main .std.firstChild .hp-grid2.clearf .right .hp-product /international-symbol-labels-pinch-point-sym7.html');
        $I->seeLink('Pinch Point Warning Markers','.col-main .std.firstChild .hp-grid2.clearf .right .hp-product /pinch-point-warning-markers-sqs4g.html');
        $I->seeLink('SHOP ALL','.col-main .std.firstChild .hp-grid2.clearf.firstChild .right .title-head.firstChild /search/pinch%2Bpoint/');
        $I->see('Top Products in Pinch Point Signs & Labels','.col-main .std.firstChild .hp-grid2.clearf .right .title-head.firstChild');
        //List of Products Row 3
        //$I->seeLink('.col-main .std.firstChild .hp-grid2.clearf /search/nfpa/');
        $I->seeLink('','.col-main .std.firstChild .hp-grid2.clearf .img-block. left.firstChild');
        $I->seeLink('','.col-main .std.firstChild .hp-grid2.clearf .img-block.right');
        //Row 4 - Left
        $I->canSeeLink('TRAFFIC & PARKING SIGNS ›','.col-main .std.firstChild .hp-catlink.clearf .left /signs/traffic-parking-signs.html/');
        //$I->canSeeLink('TAGS  ›','.col-main .std.firstChild .hp-catlink.clearf .left /tags.html/');
        $I->canSeeLink('FACILITY & SITE MAINTENANCE ›','.col-main .std.firstChild .hp-catlink.clearf .left /traffic-parking-signs.html//facility-site-maintenance.html/ns/traffic-parking-signs.html/');
        $I->canSeeLink('SAFETY SIGNS ›','.col-main .std.firstChild .hp-catlink.clearf .left /signs/safety-signs.html/');
        $I->canSeeLink('TRAFFIC CONES, BARRICADES, TAPES ›','.col-main .std.firstChild .hp-catlink.clearf .left /tape-barricades-cones.html/');
        //Row 4 - Right
        $I->canSeeLink('WAREHOUSE SAFETY & INVENTORY ›','.col-main .std.firstChild .hp-catlink.clearf .right /warehouse-safety-inventory.html/');
        $I->canSeeLink('SECURITY SOLUTIONS ›','.col-main .std.firstChild .hp-catlink.clearf .right /security-aids.html/');
        $I->canSeeLink('LABELS ›','.col-main .std.firstChild .hp-catlink.clearf .right /labels.html/');
        $I->canSeeLink('LOCKOUT & ELECTRICAL ›','.col-main .std.firstChild .hp-catlink.clearf .right /lockout-electrical.html/');
        $I->canSeeLink('PARKING LOT & GROUNDS ›','.col-main .std.firstChild .hp-catlink.clearf .right /parking-lot-grounds.html/');
    }

    public function checkPrice(WebGuy $I)
    {

        //check price
        $I->wantTo('To check price of product page and homepage');
        $I->expectTo('see that the prices are the same in product page and homepage');
        $I->amOnPage('/');
        $hpPrice = $I->grabTextFrom('div.hp-grid2:nth-child(1) > div:nth-child(1) > div:nth-child(2) > a:nth-child(1) > div:nth-child(2) > strong:nth-child(1)');
        $I->expectTo('Homepage product price: '.$hpPrice);
        $I->click('.hp-product .firstChild ');
        $I->wait('10');
        $I->seeInCurrentUrl('/no-smoking-sign-40750.html');
        $productPagePrice = $I->grabTextFrom('.mmprice-box.left.t-left.firstChild .price.firstChild');
        $I->expectTo('see price on product page is: '.$productPagePrice);
        if ($hpPrice == $productPagePrice) {
            $I->expectTo('see homepage and product page are the same');
        }else {
            return \PHPUNIT_Framework_Assert::fail("PRICE NOT EQUAL");
        }

        //return to homepage for the second sample
        $I->amOnPage('/');
        $hpPrice = $I->grabTextFrom('div.hp-grid2:nth-child(2) > div:nth-child(2) > div:nth-child(2) > a:nth-child(2) > div:nth-child(2) > strong:nth-child(1)');
        $I->expectTo('Homepage product price: '.$hpPrice);
        $I->click('a[title="Pinch Point Warning Markers"]');
        $I->wait('10');
        $I->seeInCurrentUrl('/pinch-point-warning-markers-sqs4g.html/');
        //$productPagePrice = $I->grabTextFrom('.mmprice-box.left.t-left.firstChild .price.firstChild');
        $productPagePrice = $I->grabTextFrom('span.price.firstChild');
        $I->expectTo('see price on product page is: '.$productPagePrice);
        if ($hpPrice == $productPagePrice) {
            $I->expectTo('homepage and product page are the same');
        }else {
            return \PHPUNIT_Framework_Assert::fail("PRICE NOT EQUAL");
        }








    }



}