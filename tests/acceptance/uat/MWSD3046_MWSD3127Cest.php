<?php
namespace uat;
use \WebGuy;

class MWSD3046_MWSD3127Cest
{

    /* Emedco: Develop magento compatible module from DPP of Andromeda - Staging only */

    public function _before()
    {
    }

    public function _after()
    {
    }

    static $url = 'http://www.emedco.com/';

    // tests
    public function adminLog(WebGuy $I)
    {
        $I->expectTo('find the dynamic price rule in emedco admin');
        $I->amOnPage('/zpanel');
        $I->fillField(\LoginPage::$stgAdminField_username,\LoginPage::$stgAdminValue_username);
        $I->fillField(\LoginPage::$stgAdminField_password,\LoginPage::$stgAdminValue_password);
        $I->click('Login');
        $I->wait(5);
        $I->moveMouseOver(\adminPage::$stgAdminField_promotions);
        $I->wait(5);
        $I->moveMouseOver(\adminPage::$stgAdminField_catPriceRule);
        $I->wait(5);
        $I->click(\adminPage::$stgAdminField_catPriceRule);
        $I->wait(5);
        $I->click(\adminPage::$stgAdminField_addNewRule);
        $I->wait(5);
        $I->click(\adminPage::$stgAdminField_addNewRuleConditions);
        $I->wait(3);
        $I->click(\adminPage::$stgAdminField_ruleParam);
        $I->wait(3);
        $I->canSee('Channel');
        $I->canSee('Url Parameter');
        $I->canSee('Referrer');
    }
}