<?php
namespace uat;
use \WebGuy;

class MWSD1592Cest
{

    /* Emedco level 3 page redesign  */

    public function _before()
    {
    }

    public function _after()
    {
    }



    public $subCatBarricadeTapes = array(
    'Standard Barricade Tapes',
    'Retractable Belt Systems',
    'Reinforced Barricade Tape',
    'Economy Barricade Tape',
    'Underground Warning Markers',
    'Nylon Barricade Tapes',
    'Biodegradable Barricade Tapes',
    'Jumbo 6" Barricade Tape',
    'Recyclable Paper Barricade Tape');


    // tests
    public function L3_NewPage(WebGuy $I){
        $I->wantTo('To check Header Container for Signs');
        $I->expectTo('See Emedco Level 3 page design');
        $I->amOnPage('/tape-barricades-cones/tapes/barricade-tapes.html');
        $I->seeElement('.page');
        $I->see('Barricade Tapes','.content.cf.firstChild .top-tdesc.firstChild .page-title.category-title.firstChild H1');
        $I->see('Barricade tapes are a quick, easy, and economical way to block off or secure dangerous areas in order to help prevent accidents and keep people safe.','.content.cf.firstChild .top-tdesc.firstChild p');
        //removed-filter by category, customer favorite
        $I->canSee('Filter by Category','.lftcrn-rpt .rtcrn-rpt.firstChild .catfilter-content.firstChild p');       //TO ENABLE WHEN APPLN IS DONE
        //check the subcategory by looping
        foreach ($this->subCatBarricadeTapes as $subCategory){
            $I->see($subCategory);
        }

        $I->dontSee('.customer-favorite');                                                                        //TO ENABLE WHEN APPLN IS DONE
        //toolbar
        $I->canSeeElement('.right .toolbar');
        //Products per page
        $I->canSee('Products per page:','.toolbar .pager.firstChild .firstChild .firstChild .result.firstChild');
        $I->canSee('20','.toolbar .pager.firstChild .firstChild .firstChild .result.firstChild .firstChild .alist a');
        $I->canSee('40','.toolbar .pager.firstChild .firstChild .firstChild .result.firstChild .firstChild .alist');
        $I->canSee('All','.toolbar .pager.firstChild .firstChild .firstChild .result.firstChild .firstChild .alist a');
        //Sorted
        $I->canSee('Sorted by:','.toolbar .pager.firstChild .firstChild .firstChild .a-right.sort-by');
        $I->canSee('Popularity','.toolbar .pager.firstChild .firstChild .firstChild .a-right.sort-by .firstChild li');
        $I->canSee('Name','.toolbar .pager.firstChild .firstChild .firstChild .a-right.sort-by .firstChild .list a');
        $I->canSee('Price','.toolbar .pager.firstChild .firstChild .firstChild .a-right.sort-by .firstChild .list a');
        //Page
        $I->canSee('Page','.toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination');
        $I->canSee('1','.toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination .firstChild span');
        $I->canSee('2','.toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination ol li a');
        $I->canSee('3','.toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination ol li a');
        $I->canSee('Next','.toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination ol .last .firstChild');

        //Products
        $I->canSeeElement('.lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .category-products');
        $I->canSeeElement('.category-products .products-grid.first.odd.firstChild .item .product-name.qview_productname a');
        $I->canSeeElement('.category-products .products-grid.first.odd.firstChild .item .def-uom.a-center');
        $I->canSeeElement('.category-products .products-grid.first.odd.firstChild .item .price-list');
        $I->canSeeElement('.category-products .products-grid.first.odd.firstChild .item .product-image.firstChild');

        //footer toolbar
        $I->canSeeElement('.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar');

        $I->canSee('Products per page:','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .result.firstChild');
        $I->canSee('20','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .result.firstChild .firstChild .alist.firstChild a');
        $I->canSee('40','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .result.firstChild .firstChild .alist');
        $I->canSee('All','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .result.firstChild .firstChild .alist a');
        //Sorted toolbar
        $I->canSee('Sorted by:','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.sort-by');
        $I->canSee('Popularity','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.sort-by .firstChild ');
        $I->canSee('Name','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.sort-by .firstChild .list a');
        $I->canSee('Price','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.sort-by .firstChild .list a');
        //Page toolbar
        $I->canSee('Page','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination');

        $I->canSee('1','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination ol .firstChild span');
        $I->canSee('2','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination ol li a');
        $I->canSee('3','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination ol li a');
        $I->canSee('Next','.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.cf.firstChild .right .toolbar .pager.firstChild .firstChild .firstChild .a-right.pagination ol .last .firstChild');
        //bottom copy
        $I->seeElement('.page .main.col2-left-layout .lftcrn-rpt .rtcrn-rpt.firstChild .content.firstChild');

    }

}