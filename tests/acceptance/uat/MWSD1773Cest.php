<?php
namespace uat;
use \WebGuy;

class MWSD1773Cest
{

    //NOT UPDATED FOR ACTUAL ELEMENTS

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function tryCheckout(WebGuy $I,$scenario)
    {
        $I->wantToTest('emedco credit card tokenization');
        $I->expectTo('checkout using credit card');
        $I->amOnPage(\ProductPage::$simpleProduct);
        //$I->amOnPage('/accident-prevention-tags-danger-defective-dt81369.html');
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->addProductToCart();
        $I->beginSecureCheckout();
        $I->checkoutAsMember_CheckoutMethod();
        $I->billingMethod();
        $I->checkOutAsMember_shippingMethod();
        $I->paymentMethod();
        $I->orderReview();
    }
}