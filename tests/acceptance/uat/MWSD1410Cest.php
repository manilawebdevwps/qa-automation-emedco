<?php
namespace uat;
use \WebGuy;

class MWSD1410Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function FlagSearchDYOProduct(WebGuy $I)
    {
        $I->wantTo('Check Search Result with Flag DYO Products');
        $I->amOnPage('/');
        $I->fillField('#search','asset labels');
        $I->click('#search-submit');
        $I->waitForElementVisible('#divResults', 30); // secs
        $I->see('Property ID Polyester-Tuffgard Labels');
        $I->expectTo('See flag container in product container');
        $I->seeElement('//*[@id="divResults"]/div[6]/div/ul/li[3]/span');

    }
}