<?php
namespace uat;
use \WebGuy;

class MWSD3140_MWSD3301Cest
{

    /* Emedco: Amazon Payments - Old to New Staging Server (Frontend)   */


    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function amazonPayment(WebGuy $I,$scenario)
    {
        $I->wantTo('test checkout using amazon payment ');
        $I->amOnPage(\ProductPage::$simpleProduct);
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->expectTo('see product added to cart');
        $I->wait(10);
        $I->expectTo('reload product page to escape the pop-up');
        $I->amOnPage(\ProductPage::$simpleProduct);
        $I->addProductToCart();
        $I->expectTo('view the cart');
        $I->viewCart();
        $I->expectTo('pay with amazon');
        $I->payWithAmazon();
        /*$I->canSee('When you click "Okay", we\'ll provide');
        $I->canSee('Your name:');
        $I->canSee('Your e-mail address:');
        $I->canSee('Any Amazon Payments shipping addresses selected during checkout');
        $I->canSee('Your prime status: You\'re not eligible for Prime Benefits');
        $I->canSee('We do not share your password and payment information. Your purchases are protected by the A-to-z Guarantee.');
        $I->canSee('Access your shipping and payment information from your Amazon Payments account');
        $I->canSee('Use Amazon to log into this site without another password.');
        $I->click('Okay');*/
        //added
        $I->wait(10);
        //$I->expectTo('verify the account in amazon');
        $I->switchToWindow();                         //THIS IS FAILING IN CHROME
        //$I->verifyAmazonAccount();
        $I->wait(10);
        $I->expectTo('select my shipping address');
        $I->onePageChkoutAmazon_shippingAdd();
        $I->wait(5);
        $I->expectTo('select my payment');
        $I->onePageChkoutAmazon_paymentInfo();
        $I->wait(8);
        $I->expectTo('select my shipping method');
        $I->onePageChkout_shippingMethod();
        $I->wait(8);
        $I->onePageChkout_orderReview();
        //$I->onePageChkoutAmazon_addNewAddress(); --> "+ Add new" functionality
    }
}