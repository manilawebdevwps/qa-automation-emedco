<?php
namespace uat;
use \WebGuy;

class MWSD1962Cest
{

    /*  Emedco - customers can post questions and answers on site (BazaarVoice Q&A)   */

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function bVQA(WebGuy $I)
    {
        $I->wantToTest('test that Bazaar Voice is implemented in product page');
        $I->expectTo('see that BVQASummaryContainer is visible in the product page');
        $I->changeBaseURL('http://www.emedco.com/');
        $I->amOnPage('/danger-equipment-locked-out-tag-plt23.html');
        //$I->amOnPage('/watch-your-step-message-tape-sawt8.html');

          /* Container placement  - Question & Answers */
        $I->seeInPageSource('<div id="BVQAContainer"></div>');
        /* Integration on product pages  - Question & Answers */
        $I->seeInPageSource('<script type="text/javascript"> $BV.ui("qa", "show_questions", { productId: "34369", subjectType: "product"');
        /* Container placement  - Question & Answers */
        $I->seeInPageSource('require(\'bvseosdk.php\'); $bv = new BV(array(\'deployment_zone_id\' => \'6420-en_us\',\'product_id\' => \'34369\',\'cloud_key\' => \'emedco-3ecbee1bec50c04b624e7dfccf832da8\',\'current_page_url\' => \'/danger-equipment-locked-out-tag-plt23.html\',');


        //working.....
        //$I->seeInPageSource('<script type="text/javascript"> $BV.ui("rr", "show_reviews", { productId: "34369"');
        //$I->seeInPageSource('<script type="text/javascript"> $BV.configure("global", { submissionContainerUrl:');


    }



}