<?php
namespace uat;
use \WebGuy;

class MWSD2724_MWSD3199Cest
{
    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function blog_authorNameClickable(WebGuy $I)
    {
        $I->wantTo('see author name is clickable ');
        $I->expectTo('see test result with author name');
        $I->amOnPage('/blog');
        $I->fillField('#s','ghs');
        $I->click('#searchsubmit');
        $I->wait('5');
        $I->canSeeInCurrentUrl('blog/?s=ghs');
        $I->click('Julianne Bass');
        $I->wait('5');
        $I->canSeeInCurrentUrl('blog/author/juliannebass');

    }
}