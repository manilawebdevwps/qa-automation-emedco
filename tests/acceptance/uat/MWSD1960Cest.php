<?php
namespace uat;
use \WebGuy;

class MWSD1960Cest
{

    /* Emedco - Provide product recommendations on PDPs */

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function productPage(WebGuy $I)
    {
        $I->wantToTest('that product recommendation of magento is turned on');
        $I->expectTo('see that product recommendation is found in product page');
        //$I->amOnPage('/danger-equipment-locked-out-tag-plt23.html');

        /*Group Products*/
        $I->amOnPage('/dry-erase-safety-tracker-signs-this-department-has-worked-este-departamento-ha-trabajo-81287.html');
        $I->canSee('RECOMMENDED');
        //$I->click('//#Recommended > span.firstChild > div.tab-text.firstChild');
        //#Recommended > span:nth-child(1) > div:nth-child(1)
        // dt#Recommended.tab.active span.firstChild div.tab-text.firstChild


        // $I->see('CUSTOMERS WHO PURCHASED THIS PRODUCT ALSO PURCHASED');

        /*Simple Product*/
        $I->amOnPage('/watch-your-step-message-tape-sawt8.html');
        $I->canSee('PRODUCT RECOMMENDATIONS');   //NOT YET WORKING - To BE ENABLED WHEN DEV IS FINISHED

        /*Configurable Product*/
        $I->amOnPage('catalog/product/view/id/167638/s/exit-safety-sign-sp71a/category/1980/');
        $I->canSee('PRODUCT RECOMMENDATIONS');    //NOT YET WORKING - To BE ENABLED WHEN DEV IS FINISHED
    }
}