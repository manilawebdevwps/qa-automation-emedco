<?php
namespace uat;
use \WebGuy;

class MWSD2006Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function customerFavoritesAtCategory3(WebGuy $I)
    {
        $I->wantTo('get data from DB');
        $I->expectTo('see the URLs of sitemap ');
        $results = $I->getArrayFromDB('category3','*',array());
        foreach ($results as $key=>$val) {
            $text =$val['link'];
            $I->amOnPage($text);
            $I->canSeeElement('.custfav');
            $I->canSee('Customer Favorites','h4');
        }
    }

    public function customerFavoritesAtCategory4(WebGuy $I)
    {
        $I->wantTo('get data from DB');
        $I->expectTo('see the URLs of sitemap ');
        $results = $I->getArrayFromDB('category4','*',array());
        foreach ($results as $key=>$val) {
            $text =$val['link'];
            $I->amOnPage($text);
            $I->canSeeElement('.custfav');
            $I->canSee('Customer Favorites','h4');
        }
    }

    public function customerFavoritesAtCategory5(WebGuy $I)
    {
        $I->wantTo('get data from DB');
        $I->expectTo('see the URLs of sitemap ');
        $results = $I->getArrayFromDB('category3','*',array());
        foreach ($results as $key=>$val) {
            $text =$val['link'];
            $I->amOnPage($text);
            $I->canSeeElement('.custfav');
            $I->canSee('Customer Favorites','h4');
        }
    }

}