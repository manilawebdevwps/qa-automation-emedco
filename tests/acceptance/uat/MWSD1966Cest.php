<?php
namespace uat;
use \WebGuy;

class MWSD1966Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    public $listCat = array(
    'TRAFFIC & PARKING SIGNS',
    'TAGS',
    'FACILITY & SITE MAINTENANCE',
    'SAFETY SIGNS',
    'LABELS',
    'TRAFFIC CONES, BARRICADES, TAPES',
    'LOCKOUT & ELECTRICAL',
    'WAREHOUSE SAFETY & INVENTORY',
    'SECURITY SOLUTIONS');
	
	public $listCatImg = array(
    'html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[1]/li[1]/a/div[1]/img',
    '/html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[1]/li[1]/a/div[1]/img',
    'html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[1]/li[3]/a/div[1]/img',
    'html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[2]/li[1]/a/div[1]/img',
    'html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[2]/li[2]/a/div[1]/img',
    'html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[2]/li[3]/a/div[1]/img',
    'html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[3]/li[1]/a/div[1]/img',
    'html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[3]/li[2]/a/div[1]/img',
    'html/body/div[2]/div/div/div/div[2]/div[2]/div/div[2]/ul[3]/li[3]/a/div[1]/img');

    public function NewHomePage(WebGuy $I){
        $I->wantTo('To check Homepage');
        $I->expectTo('See a larger image of category on the homepage , quick order form and subscribe form');
        $I->amOnPage('/');
        $I->seeElement('html/body/div[2]/div/div/div/div[1]/div[3]/div[1]/h1/a/img');
		$I->seeElement(".//*[@id='EnewsletterForm']");
		$I->seeElement(".//*[@id='quickorder-validate-detail']");
        //$I->see('Barricade Tapes','html/body/div[2]/div/div/div/div[1]/div[3]/div[1]/h1/a/img');
        //$I->see('Barricade tapes are a quick, easy, and economical way to block off or secure dangerous areas in order to help prevent accidents and keep people safe.','.content.cf.firstChild .top-tdesc.firstChild p');
        //$I->canSee('Filter by Category','.lftcrn-rpt .rtcrn-rpt.firstChild .catfilter-content.firstChild p');       
        foreach ($this->listCat as $sCat){
            $I->see($sCat);
        }
		
		foreach ($this->listCatImg as $iCat){
            $I->seeElement($iCat);
        }

    }

}

/*
step3

Description => Long Description


step5

Short_Description => Short Description

Description => Long Description

Meta_Keyword => Meta_Keywords

Meta_keywords value is in Keywords
*/