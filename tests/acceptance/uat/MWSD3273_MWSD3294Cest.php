<?php
namespace uat;
use \WebGuy;

class MWSD3273_MWSD3294Cest
{

    /* Emedco - Header & Navigation */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function newMainNav(WebGuy $I)
    {
        $I->wantTo('get url from DB');
        $I->expectTo('see the URLs with noindex, nofollow meta tag');
        //$I->changeBaseURL('http://www.emedco.com/');
        $I->amOnPage('/');
        $I->canSeeElement('#defaultMenu');
        //$I->dontSee('Custom Products','.level0.nav-1.level-top.first.parent.firstChild');
        $I->canSee('Signs','.level0.nav-2.level-top.parent');
        $I->canSee('Labels','.level0.nav-3.level-top.parent');
        $I->canSee('Tags','.level0.nav-4.level-top.parent');
        $I->canSee('Traffic Cones, Barricades, Tapes','.level0.nav-5.level-top.parent');
        //$I->dontSee('Parking Lot and Grounds','.level0.nav-6.level-top.parent');
        $I->canSee('Lockout & Electrical','.level0.nav-7.level-top.parent');
        //$I->dontSee('Warehouse Safety & Inventory Control','.level0.nav-8.level-top.parent');
        //$I->dontSee('Security Solutions','.level0.nav-9.level-top.parent');
        $I->canSee('Workplace Safety','.level0.nav-10.level-top.parent');
        $I->canSee('Facility & Site Maintenance','.level0.nav-11.level-top.parent');
        //All Products
        //$I->canSee('All Products','.level0.nav-8.level-top.parent');
        $I->canSee('signs','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-1.first.oddnav a span');
        $I->canSeeElement('ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav1-bg.jpg']);
        //html body#-body.cms-page-view.cms-mining div#content-container div.header-container div.header div#miningMenu.nav-container ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow
        //labels
        $I->canSee('Labels','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-2.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav2-bg.jpg']);
        //html body#-body.cms-page-view.cms-mining div#content-container div.header-container div.header div#miningMenu.nav-container ul#nav li.level0.nav-2.level-top.parent div.drop-shadow
        //Tags
        $I->canSee('Tags','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-3.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav3-bg.jpg']);
        //Traffic Cones, Barricades & Tapes
        $I->canSee('Traffic Cones, Barricades & Tapes','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-4.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav4-bg.jpg']);
        //Parking Lot & Grounds
        $I->canSee('Parking Lot & Grounds','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-5.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav5-bg.jpg']);
        //Lockout & Electrical
        $I->canSee('Lockout & Electrical','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-6.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav6-bg.jpg']);
        //Custom Products
        $I->canSee('Custom Products','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-7.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav7-bg.jpg']);
        //Warehouse & Inventory Control
        $I->canSee('Warehouse & Inventory Control','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-8.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav8-bg.jpg']);
        //Security Solutions
        $I->canSee('Security Solutions','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-9.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav9-bg.jpg']);
        //Workplace Safety
        $I->canSee('Workplace Safety','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-10.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav10-bg.jpg']);
        //Facility & Site Maintenance
        $I->canSee('Facility & Site Maintenance','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-11.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav11-bg.jpg']);
        //Pipe & Valve Marking
        $I->canSee('Pipe & Valve Marking','ul#nav li.level0.nav-1.level-top.first.parent div.drop-shadow div.consCont ul.level0 li.level1.nav-1-2.last a span');
        $I->canSeeElement('ul#nav li.level0.nav-12.level-top.parent div.drop-shadow',['url'=>'"http://static.seton.ca/skin/frontend/seton/miningca/images/mining/nav12-bg.jpg']);

        //links
        $I->click('li.level0:nth-child(2)');//Signs
        $I->wait('3');
        $I->canSeeInCurrentUrl('signs.html');
        $I->moveBack();
        $I->wait('3');
        $I->click('li.level0:nth-child(3)');//Labels
        $I->wait('3');
        $I->canSeeInCurrentUrl('labels.html');
        $I->moveBack();
        $I->wait('3');
        $I->click('li.level0:nth-child(4)');//Tags
        $I->wait('3');
        $I->canSeeInCurrentUrl('tags.html');
        $I->moveBack();
        $I->wait('3');
        $I->click('li.level0:nth-child(5)');//Traffic Cones, Barricades, Tapes
        $I->wait('3');
        $I->canSeeInCurrentUrl('tape-barricades-cones.html');
        $I->moveBack();
        $I->wait('3');
        $I->click('li.level0:nth-child(6)');//Lockout & Electrical
        $I->wait('3');
        $I->canSeeInCurrentUrl('lockout-electrical.html');
        $I->moveBack();
        $I->wait('3');
        $I->click('li.level0:nth-child(7)');//Workplace Safety
        $I->wait('3');
        $I->canSeeInCurrentUrl('safety-compliance.html');
        $I->moveBack();
        $I->wait('3');
        $I->click('li.level0:nth-child(8)');//Facility & Site Maintenance
        $I->wait('3');
        $I->canSeeInCurrentUrl('facility-site-maintenance.html');
        $I->moveBack();
        $I->wait('3');
    }
}