<?php
namespace uat;
use \WebGuy;

class MWSD2087Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function checkDYO(WebGuy $I)
    {
        $I->wantTo('check dyo button in configurable product page');
        $I->expectTo('see the DYO button');
        $I->amOnPage('/danger-do-not-enter-construction-signs-42792.html');
        $I->selectOption('//form/select[@name=super_attribute[170]]', 'Size');
        $I->selectOption('//form/select[@name=super_attribute[169]]', 'Material');
        $I->selectOption('//form/select[@name=super_attribute[353]]', 'Topcoat');
        $I->seeElement('//*[@id="DYO-PDP-BUTTON"]');
        $I->click('//*[@id="DYO-PDP-BUTTON"]');
        $I->amOnPage('/semi-custom-osha-danger-safety-signs-horizontal-dcwaa.html');
    }

}