<?php
namespace uat;
use \WebGuy;

class MWSD2487_MWSD2576Cest
{
    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function frontEnd(WebGuy $I)
    {
        $I->wantTo('test video merchandising icon in product page ');
        $I->expectTo('see container of the merchandising video icon');
        $I->amOnPage('/3m-particulate-respirator-8511-n95-re326.html#product-features');
        $I->canSee('.product-img-box');
        $I->canSee('#button-block');
        $I->canSee('#button-block > li:nth-child(1)');
        $I->click('#button-block > li:nth-child(1)');
    }
}