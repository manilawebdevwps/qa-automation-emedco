<?php
namespace uat;
use \WebGuy;

class MWSD1353Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CheckGAcodeDYOpages(WebGuy $I)
    {
        $I->wantToTest('If all Best Pages inserted the meta tag');
        $I->expectTo('See DYO pages');
        $I->canSeeInCurrentUrl('/semi-custom-danger-sign-46223.html');
        $I->expectTo('Google Analytics Event Tracking');
        $I->canSeeElementInDOM('_gaq.push(\'_trackEvent', 'DYO', 'Init', '/semi-custom-danger-sign-46223.html\');');

    }
}