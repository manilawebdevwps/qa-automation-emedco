<?php
namespace uat;
use \WebGuy;

class MWSD3344_MWSD3400Cest
{

    /* Emedco - Sitewide Free Shipping - Coupon Code Application (Staging) - Part 2 */


    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }


    // tests
    public function siteWideFreeShipping(WebGuy $I,$scenario)
    {
        $I->wantTo('test free shipping without using coupon code');
        $I->expectTo('log-in in  admin to configure the promo');
        //$I->changeBaseURL('http://www.emedco.com/');
        /*$I = new WebGuy\AdminSteps($scenario);
        //$I->loginAsAdmin();
        $I->systemConfig_AuctionmaidMatrixRates(); */
        //Test functionality
        $I->amOnPage('/ansi-lockout-tags-danger-do-not-start-work-on-machine-dt81421.html');
        $I->wait('5');
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->expectTo('add product to cart');
        $I->addProductToCart();
        $I->expectTo('view the cart');
        $I->viewCart();
        $I->expectTo('begin secure checkout');
        $I->beginSecureCheckout();
        $I->expectTo('login as returning customer');
        $I->onePageChkout_returningCustomer();
        $I->wait(6);
        $I->expectTo('transact with shipping info');
        $I->beginSecureCheckout();
        $I->onePageChkout_shippingInfo();
        $I->expectTo('continue with shipping method');
        $I->wait(3);
        $I->onePageChkout_shippingMethod();
        $I->expectTo('transact payment info');
        $I->wait(5);
        $I->onePageChkout_paymentInfo();
        $I->expectTo('transact payment method');
        $I->onePageChkout_paymentMethod();
        $I->wait(5);
        $I->onePageChkout_submitOrder();


    }

}