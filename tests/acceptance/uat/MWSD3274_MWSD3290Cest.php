<?php
namespace uat;
use \WebGuy;

class MWSD3274_MWSD3290Cest
{

    /*  Emedco - on page load, expose all config options for config PDP (Frontend) - Staging    */


    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function exposeConfigProduct(WebGuy $I)
    {
        $I->wantTo('check functionality of Label Legend Finder');
        $I->expectTo('see new search bar for Label Legend Finder');
        $I->changeBaseURL('file:///C:/QA_Work/Daily%20Ticket%20Test/MWSD3273/');
        $I->amOnPage('nav.html');
        $I->wait('5');
        //$I->canSee('3M™ Apache Fall Protection Harness');
        //size
        $I->canSee('Size','li.opt-label:nth-child(1)');
        $I->canSee('2X-Large','div.optdiv:nth-child(1) > label:nth-child(2)');
        $I->canSee('3X-Large','div.optdiv:nth-child(1) > label:nth-child(2)');
        //Options
        $I->canSee('Options','li.opt-label:nth-child(4)');
        $I->canSee('Pass-Thru Chest and Leg Connections','#conf-radio-1 > div:nth-child(1) > label:nth-child(2)');
        $I->canSee('Pass-Thru Chest Connection and Grommet Leg Connection','#conf-radio-1 > div:nth-child(2) > label:nth-child(2)');
        //Includes
        $I->canSee('Includes','li.opt-label:nth-child(7)');
        $I->canSee('Harness With Back Pad','#conf-radio-2 > div:nth-child(1) > label:nth-child(2)');
        $I->canSee('Harness With Back Pad And Toolbelt','#conf-radio-2 > div:nth-child(1) > label:nth-child(2)');
        //D-Ring Locations
        $I->canSee('D Ring Locations','li.opt-label:nth-child(10)');
        $I->canSee('Back; Hip','#conf-radio-3 > div:nth-child(1) > label:nth-child(2)');
    }
}