<?php
namespace uat;
use \WebGuy;

class MWSD2605_MWSD2623Cest
{

    /*  Emedco: Amazon Payments - Staging Only  */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function tryToTest(WebGuy $I,$scenario)
    {
        $I->wantTo('test checkout using amazon payment ');
        $I->amOnPage(\ProductPage::$simpleProduct);
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->expectTo('see product added to cart');
        $I->wait(10);
        $I->addProductToCart();
        $I->expectTo('view the cart ');
        $I->viewCart();
        $I->canSeeElement('//img[@src="https://images-na.ssl-images-amazon.com/images/G/01/EP/offAmazonPayments/us/sandbox/prod/image/lwa/gold/medium/PwA.png"]');
        $I->click('#OffAmazonPaymentsWidgets0');
        //$I->wait(10);
        //$I->acceptPopup();
        $I->switchToWindow('amazonloginpopup');
        $I->fillField(\LoginPage::$amazonField_username,\LoginPage::$amazonVal_username);
        $I->fillField(\LoginPage::$amazonField_password,\LoginPage::$amazonVal_password);
        //$I->click('$amazonBtn_signIn');
        $I->click('button.button-text.signin-button-text');
        //https://emedco.zeondemo.com/amazon_login/customer/verify/?redirect=checkout/onepage
    }






}