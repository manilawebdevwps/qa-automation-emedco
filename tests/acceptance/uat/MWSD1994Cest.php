<?php
use \WebGuy;

class MWSD1994Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

     // tests
    public function SeeWhy(WebGuy $I) {
        $I->wantTo('To check SeeWhy Abandon Javascript');
        $I->expectTo('See Javascript of seewhy');
        $I->amOnPage('/customer/account/login/');
        $I->seeInPageSource('https://d3m83gvgzupli.cloudfront.net/webEvent/cywevent.js');
        $I->seeInPageSource('getqueryemail');
    }

    
}