<?php
namespace uat;
use \WebGuy;

class MWSD1449Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function CanonicalTagLong(WebGuy $I)
    {
        $I->wantToTest('Canonical Tag on Long Form Product Page');
        $I->amOnPage('http://www.emedco.com/labels/inventory-control-warehouse-id/fluorescent/paper-labels-fil19.html');
        $I->seeInPageSource('<link rel = "canonical" href=http://www.emedco.com/paper-labels-fil19.html />');

    }

    public function CanonicalTagShort(WebGuy $I)
    {
        $I->wantToTest('Canonical Tag on Short Form Product Page');
        $I->amOnPage('http://www.emedco.com/paper-labels-fil19.html');
        $I->seeInPageSource('<link rel = "canonical" href=http://www.emedco.com/paper-labels-fil19.html />');

    }
}