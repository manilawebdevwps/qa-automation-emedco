<?php
namespace uat;
use \WebGuy;
use \Faker;
class MWSD1139Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }
    public function _liveChat($I){
        //$I = $this;
        $faker = Faker\Factory::create();
        $I->fillField('fname', $faker->firstName);
        $I->fillField('optionaldata5', $faker->email);
        $I->fillField('optionaldata4','Sample Question for Automation');
        $I->click('Submit');
    }

    // tests
    public function CheckLivePersonChat(WebGuy $I)
    {
        $I->amOnPage('/');
        $I->wantTo('To check Pop-up chat is displayed');
        $I->click('Live Chat');
        $I->switchToWindow('custclient');
        $I->seeInCurrentUrl('/customer_service_webchat_customer_form.htm');
        $I->wantTo('To check if input will lead to the result page.');
        $this->_liveChat($I);
        $I->seeInCurrentUrl('/client');
    }
}