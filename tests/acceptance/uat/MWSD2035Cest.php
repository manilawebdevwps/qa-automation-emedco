<?php
namespace uat;
use \WebGuy;

class MWSD2035Cest
{

    /*Emedco - Implement Rich Relevance*/


    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function richRelevanceItemPage(WebGuy $I)
    {
        $I->wantTo('test rich relevance exist in product page');
        $I->expectTo('see RICHRECS RECOMMENDATIONS on product page');
        $I->amOnPage('/hand-held-metal-detector-gwmdhh.html');
        $I->canSee('RichRecs Recommendations');
        $I->amOnPage('/accident-prevention-hand-signal-tag-dt694.html');
        $I->canSee('RichRecs Recommendations');
    }

    public function richRelevanceCategoryPage(WebGuy $I)
    {
        $I->wantTo('test rich relevance exist in category page');
        $I->expectTo('see RICHRECS RECOMMENDATIONS on category page');
        $I->amOnPage('/signs/other-signs.html');
        $I->canSee('RichRecs Recommendations');
        $I->amOnPage('/signs/safety-signs/protective-wear-ppe.html');
        $I->canSee('RichRecs Recommendations');
    }



}