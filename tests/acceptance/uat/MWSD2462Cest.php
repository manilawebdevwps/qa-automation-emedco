<?php
namespace uat;
use \WebGuy;

class MWSD2462Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function findQuickOrder(WebGuy $I)
    {
        $I->wantTo('check catalog quick order');
        $I->expectTo('see the catalog quick order ');
        $I->amOnPage('/');
        $I->canSee('#quickorder-validate-detail','Catalog Quick Order');
        $I->seeInField('//*[@id="sku_0"]','Item #');
        $I->seeInField('//*[@id="sku_1"]','Item #');
        $I->seeInField('//*[@id="qty_0"]','1');
        $I->seeInField('//*[@id="qty_1"]','1');
        $I->seeLink('Clear',"javascript:document.getElementById('quickorder-validate-detail').reset();");

        //submit without input
        $I->click('.btn.addtocart-btn.firstChild input[type=submit]');
        $I->switchToWindow();
        $I->canSee('Please enter an item number and quantity.');
        $I->click('.btn input[type=submit]');

        //submit WRONG input
        $I->click('.btn.addtocart-btn.firstChild input[type=submit]');
        $I->switchToWindow();
        $I->canSee('Please enter an item number and quantity.');
        $I->click('.btn input[type=submit]');

        //submit for 1 input -> Simple
        $I->fillField("#sku_0", "DKL30-BL-H-B");
        $I->fillField("#qty_0", "2");
        $I->click('.btn.addtocart-btn.firstChild input[type=submit]');
        $I->seeInCurrentUrl('/checkout/cart/');
        $I->see('.product-name > a:nth-child(1)','Western Enterprises - Pipe Thread Hex Nipples');
        $I->see('.qty','1');

        //submit for 2 inputs -> Simple
        $I->fillField("#sku_1", "hh464");
        $I->fillField("#qty_1", "2");
        $I->click('#quickorder-validate-detail input[type=submit]');
        $I->seeInCurrentUrl('/checkout/cart/');
        $I->see('.product-name > a:nth-child(1)','Western Enterprises - Pipe Thread Hex Nipples');
        $I->see('.qty','2');

        //submit for 1 input -> Configurable
        $I->fillField("#sku_0", "DKL30-BL-H-B");
        $I->fillField("#qty_0", "2");
        $I->click('.btn.addtocart-btn.firstChild input[type=submit]');
        $I->seeInCurrentUrl('color-coded-master-message-padlock-rm78.html/');

       //submit 2 inputs
        $I->fillField("#sku_0", "DKL30-BL-H-B");
        $I->fillField("#qty_0", "2");
        $I->fillField("#sku_1", "hh464");
        $I->fillField("#qty_1", "2");
        $I->click('#quickorder-validate-detail input[type=submit]');
        $I->seeInCurrentUrl('/checkout/cart/');
        $I->see('/html/body/div[1]/div/div/div/div[3]/div[1]/div/div/div/div/fieldset/table/tbody/tr[2]/td[1]/div/div[2]/span/label','Item #:');

    }
}

