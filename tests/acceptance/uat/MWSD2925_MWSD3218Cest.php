<?php
namespace uat;
use \WebGuy;

class MWSD2925_MWSD3218Cest
{

    /* Emedco - Label Legend Finder */


    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function labelLegendFinder(WebGuy $I)
    {
        $I->wantTo('check functionality of Label Legend Finder');
        $I->expectTo('see new search bar for Label Legend Finder');
        $I->changeBaseURL('http://www.emedco.dev/');
        //$I->amOnPage('labels.html');
        $I->amOnPage('signs.html');
        $I->wait(5);
        //$I->canSee('LABELS','div.content.firstChild div.title-container.firstChild h1.firstChild');
        $I->canSee('SIGNS','div.content.firstChild div.title-container.firstChild h1.firstChild');
        //$I->canSee('LABELS','div.label-finder.cf form#legendForm input#legend.firstChild');
        $I->canSeeElement('.sign-finder.cf form#legendForm input#legend.firstChild'); //should be label finder
        //$I->canSeeElement('//img[@src="http://www.emedco.dev/skin/frontend/enterprise/emedco/images/finding-right-label.png"]');
        $I->canSeeElement('//img[@src="http://www.emedco.dev/skin/frontend/enterprise/emedco/images/finding-right-sign.png"]');
        //search ...
        $I->expectTo('see result from searchable keyword');
        $I->fillField('#legend','OSHA');
        $I->click('#legendForm > button');
        $I->wait(5);
        //positive search result
        $I->expectTo('see number of results and the listed products');
        $I->canSeeInCurrentUrl('legend/?legend=osha');
        $I->canSee('Search results for:','.breadcrumbs');
        $I->canSee('OSHA:','.breadcrumbs');
        $I->canSee('Products(s)','.amount');
        $I->canSeeElement('.clear-button');
        $I->canSeeElement('.category-products');
        //clear button functions
        $I->expectTo('see again the label page');
        $I->click('.clear-button');
        $I->wait(5);
        //$I->canSeeInCurrentUrl('labels.html');
        $I->canSeeInCurrentUrl('signs.html');
        $I->canSeeElement('.sign-finder.cf form#legendForm input#legend.firstChild'); //should be label finder
        //$I->canSeeElement('//img[@src="http://www.emedco.dev/skin/frontend/enterprise/emedco/images/finding-right-label.png"]');
        $I->canSeeElement('//img[@src="http://www.emedco.dev/skin/frontend/enterprise/emedco/images/finding-right-sign.png"]');
        //negative search result
        $I->expectTo('see zero(0) number of results and the listed products');
        $I->fillField('#legend','fdgfgfh');
        $I->click('#legendForm > button');
        $I->wait(5);
        $I->canSeeInCurrentUrl('endecasearch/result/searchwithincategory/?category_id=11840&q=fdgfgfh');
        $I->canSee('0 results for','.no-result-within-category');
        $I->canSee('fdgfgfh:','.no-result-within-category');
        $I->canSeeElement('.clear-button');
        $I->canSeeElement('.category-list');
}


}