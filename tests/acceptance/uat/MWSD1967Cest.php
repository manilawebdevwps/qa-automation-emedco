<?php
namespace uat;
use \WebGuy;

class MWSD1967Cest
{

    /* Truck Ship Options - Add Features in Shipping Method / Checkout */

    public function _before()
    {
    }

    public function _after()
    {
    }


    //NOT UPDATED FOR ACTUAL ELEMENTS


    // tests
    public function PDP(WebGuy $I,$scenario)
    {
        //USE CASE#1 - truck shipment only (if truck shipment, no free shipping - no keycode)
        $I->wantToTest('Case #1 - Truck only ship items');
        $I->expectTo('see cart message, cart payment for truck only items');
        $I = new WebGuy\UserSteps($scenario);
        $I->amOnPage(\ProductPage::$URL_truckShipmentOnly);
        $I->maximizeWindow();
        //$grabbedPrice_productPage = $I->grabTextFrom(\ProductPage::$fieldProductPrice);
        $I->click('#btnaddtocart');
        $I->wait(8);
        //cart
        $I->click('#view_cart');
        $I->wait(8);
        $I->seeInCurrentUrl('checkout/cart/');
        $I->canSee(\CartPage::$cartValue_truckShipOnly_message,\CartPage::$cartField_Message);
        $I->click(\CartPage::$cartField_ShipEstimateLink);
        $I->wait(10);
        $I->canSee('Please choose the shipping method you would like to use for this order (you can change it later):',' p > strong');
        //$I->see('$'.\ProductPage::$cartValue_price_truckRate,\CartPage::$cartField_ShipEstimate_opts); //$168.95
        $I->see(\CartPage::$cartValue_method_truckRate,\CartPage::$cartField_ShipEstimate_opts); //flat rate truck
        $I->dontSee(\CartPage::$cartValue_ShipEstimate_bestWay,\CartPage::$cartField_ShipEstimate_opts);
        $I->dontSee(\CartPage::$cartValue_ShipEstimate_2ndDay,\CartPage::$cartField_ShipEstimate_opts);
        $I->dontSee(\CartPage::$cartValue_ShipEstimate_nextDay,\CartPage::$cartField_ShipEstimate_opts);
        $I->click('.closebtn');
        $I->click(\CartPage::$cartButton);
       // $I->wait(5);
        $I->seeInCurrentUrl(\CartPage::$URL_checkout);
        //log-in
        $I->loginAsMember(\LoginPage::$checkoutValue_username,\LoginPage::$checkoutValue_password);
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->billingMethod();
        //shipping method - customized
        $I->see(\CartPage::$checkoutValue_shippingMethod,\CartPage::$checkoutField_shippingMethod);        //[text] - TRUCK SHIP
        $I->see(\CartPage::$checkoutValue_shippingMethodOpts_truckShipOnly,\CartPage::$checkoutField_ShipMethodOpts_truckShipOnly); //[text] - FLAT RATE TRUCK SHIPPING
        $I->see(\ProductPage::$cartValue_price_truckRate,\CartPage::$checkoutField_ShipMethodOpts_truckShipOnly); //[price] - FLAT RATE TRUCK SHIPPING  $168.95
        $I->dontSee(\CartPage::$checkoutValue_shippingMethodOpts_bestWayGround,\CartPage::$checkoutField_ShipMethodOpts_truckShipOnly);
        $I->dontSee(\CartPage::$checkoutValue_shippingMethodOpts_2ndDay,\CartPage::$checkoutField_ShipMethodOpts_truckShipOnly);
        $I->dontSee(\CartPage::$checkoutValue_shippingMethodOpts_nextDay,\CartPage::$checkoutField_ShipMethodOpts_truckShipOnly);
        $I->fillField(\cartPage::$checkoutField_AddlShippingInfo,'QA test,please do not process...');
        $I->click(\cartPage::$checkoutButton_shipping);
        $I->wait(5);
        $I->paymentMethod();
        $I->wait(5);
        //$I->orderReview($grabbedPrice_productPage);//168.95
        $I->orderReview();//168.95
        //$I->see($grabbedPrice_productPage,\ProductPage::$cartField_productPrice); //product
        $I->see(\ProductPage::$cartValue_price_truckRate,\ProductPage::$cartField_shipPrice); //flat rate truck ($168.95)
    }

}