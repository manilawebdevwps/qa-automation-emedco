<?php
namespace uat;
use \WebGuy;

class MWSD2724_MWSD3197Cest
{

    /*  Emedco Blog: Issues stopping us from going in front of customers - Staging only
        QA Test : Milestone 1: Make search result clickable
    */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function blog_clickableResult(WebGuy $I)
    {
        $I->wantTo('test checkout using amazon payment ');
        $I->amOnPage('/blog');
        $I->fillField('#s','ghs');
        $I->click('#searchsubmit');
        $I->wait('5');
        $I->canSeeInCurrentUrl('blog/?s=ghs');
        $searchResult = $I->grabTextFrom('h2 > a');
        $I->see($searchResult);
        $I->click('h2 > a');
        $I->canSee($searchResult,'h1');
    }
}