<?php
namespace uat;
use \WebGuy;
use Faker\Factory;


class MWSD937Cest
{


    public static $formData = 'Thank you for signing-up for our email newsletter!';

    public function _before()
    {

    }

    public function _generateName(){
        $person = Factory::create();
        return $person;
    }

    public function _after()
    {
        //unset the array
    }

    // tests
    public function NewsletterSignUp(WebGuy $I) {
        $I->amOnPage('/enewsletter');
        $I->canSee('Newsletter Sign-up');
        $I->canSeeElement('#email');
        $I->fillField('#email','bradyqatest@gmail.com');
        $I->canSeeElement('#firstname');
        $user = $this->_generateName();
        $I->fillField('#firstname',$user->firstname);
        $I->canSeeElement('#lastname');
        $I->fillField('#lastname',$user->lastname);
        $I->cantSeeElement('#companyname');
        $I->canSeeElement('.btn-submit');
        $I->click('.btn-submit');
        $I->waitForElement('.messages',5);
        $I->wait(30);
        $I->useIMAP(self::$formData);

    }

}
