<?php
namespace uat;
use \WebGuy;

class MWSD2486_MWSD2584Cest
{

    /* QA-Testing: Apply PDF Merchandising Capability - Backend (Staging) */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function tryToTest(WebGuy $I)
    {
        $I->wantTo('test video merchandising icon ');
        $I->expectTo('see container of the merchandising video icon');
        $I->amOnPage('/index.php/zpanel');
        $I->fillField('#username','lranada');
        $I->fillField('#login','lilybeth5');
        $I->click('.form-button');
        $I->wait(10);
        $I->moveMouseOver('li.level0:nth-child(3) > a:nth-child(1) > span:nth-child(1)');
        $I->wait(25);
        //$I->moveMouseOver(['li.level0:nth-child(3) > a:nth-child(1) > span:nth-child(1)'],0,20);
        $I->moveMouseOver('li.level0:nth-child(3) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1) > span:nth-child(1)');
        $I->click('li.level0:nth-child(3) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1) > span:nth-child(1)');
        $I->wait(5);
        $I->fillField('#product_filter_sku','RE326');
        $I->click('html/body/div[1]/div[3]/div/div[3]/div/table/tbody/tr/td[2]/button[2]');
        $I->wait(10);
        #$I->checkOption('.massaction-checkbox');
        $I->click('td.last > a:nth-child(1)');
        $I->canSee('PDF');
        $I->click('PDF');
        $I->wait(20);
        $I->canSee('PDF Merchandise');

    }
}