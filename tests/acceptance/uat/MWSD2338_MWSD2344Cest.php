<?php
namespace uat;
use \WebGuy;

class MWSD2338_MWSD2344Cest
{

    /*  QA-Testing: Create the Sidebar part (Staging)   */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    static $url ='http://emedco.zeondemo.com';

    // tests
    public function sideBar(WebGuy $I)
    {
        $I->wantTo('test the sidebar of the new blog site ');
        $I->expectTo('see the new page of emedco blog ');
        $I->amOnPage('/blog');
        //promo1
        $I->canSeeElement('.page-sidebar');
        $I->canSeeElement('//img[@src="http://emedco.zeondemo.com/blog/wp-content/uploads/2015/05/emedco_blog_promoA1.jpg"]');
        //follow us
        $I->canSee('Follow Us','#emedco_social_profile-2');
        $I->canSeeInPageSource('Like us on facebook','#emedco_social_profile-2');
        $I->canSeeInPageSource('Follow us on twitter','#emedco_social_profile-2');
        $I->canSeeInPageSource('Subcribe on our youtube channel','#emedco_social_profile-2');
        $I->canSeeInPageSource('Add us on google+','#emedco_social_profile-2');
        $I->canSeeInPageSource('Like us on linkedin','emedco_social_profile-2');
        //Get Updates
        $I->canSee('Get Updates','#emailsubscriptionwidget-3');
        $I->canSeeInPageSource('Email Address', '#emailsubscriptionwidget-3');
        $I->canSeeElement('#emailSub-form input[type=submit]');
        //Recent Post
        $I->canSee('Recent Post','#recent-posts-widget-with-thumbnails-2');
        $I->canSee('Countdown to Compliance: GHS What You Need to Know','#recent-posts-widget-with-thumbnails-2');
        $I->canSee('Burn Awareness: Top 6 Most Frequent Burn Types','#recent-posts-widget-with-thumbnails-2');
        $I->canSee('You’ll Never See Them Coming: The Importance of Permis','#recent-posts-widget-with-thumbnails-2');
        $I->canSee('Workplace Safety News Roundup','#recent-posts-widget-with-thumbnails-2');
        $I->canSee('5 Types of Cold Stress Left Untreated','#recent-posts-widget-with-thumbnails-2');

        //All Categories
      $I->canSee('All Categories','.page-sidebar');
        //$I->canSeeElement('input', ['value' => '-1']);
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Select Category');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Ada Signs');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Ansi');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Asset Tags');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Calibration Inspection Quality Control');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Cones Barricades');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Confined Space Safety Training');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Confined Space Signs');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Custom Products');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Custom Tags');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Egress');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Evacuation');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Events and Announcements');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','Exit Fire and Emergency Signs');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','exit-fire-safety');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','eye-protection-personal-protective-wear-ppe');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','fall-protection-equipment-personal-protective-wear-ppe');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','fire-extinguishers');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','first-aid');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','floor-safety');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','forklift-safety');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','forklift-training-and-certification');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','general-safety');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','ghs');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','going-green');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','hand-protection');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','hazard-communication');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','head-protection-personal-protective-wear-ppe');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','hearing-protection-personal-protective-wear-ppe');
        $I->dontSeeOptionIsSelected('.postform input[name=cat]','inspection-tags-tags');
        //continue............
        $I->canSeeElement('//img[@src="http://localhost/blog/wp-content/uploads/2015/05/promo-vertical.jpg"]');






    }
}