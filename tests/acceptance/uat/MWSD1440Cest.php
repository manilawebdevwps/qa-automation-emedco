<?php
namespace uat;
use \WebGuy;

class MWSD1440Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function LiveChatPopUp(WebGuy $I)
    {
        $I->amOnPage('/');
        $I->wantToTest('Live chat will no longer pop up to invite');
        $I->dontSeeInPageSource('<script type="text/javascript" src="http://emedco.slcontent.com/skin/frontend/enterprise/emedco/js/InstantInvite3.js?q=1372689960&Lo0P=9fde07fb6d7521ed4a14a3c918f943f334318"></script>');
        $I->expectTo('no longer existing on the head part of the code');




    }
}