<?php
namespace uat;
use \WebGuy;

class MWSD3043_MWSD3130Cest
{


    /* Emedco - Improve Checkout Experience (Login - Back-end) - Staging only */

    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    public function _loginReturnCustomerFunctionTest(WebGuy $I)         //ok
    {
        //Email
        $I->fillField('#login-email','a');
        $I->click(\CartPage::$chkOutValue_retCustomer_button);
        $I->wait(5);
        $I->canSee('Please enter a valid email address. For example johndoe@domain.com.','#advice-validate-email-login-email');
        $I->canSee('This is a required field.','#advice-required-entry-login-password');
        //Password
        $I->fillField('#login-password','a');
        $I->click(\CartPage::$chkOutValue_retCustomer_button);
        $I->wait(5);
        $I->canSee('Please enter a valid email address. For example johndoe@domain.com.','#advice-validate-email-login-email');
        $I->canSee('Please enter 6 or more characters. Leading or trailing spaces will be ignored.','#advice-validate-password-login-password');
        //invalid - below 6 characters for email and password
        //$I->fillField('#login-email','a');
        //$I->fillField('#login-password','a');
        //$I->click(\CartPage::$chkOutValue_retCustomer_button);
        //$I->wait(5);
        //$I->canSee('Please enter a valid email address. For example johndoe@domain.com.','#advice-validate-email-login-email');
        //$I->canSee('Please enter 6 or more characters. Leading or trailing spaces will be ignored.','#advice-validate-password-login-password');
        //invalid - no value for password
        $I->fillField('#login-email','123@test.com');
        $I->click(\CartPage::$chkOutValue_retCustomer_button);
        $I->wait(3);
        $I->dontSee('Please enter a valid email address. For example johndoe@domain.com.','#advice-validate-email-login-email');
        //invalid - no value for email
        $I->fillField('#login-password','123456');
        $I->click(\CartPage::$chkOutValue_retCustomer_button);
        $I->wait(3);
        $I->dontSee('This is a required field.','#advice-required-entry-login-email');
        $I->dontSee('Please enter 6 or more characters. Leading or trailing spaces will be ignored.','#advice-validate-password-login-password');
        //invalid - invalid email and password
        $I->fillField('#login-email','123@test.com');
        $I->fillField('#login-password','123456');
        $I->wait(5);
        $I->canSee('Invalid login or password.','.error-msg');
        $I->dontSee('This is a required field.','#advice-required-entry-login-email');
        $I->dontSee('Please enter 6 or more characters. Leading or trailing spaces will be ignored.','#advice-validate-password-login-password');
    }

    /*
    public function _loginPageNewCustomerFunctionTest(WebGuy $I)         //ok
    {
        //$I->seeInCurrentUrl('/checkout/cart/');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait(5);
        $I->canSee('This is a required field.','#advice-required-entry-firstname');            //First Name';
        $I->canSee('This is a required field.','#advice-required-entry-lastname');             //last Name';
        $I->canSee('This is a required field.','#advice-required-entry-email_address');        //Email Address';
        $I->canSee('This is a required field.','#advice-required-entry-password');             //password;
        $I->canSee('This is a required field.','#advice-required-entry-confirmation');         //confirm password;
        $I->wait(5);
        //fill-up first name
        $I->fillField('#firstname','a');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait(3);
        $I->dontSee('This is a required field.','#advice-required-entry-firstname');                //First Name';
        //fill-up last name
        $I->fillField('#lastname','a');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait(3);
        $I->dontSee('This is a required field.','#advice-required-entry-lastname');                  //last Name';
        //fill-up wrong email address
        $I->fillField('#email_address','a');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait(5);
        $I->dontSee('This is a required field.','#advice-required-entry-email_address');             //Email Address;
        $I->canSee('Please enter a valid email address. For example johndoe@domain.com.','#advice-validate-email-email_address');              //Email Address';
        //fill-up correct email address
        $I->fillField('#email_address','lilybeth_ranada@bradycorp.com');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait(5);
        $I->dontSee('This is a required field.','#advice-required-entry-email_address');             //Email Address;
        $I->dontSee('Please enter a valid email address. For example johndoe@domain.com.','#advice-validate-email-email_address');
        //fill-up wrong password
        $I->fillField('#password','a');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait(3);
        $I->dontSee('This is a required field.','#advice-required-entry-password');                 //password;
        $I->canSee('Please enter 6 or more characters. Leading or trailing spaces will be ignored.','#advice-validate-password-password');
        //fill-up correct password
        $I->fillField('#password','123456');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait(3);
        $I->dontSee('This is a required field.','#advice-required-entry-password');                 //password;
        $I->dontSee('Please enter 6 or more characters. Leading or trailing spaces will be ignored.','#advice-validate-password-password');
        //fill-up wrong password
        $I->fillField('#confirmation','a');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait(3);
        $I->dontSee('This is a required field.','#confirmation');                                    //confirm password;
        $I->canSee('Please make sure your passwords match.','#advice-validate-cpassword-confirmation');
    }   */


    // tests
    public function ReturningCustomerBackend(WebGuy $I,$scenario)         //ok
    {
        $I->wantTo('test functionality of returning costumer');
        $I->expectTo('see new layout of one page checkout of returning costumer');
        $I->changeBaseURL('http://www.emedco.dev/');
        $I->amOnPage('/');
        $I->wait(20);
        $I->click('body');
        $I->amOnPage('/western-enterprises-pipe-thread-hex-nipples-hh464.html');
        $I->wait(5);
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->expectTo('add product to cart');
        $I->addProductToCart();
        $I->expectTo('do secure checkout ');
        $I->beginSecureCheckout();
        //$this->_loginReturnCustomerFunctionTest($I);
        $I->expectTo('do checkout as returning customer');
        $I->wait(5);
        $I->onePageChkout_returningCustomer();
        $I->wait(5);
        $I->onePageChkout_shippingAdd();
        $I->wait(5);
        $I->onePageChkout_shippingMethod();
        $I->wait(3);
    }
    /*
    public function NewCustomerBackend(WebGuy $I,$scenario)         //ok
    {
        $I->wantTo('test functionality of new costumer');
        $I->expectTo('see new layout of one page checkout of new costumer');
        $I->changeBaseURL('http://www.emedco.dev/');
        $I->amOnPage('/');
        $I->amOnPage('/western-enterprises-pipe-thread-hex-nipples-hh464.html');
        $I->wait(5);
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->expectTo('add product to cart');
        $I->addProductToCart();
        $I->expectTo('do secure checkout ');
        $I->beginSecureCheckout();
        $I->wait(5);
        //Functionality - New Customer
        $this->_loginPageNewCustomerFunctionTest($I);
        $I->expectTo('do checkout as new customer');
        $I->onePageChkout_NewCustomer();
    }*/

    /*public function GuestBackend(WebGuy $I,$scenario)   //ok
    {
        $I->wantTo('test functionality of guest costumer');
        $I->expectTo('see new layout of one page checkout of guest costumer');
        $I->changeBaseURL('http://www.emedco.dev/');
        $I->amOnPage('/');
        $I->amOnPage('/western-enterprises-pipe-thread-hex-nipples-hh464.html');
        $I->wait(5);
        $I = new WebGuy\CheckoutSteps($scenario);
        $I->expectTo('add product to cart');
        $I->addProductToCart();
        $I->expectTo('do secure checkout ');
        $I->beginSecureCheckout();
        $I->wait(5);
        //no functionality test
        $I->onePageChkout_guest();
    }*/

////////////////////////////////////////////////////////////////////////////////















}