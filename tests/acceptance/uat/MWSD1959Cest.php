<?php
namespace uat;
use \WebGuy;

class MWSD1959Cest
{

    /*  Emedco - add product type (simple, config, dyo) to code */

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function addProductTypeToCode(WebGuy $I)
    {
        $I->wantToTest('add product type simple, config, dyo to code');
        $I->expectTo('see a DYO-specific chat popup');
        //SIMPLE >>>> SIGNS>SAFETY SIGNS > CONSTRUCTIONS, SCAFFOLD, AND MINING SIGNS (P5)
        $I->changeBaseURL('http://www.emedco.com/');   //To disable when staging is available
        $I->amOnPage('/weatherproof-tyvek-signs-on-a-roll-caution-construction-in-progress-tvrl4.html');
        $I->reloadPage();
        $I->canSeeInPageSource('var custom_dl = {');
        $I->canSeeInPageSource('pdptype : "simple",');
        $I->canSeeInPageSource('category1: "Workplace Safety",');
        $I->canSeeInPageSource('category2: "Safety Signs",');
        $I->canSeeInPageSource('category3: "",');
        //$I->canSeeInPageSource('{ pdptype : "simple" , category1 : "Signs", category2 : "Safety Signs", category3 : "Construction, Scaffold and Mining Signs"}');
        //$I->canSeeInPageSource('{ pdptype : "simple" , category1 : "Workplace Safety", category2 : "Safety Signs", category3 : ""}');

        //GROUPED >>>>> SIGNS>OTHER SIGNS>BANNER>STOCK BANNERS > (P4)
        $I->amOnPage('/safety-slogan-banners-safety-protects-people-me0503.html');
        //$I->amOnPage('/safety-slogan-banners-safety-protects-people-me0503.html?invalidate=1'); //INVALIDATE due to cache issue. To be disabled when cache is refresed
        //$I->reloadPage();
        $I->canSeeInPageSource('var custom_dl = {');
        //$I->canSeeInPageSource('{ pdptype : "grouped" , category1 : "Signs", category2 : "Other Signs", category3 : "Banner", category4 : "Stock Banners"}');
        $I->canSeeInPageSource('pdptype : "grouped",');
        $I->canSeeInPageSource('category1: "Workplace Safety",');
        $I->canSeeInPageSource('category2: "Training and Motivation Products",');
        $I->canSeeInPageSource('category3: "Motivational Products",');

        //CONFIGURABLE >>>> SIGNS>SAFETY SIGNS>CONFINED SPACE
        $I->amOnPage('/bilingual-danger-confined-space-signs-64532.html');
        //$I->amOnPage('/bilingual-danger-confined-space-signs-64532.html?invalidate=1');        //INVALIDATE due to cache issue. To be disabled when cache is refresed
        //$I->reloadPage();
        $I->canSeeInPageSource('var custom_dl = {');
        //$I->canSeeInPageSource('{ pdptype : "configurable" , category1 : "Signs", category2 : "Safety Signs", category3 : "Confined Space Signs"}');
        $I->canSeeInPageSource('pdptype : "configurable",');
        $I->canSeeInPageSource('category1: "Mining",');
        $I->canSeeInPageSource('category2: "Processing",');
        $I->canSeeInPageSource('category3: "Confined Space",');

        //STAGING IS HAVING A PROBLEM. THIS PART OF DYO IS NOT TESTED ON STG

       //DYO >>>>> Custom Products>Custom Traffic and Parking Signs(P2)
        $I->amOnPage('/semi-custom-pedestrian-crossing-signs-mm0082.html');
        //$I->amOnPage('/semi-custom-pedestrian-crossing-signs-mm0082.html?invalidate=1');        //INVALIDATE due to cache issue. To be disabled when cache is refresed
        //$I->reloadPage();
        $I->canSeeInPageSource('var custom_dl = {');
        $I->canSeeInPageSource('pdptype : "dyo"');
        $I->canSeeInPageSource('category1: "Parking Lot and Grounds"');
        $I->canSeeInPageSource('category2: "Traffic and Parking Signs",');
        $I->canSeeInPageSource('category3: "Traffic Signs",');

    }
}