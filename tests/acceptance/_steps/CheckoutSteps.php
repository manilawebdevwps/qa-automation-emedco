<?php
namespace WebGuy;
use Codeception\Module\WebHelper;
use Symfony\Component\DomCrawler\Tests\Field\InputFormFieldTest;

class CheckoutSteps extends \WebGuy
{
    public function addProductToCart()
    {
        $I = $this;
        $I->click('#btnaddtocart');                 //THIS IS FAILING IN FF (browser incompatibility)
        $I->wait(4);
        $I->click('.addcart-btn__begin');
        //$I->wait(7);
    }

    public function viewCart()
    {
        $I = $this;
        $I->canSeeInCurrentUrl('/checkout/cart/');
        /* old page
         *$I->click('#view_cart');
        //$I->click('.topcart-img > a:nth-child(1) > img:nth-child(1)');
        $I->seeInCurrentUrl('/checkout/cart/');
        $I->see(\ProductPage::$simpleProductName,'.first');
        $I->seeElement('.first .a-center .input-text.qty');
        $I->see('$','.first .price');
        $I->see('$','.first .cart-rice');
        $I->see('If you have a keycpode to apply to this order, enter it here.','.discount-form h3');
        $I->see('total','.totals');
        $I->seeElement('#shopping-cart-totals-table');
        $I->see('Estimate Shipping & Handling','.last .estimate-links .ship-tax');
        $I->see('Estimate Tax','.last .estimate-links .ship-tax');
        $I->seeElement('.button.btn-continue-shopping');
        $I->seeElement('.button.btn-begin-secure-checkout.btn-checkout');
        */
        $I->canSee(\CartPage::$prodDetailsValue_title,\CartPage::$prodDetailsElement_title);
        $I->canSee(\CartPage::$prodDetailsValue_qty,\CartPage::$prodDetailsElement_table);
        $I->canSee(\CartPage::$prodDetailsValue_price,\CartPage::$prodDetailsElement_table);
        $I->canSee(\CartPage::$prodDetailsValue_total,\CartPage::$prodDetailsElement_table);
        $I->canSee(\ProductPage::$simpleProductName,\CartPage::$prodDetailsElement_table);
        $I->canSee(\ProductPage::$simpleProductSKU,\CartPage::$prodDetailsElement_table);
        $I->canSee('$',\CartPage::$prodDetailsElement_table);
        $I->canSee(\CartPage::$cartSummValue_title,\CartPage::$cartSummElement_table);
        $I->canSee(\CartPage::$cartSummValue_prodTotal,\CartPage::$cartSummElement_table);
        //$I->canSee(\CartPage::$cartSummValue_estTax,\CartPage::$cartSummElement_table);       //disble temp, not showing
        //$I->canSee(\CartPage::$cartSummValue_estShip,\CartPage::$cartSummElement_table);      //disble temp, not showing
        $I->canSee(\CartPage::$orderTotalValue_title,\CartPage::$cartSummElement_table);
        $I->canSee(\CartPage::$offCodeValue,\CartPage::$cartSummElement_table);
        $I->canSee(\CartPage::$offCodeValue_text,\CartPage::$cartSummElement_table);
        /*$I->canSee('Customers Who Viewed Items You Browsed Also Viewed','.cart-related > h2:nth-child(1)');
        $I->canSeeElement('#relprodcart');
        /*no RR page on test */
    }


    function beginSecureCheckout()
    {
        $I = $this;
        $I->seeInCurrentUrl('/checkout/cart/');
        $I->click(\CartPage::$cartButton_beginSecureCheckout);
        //$I->wait(5);
    }

    function loginPageCheck()
    {
        $I = $this;
        $I->wantTo('test functionality of login page - Returning Customer ');
        $I->expectTo('see login page - Returning Customer');
        $I->seeInCurrentUrl('/checkout/onepage/');
        $I->canSee(\CartPage::$chkOutValue_txt,\CartPage::$chkOutElement_txt);
        $I->canSee(\CartPage::$chkOutValue_retCustomer,\CartPage::$chkOutElement_retCustomerTable);
        $I->canSee(\CartPage::$chkOutValue_retCustomer_email,\CartPage::$chkOutElement_retCustomerTable);
        $I->canSeeElement(\CartPage::$chkOutElement_retCustomer_email);
        $I->canSee(\CartPage::$chkOutValue_retCustomer_pwd,\CartPage::$chkOutElement_retCustomerTable);
        $I->canSeeElement(\CartPage::$chkOutElement_retCustomer_pwd);
        $I->canSee(\CartPage::$chkOutValue_retCustomer_forgotPwd,\CartPage::$chkOutElement_retCustomerTable);
        $I->canSeeElement(\CartPage::$chkOutValue_retCustomer_button);
        $I->click(\CartPage::$chkOutValue_retCustomer_button);
        $I->canSee('This is a required field.','#advice-required-entry-login-email');
        $I->canSee('This is a required field.','#advice-required-entry-login-password');
    }

    function checkoutMethod_returningCustomer()
    {
        $I = $this;
        //$I->seeInCurrentUrl('/checkout/cart/');
    }


    function onePageChkout_returningCustomer()
    {
        $I = $this;
        $I->seeInCurrentUrl('checkout/onepage/');
        //valid
        $I->fillField('#login-email','lilybeth_ranada@bradycorp.com');
        $I->fillField('#login-password','lilybeth');
        $I->click(\CartPage::$chkOutValue_retCustomer_button);
    }

    function onePageChkout_guest()
    {
        $I = $this;
        $I->seeInCurrentUrl('/checkout/onepage/');
        $I->canSee(\CartPage::$chkOutValue_guest_txt,\CartPage::$chkOutElement_guestTable);                       //no time...
        $I->canSeeElement(\CartPage::$chkOutValue_guest_link);                                                    //checkout
        $I->click(\CartPage::$chkOutValue_guest_link);
    }

    function onePageChkout_NewCustomer()
    {
        $I = $this;
        $I->seeInCurrentUrl('/checkout/onepage/');
        //New Customer
        $I->canSee(\CartPage::$chkOutValue_newCustomer,\CartPage::$chkOutElement_newCustomerTable);
        $I->canSee(\CartPage::$chkOutValue_newCustomer_txt,\CartPage::$chkOutElement_newCustomerTable);
        $I->canSee(\CartPage::$chkOutValue_newCustomer_fn,\CartPage::$chkOutElement_newCustomerTable);            //First Name';
        $I->canSeeElement(\CartPage::$chkOutElement_newCustomer_fn);                                              //#firstname
        $I->canSee(\CartPage::$chkOutValue_newCustomer_ln,\CartPage::$chkOutElement_newCustomerTable);            //Last Name
        $I->canSeeElement(\CartPage::$chkOutElement_newCustomer_ln);                                              //#lastname
        $I->canSee(\CartPage::$chkOutValue_newCustomer_email,\CartPage::$chkOutElement_newCustomerTable);         //Email Address
        $I->canSeeElement(\CartPage::$chkOutElement_newCustomer_email);//#email_address
        $I->canSee(\CartPage::$chkOutValue_newCustomer_pwd,\CartPage::$chkOutElement_newCustomerTable);           //Password
        $I->canSeeElement(\CartPage::$chkOutElement_newCustomer_pwd);                                             //#password
        $I->canSee(\CartPage::$chkOutValue_newCustomer_cnfirmpwd,\CartPage::$chkOutElement_newCustomerTable);     //Confirm Password
        $I->canSeeElement(\CartPage::$chkOutElement_newCustomer_cnfirmpwd);                                       //#confirmation
        $I->canSeeElement(\CartPage::$chkOutValue_newCustomer_button);                                            //Register
        //$I->click(\CartPage::$chkOutValue_newCustomer_button);
        //fill-up correct confirmation password
        //$I->fillField('#login-email','qatest@bradycorp.com');
        //$I->fillField('#login-password','qatest');
        //$I->fillField('#confirmation','qatest');
        $I->fillField(\LoginPage::$checkOutElement_newCustomerEmail,\LoginPage::$checkOutValue_newCustomerEmail);
        $I->fillField(\LoginPage::$checkOutElement_newCustomerPwd,\LoginPage::$checkOutValue_newCustomerPwd);
        $I->wait('10');
        $I->fillField(\LoginPage::$checkOutElement_newCustomerPwdConf,\LoginPage::$checkOutValue_newCustomerPwd);
        $I->wait('10');
        $I->click(\CartPage::$chkOutValue_newCustomer_button);
        $I->wait('3');
        $I->dontSee('This is a required field.','#confirmation');                                    //confirm password;
        $I->dontsee('Please make sure your passwords match.','#advice-validate-cpassword-confirmation');
        //shipping Info
        $I->seeInCurrentUrl('checkout/onepage/index/');
        $I->canSee('Shipping Info','h2');
    }

    function checkoutMethod_guest()        //obsolete
    {
        $I = $this;
        $I->seeInCurrentUrl('/checkout/onepage/');
        $I->see('Checkout Method','H2');
        $I->checkOption(\LoginPage::$checkoutMethod_guest);
        $I->click(\LoginPage::$checkoutMethodBtn_guest);
        $I->wait(5);
    }


    function onePageCheckout()          //obsolete
    {
        $I = $this;
        $I->seeInCurrentUrl('/checkout/onepage/');
        /*$I->see('Checkout Method','H2');
        $I->fillField(\LoginPage::$checkoutValue_username,\LoginPage::$checkoutField_username);
        $I->fillField(\LoginPage::$checkoutValue_password,\LoginPage::$checkoutField_password);
        $I->click(\LoginPage::$checkoutButton_checkoutMethod);
        $I->wait(5);*/
    }

    /*function billingMethod_guest()      //Obsolete
    {
        $I = $this;
        $I->fillField(\CartPage::$billingMethodFld_firstName,\CartPage::$billingMethodVal_firstName);
        $I->fillField(\CartPage::$billingMethodFld_lastName,\CartPage::$billingMethodVal_lastName);
        $I->fillField(\CartPage::$billingMethodFld_comp,\CartPage::$billingMethodVal_comp);
        $I->fillField(\CartPage::$billingMethodFld_email,\CartPage::$billingMethodVal_email);
        $I->fillField(\CartPage::$billingMethodFld_add,\CartPage::$billingMethodVal_add);
        $I->fillField(\CartPage::$billingMethodFld_city,\CartPage::$billingMethodVal_city);
        $I->selectOption(\CartPage::$billingMethodFld_state,\CartPage::$billingMethodVal_state);
        $I->fillField(\CartPage::$billingMethodFld_zip,\CartPage::$billingMethodVal_zip);
        $I->selectOption(\CartPage::$billingMethodFld_country,\CartPage::$billingMethodVal_country);
        $I->fillField(\CartPage::$billingMethodFld_tel,\CartPage::$billingMethodVal_tel);
        $I->selectOption(\CartPage::$billingMethodFld_shipSameAdd,\CartPage::$billingMethodVal_shipSameAdd);
        $I->click(\CartPage::$billingMethodBtn);
    }*/

    /*function billingMethod_Registered()
    {
        $I = $this;
        $I->selectOption('form select[name=billing[use_for_shipping]]', 'Ship to this address (Cannot be a PO Box)');
        $I->click(\CartPage::$checkoutButton_billing);
    }*/

    function onePageChkout_shippingMethod()
    {
        $I = $this;
        //$I->click('#matrixrate');
        //$I->selectOption('form select[name=shipping_method]','Ship via my own carrier');
        //<form id="co-shipping-method-form" action="">
        //<select name="shipping_method" id="s_method_matrixrate">
        //<option value="matrixrate_matrixrate_85"  title="BEST WAY GROUND"> */
        //$I->selectOption('form select[name=shipping_method]','BEST WAY GROUND');
        $I->click('.btn.btn-ncontinue.firstChild');

    }


    //function onePageChkout_shippingAdd()
    function onePageChkout_shippingInfo()
    {
        $I = $this;
        $I->seeInCurrentUrl('checkout/onepage/');
        $I->canSee(\CartPage::$chkOutValue_shippingInfo);                                            //Shipping Info
        //$I->wait(5);
        $I->click('#shipping_address_id_148934');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    function payWithAmazon()
    {
        $I = $this;
        $I->canSeeElement('//img[@src="https://images-na.ssl-images-amazon.com/images/G/01/EP/offAmazonPayments/us/sandbox/prod/image/lwa/gold/medium/PwA.png"]');
        $I->wait(8);
        $I->click('#OffAmazonPaymentsWidgets0');
        $I->wait(5);
        //$I->acceptPopup();
        //$I->switchToWindow('amazonloginpopup');
        $I->fillField(\LoginPage::$amazonField_username,\LoginPage::$amazonVal_username);
        $I->fillField(\LoginPage::$amazonField_password,\LoginPage::$amazonVal_password);
        //$I->click('$amazonBtn_signIn');
        $I->click('.button-text.signin-button-text');
    }

    function verifyAmazonAccount()
    {
        $I = $this;
        //$I->seeInCurrentUrl('/amazon_login/customer/verify/?redirect=checkout/onepage');
        $I->switchToWindow();
        $I->fillField('#pass',\LoginPage::$amazonVal_password);
        $I->click('#send2');
    }

    function onePageChkoutAmazon_shippingAdd()
    {
        $I = $this;
        //$I->wait(5);
        $I->seeInCurrentUrl('checkout/onepage/');
        $I->switchToIFrame('OffAmazonPaymentsWidgets0IFrame');
        //$I->canSee(\CartPage::$chkOutValueAmazon_shippingAdd,'#amazon-widget-address-wrapper > h2:nth-child(1)');
        $I->canSee('Address Book','h1');


        $n=8;
        for ($i=0; $i < $n ; $i++  ){
            $I->expectTo('see #'.$i);
            $buyer = $I->grabTextFrom('li.en_US:nth-child(1) > a:nth-child(1) > span:nth-child(1) > strong:nth-child(1)');
            $I->expectTo('see buyer1 - '.$buyer);

            if ($buyer == 'Lilybeth R.')
            {
                $I->expectTo('see buyer1 = Lilybeth and select -> '.$i.'');
                //$I->expectTo('see #'.$i);
                $I->click('li.en_US:nth-child(1) > a:nth-child(1) > span:nth-child(1) > strong:nth-child(1)');
                break;
            }
            else {
                $I->expectTo('see buyer2 ...');
                $buyerNext = $I->grabTextFrom('li.en_US:nth-child(2) > a:nth-child(1) > span:nth-child(1) > strong:nth-child(1)');
                $I->expectTo('see buyer 2 - '.$buyerNext);
                if ( $buyerNext == 'Lilybeth R.'){
                    $I->expectTo('see #'.$i);
                    $I->expectTo('see buyer 2= Lilybeth and select ->'.$buyerNext);
                    $I->click('li.en_US:nth-child(2)');
                }
                else {
                    //$I->expectTo('see #'.$i);
                    $I->expectTo('see BOTH are not Lilybeth, next ->');
                    $I->click('.next > a:nth-child(1)');
                }
            }//if ($buyer == 'Lilybeth R.')
        }//for ($i=0; $i < $n ; $i++  ){
    }

    function onePageChkoutAmazon_addNewAddress()
    {
        $I = $this;
        $I->click('.info > a:nth-child(2)');
        $I->switchToIFrame('OffAmazonPaymentsPopupWindowA14DJ33B27YOC');
        $I->fillField('#address_name','Lilybeth Ranada');
        $I->fillField('#address_addressLine1','3F WCC ');
        $I->fillField('#address_addressLine2','Shaw Boulevard, Edsa');
        $I->fillField('#address_city','Mandaluyong City');
        $I->fillField('#address_state','Metro Manila');
        $I->fillField('#address_zip','1552');
        $I->fillField('#address_phone','1234567890');
        $I->click('#ship-to-this-addres-button');
        //Verify Shipping
        $I->canSee('#validateShippingAddressHeader > h1','Verify your shipping address');
        $I->canSee('#validateShippingAddressHeaderMessage','There\'s a problem with the address provided - we\'ve marked our suggestions in red below. Please choose which version of the address you want to use, or click Edit next to the address you want to change.');
        //Original address with edit
        #$I->click('#originalOption');
        #$I->click('#validateShippingAddressTable > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(4) > input:nth-child(1)');
        //Suggested Address with Edit
        #$I->click('#validateShippingAddressTable > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(4) > input:nth-child(1)');
        //Suggested Address without Edit
        $I->click('#shipToAddressButton > img:nth-child(1)');
    }


    /*function onePageChkout_shippingMethod()  //Second Day Air
    {
        $I = $this;
        $I->seeInCurrentUrl('checkout/onepage/index/');
        $I->canSee(\CartPage::$chkOutValue_shippingInfo);                                            //Shipping Info
        $I->canSee(\CartPage::$chkOutValue_shippingMethod);                                          //Shipping Method
       I->click('#matrixrate');
        $I->expectTo('select the second day air');
        $I->wait(5);
        //$I->selectOption('#co-shipping-form select[name=matrixrate_matrixrate_102]','BEST WAY 2ND DAY AIR - DOMESTIC');
        $I->checkOption('matrixrate_matrixrate_102');
        $I->wait $(5);
        $I->click('Continue'); // or button.btn:nth-child(1)
    }*/

    function onePageChkout_paymentInfo()
    {
        $I = $this;
        //$I->seeInCurrentUrl('checkout/onepage/index/');
        $I->canSee(\CartPage::$chkOutValue_paymentInfo);                                      //Payment Info
        $I->canSee(\CartPage::$chkOutValue_billingAdd);                                      //Billing Add
        //$I->wait(5);
        $I->checkOption('#billing_address_id_148934');
    }


    function onePageChkoutAmazon_paymentInfo()
    {
        $I = $this;
        $I->switchToWindow();
        $I->switchToIFrame('OffAmazonPaymentsWidgets1IFrame');        //FAILING IN CHROME
        $n=2;
        for ($i=0; $i < $n ; $i++  ){
            $I->expectTo('see #'.$i);
            $cc = $I->grabTextFrom('html/body/div[1]/div[3]/div[2]/ul/li[1]/a/span/span[1]/strong');
            $I->expectTo('see cc1 -> '.$cc);

            if ($cc == 'Discover')
            {
                $I->expectTo('see cc1 = Discover and select -> '.$i.'');
                //$I->expectTo('see #'.$i);
                $I->click('html/body/div[1]/div[3]/div[2]/ul/li[1]/a/span/span[1]/strong');
                break;
            }
            else {
                $I->expectTo('see cc 2 ...');
                $cc2 = $I->grabTextFrom('html/body/div[1]/div[3]/div[2]/ul/li[2]/a/span/span[1]/strong');
                $I->expectTo('see cc 2 -> '.$cc2);
                if ( $cc2 == 'Discover'){
                    //$I->expectTo('see #'.$i);
                    $I->expectTo('see cc 2 = Discover and select ->'.$cc2);
                    $I->click('html/body/div[1]/div[3]/div[2]/ul/li[2]/a/span/span[1]/strong');
                    $I->wait(5);
                    break;
                }
                else {
                    //$I->expectTo('see #'.$i);
                    $I->expectTo('see BOTH are not Discover, next ->');
                    $I->click('.next > a:nth-child(1)');

                }
            }//if ($buyer == 'Lilybeth R.')
        }//for ($i=0; $i < $n ; $i++  ){
        $I->wait(5);
        $I->switchToWindow();
        $I->click('.btn-ncontinue');
    }





    function onePageChkout_paymentMethod()
    {
        $I = $this;

        $randCC = rand(0, 3);
        $randMonth = rand(0, 11);
        $ccType = $I->captureCCType();
        $I->click('#pay_method_paymetric_xiintercept_iframe');
        $I->switchToIFrame('xiintercept-iframe');
        $I->fillField('#c-chn','FreeShippingTest');
        $I->expectTo('see the $randCC = '.$randCC);
        $I->seeMyVar($ccType);
        $I->selectOption('form select[name=c-ct]',$ccType[$randCC][0]);
        $I->wait(5);
        //$I->seeMyVar($ccType);
        $I->fillField('#c-cn', $ccType[$randCC][1]);                    //dito
        $I->expectTo('see the $randMonth = '.$randMonth);
        $ccMonth = $I->captureCCMonth();
        //$I->seeMyVar($ccMonth);
        $I->selectOption('form select[name=c-exmth]', $ccMonth[$randMonth]);
        //$I->selectOption('form select[name=c-exmth]', 'January');
        $ccYear = $I->captureCCYear();
        $I->selectOption('form select[name=c-exyr]', $ccYear[rand(0, 9)]);
        //$I->selectOption('form select[name=c-exyr]', '2017');
        $I->fillField('#c-cvv',  $ccType[$randCC][2]);
        //$I->fillField('#c-cvv', '123');
        $I->wait(6);

        $I->executeInSelenium(function (\Webdriver $webdriver) {
            $handles=$webdriver->getWindowHandles();
            $last_window = end($handles);
            $webdriver->switchTo()->window($last_window);
        });

        $I->wait(6);
        //$I->click('#payment-buttons-container > button.btn.btn-ncontinue');
        $I->click('html/body/div[3]/div/div/div/div[2]/div[2]/div/div[2]/ol/div/li[2]/div[2]/div/div/button');
    }


    function onePageChkout_submitOrder()
    {
        $I = $this;
        $I->canSee('Please confirm your details','.step-content');
        $I->canSee('Shipping Address','.step-content');
        $I->canSee('Billing Address','.step-content');
        $I->canSee('Shipping Method','.step-content');
        $I->canSee('Payment Method','.step-content');
        $I->canSee('Product Details','.step-content');
        $I->canSee('QTY','.step-content');
        $I->canSee('Unit Price','.step-content');
        $I->canSee('Total','.step-content');
        $I->canSee(\ProductPage::$simpleProductSKU,'.step-content');
        $I->canSeeElement('.btn.btn-ngoback.left');
        $I->canSee('Summary','#col-right-opcheckout');
        $I->canSee('Product Total','#totalsprogress');
        $I->canSee('Order Total','#totalsprogress');
        $I->canSee('Shipping Address','#col-right-opcheckout');
        $I->canSee('Shipping Method','#col-right-opcheckout');
        $I->canSee('Billing Address','#col-right-opcheckout');
        $I->canSee('Payment Method','#col-right-opcheckout');
        $I->click('.btn-nsubmit-order');
    }




}

