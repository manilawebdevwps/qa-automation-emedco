<?php
namespace WebGuy;

class HeaderPageSteps extends \WebGuy
{

    public function check_pageHeader()
    {
        $I = $this;
        $I->canSeeElement('html body.catalog-product-view.catalog-product-view.product-ansi-signs-caution-follow-lock-out-procedures-80801 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild');
        $I->canSeeElement('.header-top');
        $I->canSeeElement('.header-logo');
        $I->canSeeElement('.nav-container');
    }

    public function check_topHeader()
    {
        $I = $this;
        $I->canSeeElement('html body.catalog-product-view.catalog-product-view.product-ansi-signs-caution-follow-lock-out-procedures-80801 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-top.clearf.firstChild div.left.firstChild');
        $I->canSeeElement('html body.catalog-product-view.catalog-product-view.product-ansi-signs-caution-follow-lock-out-procedures-80801 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-top.clearf.firstChild div.left.firstChild');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-ansi-signs-caution-follow-lock-out-procedures-80801 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-top.clearf.firstChild','877-267-3501');
        //live chat
        $I->canSee('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-top.clearf.firstChild div.left.firstChild a span.header-sprite.header-sprite__livechat.firstChild','Live Chat');
        $I->click('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-top.clearf.firstChild div.left.firstChild a span.header-sprite.header-sprite__livechat.firstChild');
        //$I->click('//a/span','Live Chat');
        $I->wait('10');
        $I->canSee('#SurveyDiv','To help us serve you better, please provide some information before we begin your chat.');
        $I->click('lp_closeChat');
        //catalog quick order
        $I->canSeeElement('html body.catalog-product-view.catalog-product-view.product-ansi-signs-caution-follow-lock-out-procedures-80801 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-top.clearf.firstChild div.right','Catalog Quick Order');
        $I->click('//a[2]/span','Catalog Quick Order');
        $I->canSeeInCurrentUrl('/catalog-quick-order');
        //Login
        $I->canSee('html body.andromeda-quickorder-index-index div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-top.clearf.firstChild div.right a.firstChild span.header-sprite.header-sprite__signin.firstChild','Login');
        $I->click('//div[2]/a/span','Login');
        $I->canSeeInCurrentUrl('/customer/account/login');
        //create account
        $I->click(['link' => 'Create Account']);
        $I->canSeeInCurrentUrl('/customer/account/login');
    }


    public function check_logoHeader()
    {
        $I = $this;
        $I->canSeeElement('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf');
        $I->canSeeElement('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf ul.header-sprite.header-sprite__checklist.firstChild');
        $I->canSee('ul.header-sprite > li:nth-child(1)','Lifetime Guarantees');
        $I->canSee('ul.header-sprite > li:nth-child(2)','Same Day Shipping');
        $I->canSee('ul.header-sprite > li:nth-child(3)','Easy Customization');
        $I->canSee('ul.header-sprite > li:nth-child(4)','Safety Identification Superstore');
        //$I->canSeeElement('img[alt="Emedco Home"]');
        //$I->seeElement('//img[@src="/pixel.jpg"]');
        //$I->seeElement('//img[@src="http://emedco.slcontent.com/skin/frontend/enterprise/emedco/images/logo-emedco.gif');
        $I->canSeeElement('.logo');
        $I->canSeeElement('//img[@src="/logo-emedco.gif"]');
        $I->canSeeElement('#search-by-cat');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul#search-dropdown.search-dropdown li.sub-search-dropdown.firstChild','All Products');
        $I->click('.sub-search-dropdown');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li.firstChild','All Products');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Custom Products');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Signs');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Labels');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Tags');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Tape, Barricades & Cones');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Parking Lot and Grounds');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Lockout & Electrical');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Warehouse Safety & Inventory');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Security Aids');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Safety Compliance');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__search form#search_mini_form div#search-by-cat.firstChild ul.search-dropdown-child li','Facility & Site Maintenance');
        //search bar
        $I->canSeeElement('#form-search');
        $I->canSeeElement('#search');
        $I->canSeeElement('#search-submit');
        //cart
        $I->canSeeElement('.header-logo__topcart');
        $I->canSeeElement('.top-cart');
        $I->canSeeElement('ul.right');
        $I->canSeeElement('.topcart-img');
        $I->canSeeElement('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__topcart.right div.top-cart.right.firstChild div.block-title.no-items ul.right.firstChild li.firstChild dl.firstChild dd#cartHeader.text-centered.firstChild span.firstChild','items');
        $I->canSeeElement('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div.header-logo.clearf div.header-logo__topcart.right div.top-cart.right.firstChild div.block-title.no-items ul.right.firstChild li.firstChild dl.firstChild dd#cartHeader.text-centered.firstChild span.price','');
        $I->canSeeElement('.#mini-cart');
    }


    public function check_Nav()
    {
        $I = $this;
        $I->canSeeElement('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container');
        $I->canSeeElement('#nav');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-1.level-top.first.parent.firstChild a.level-top.firstChild span.firstChild','Custom Products');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-2.level-top.parent a.level-top.firstChild span.firstChild','Signs');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-3.level-top.parent a.level-top.firstChild span.firstChild','Labels');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-4.level-top.parent a.level-top.firstChild span.firstChild','Tags');
        $I->canSee('html body.catalog-product-view.catalog-product-view.product-magnetic-number-set-mgno2 div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-5.level-top.parent a.level-top.firstChild span.firstChild','Traffic Cones, Barricades, Tapes');
        $I->canSee('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-6.level-top.parent a.level-top.firstChild span.firstChild','Parking Lot and Grounds');
        $I->canSee('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-7.level-top.parent a.level-top.firstChild span.firstChild','Lockout & Electrical');
        $I->canSee('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-8.level-top.parent a.level-top.firstChild span.firstChild','Warehouse Safety & Inventory Control');
        $I->canSee('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-9.level-top.parent a.level-top.firstChild span.firstChild','Security Solutions');
        $I->canSee('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-10.level-top.parent a.level-top.firstChild span.firstChild','Workplace Safety');
        $I->canSee('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.page-header.firstChild div#defaultMenu.nav-container ul#nav.firstChild li.level0.nav-11.level-top.last.parent a.level-top.firstChild span.firstChild','Facility & Site Maintenance');
    }

}
