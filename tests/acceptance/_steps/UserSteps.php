<?php
namespace WebGuy;

class UserSteps extends \WebGuy
{
    function check404page($url= '/'){
        $I = $this;
        $I->wantTo('Want to check if the website encounter 404 page');
        $I->maximizeWindow();
        $I->amOnPage($url);
        $I->dontSee('We are sorry, but the page you are looking for cannot be found.');
    }
}