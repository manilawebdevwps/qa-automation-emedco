<?php
namespace WebGuy;

use Faker\Provider\Image;

class AdminSteps extends \WebGuy
{

    public function loginAsAdmin()
    {
        $I = $this;
        $I->amOnPage(\adminPage::$urlStg_admin);
        $I->fillField(\adminPage::$stgAdminField_username,\adminPage::$stgAdminValue_username);
        $I->fillField(\adminPage::$stgAdminField_password,\adminPage::$stgAdminValue_password);
        $I->click('Login');
    }

    public function systemConfig_AuctionmaidMatrixRates()
    {
        $I = $this;
        $I->moveMouseOver(\adminPage::$stgAdminField_system);
        $I->wait(3);
        $I->click(\adminPage::$stgAdminField_system);
        $I->moveMouseOver(\adminPage::$stgAdminField_configuration);
        $I->click(\adminPage::$stgAdminField_configuration);
        $I->wait(7);
        $I->click(\adminPage::$stgAdminField_shippingMethod);
        $I->wait(3);
        $I->click(\adminPage::$stgAdminField_auctionmaidMatrixRates);
        $I->wait(2);
        $I->selectOption('//form/select[@id='.\adminPage::$stgAdminField_freeShipping_withMinOrderAmtActive.']', \adminPage::$stgAdminValue_freeShipping_withMinOrderAmtActive);
        //100
        $I->fillField(\adminPage::$stgAdminField_freeShipping_amount,'100');
        $I->click(\adminPage::$stgAdminField_freeShipping_button);
    }
}