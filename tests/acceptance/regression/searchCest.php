<?php
namespace regression;
use \WebGuy;

class searchCest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function inputProduct(WebGuy $I)
    {
        $I->wantTo('To check search bar of emedco');
        $I->expectTo('see result from search bar using SKU as input');
        $I->amOnPage('/');
        $I->seeElement('html body.cms-index-index.cms-home div.page1 div.page2.firstChild div.wrapper.firstChild div.page div.header-container.firstChild div.new-header div#quick-access.quick-access form#search_mini_form div#search-by-cat.firstChild div#form-search.form-search input#search.input-text');

        //search for SKU
        $I->fillField(\HomePage::$searchElement,\ProductPage::$validProduct_search);
        $I->click(\HomePage::$searchButton);
        $I->wait('10');
        $I->seeInCurrentUrl(\ProductPage::$URL_validProduct_search);

        //search product name
        $I->expectTo('see result from search bar using product name as input');
        $I->fillField(\HomePage::$searchElement,\ProductPage::$validProductName_search);
        $I->click(\HomePage::$searchButton);
        $I->wait('10');
        $I->see('Narrow Your Results','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_left-pane.firstChild div.pb_dimension-filters-container.firstChild div.pb_dimfil-header.firstChild');
        $I->see('Results for','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_panel_cont.firstChild div.pb_results_search.firstChild');
        $I->see('Sort By:','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_sort-search');
        $I->see('Showing','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_panel_cont div.pb_showing_count');
        $I->see('results','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_panel_cont div.pb_showing_count span');
        $I->see('Show','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_sort-search div.pb_showby label span');
        $I->seeElement('div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div#div_pagination.pb_pagination');
        $I->seeElement('div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_sort-search div.pb_pagination');
        $I->see('Sort By:','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_sort-search div.pb_sort label span');
        $I->see('Showing','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_sort-search div.pb_showing_count');
        $I->see('results','div.lftcrn-rpt div.rtcrn-rpt.firstChild div.content.firstChild div.pb_cont.firstChild div.pb_right-pane div.pb_right-pane-container.firstChild div.pb_center-container.firstChild div#divResults.firstChild div.pb_sort-search div.pb_showing_count span');
        $I->see(\ProductPage::$validProductNameComp_search);

        //invalid SKU
        $I->expectTo('see result from search bar using invalid SKU as input');
        $I->fillField(\HomePage::$searchElement,\ProductPage::$invalidProduct_search);
        $I->click(\HomePage::$searchButton);
        $I->wait('10');
        $I->seeInCurrentUrl('/search');
        $I->seeInCurrentUrl(\ProductPage::$invalidProduct_search.'?');
        $I->see('Sorry, we couldn\'t find a match for','div.search-content-right div.search-result.firstChild p.firstChild');
        $I->see(\ProductPage::$invalidProduct_search,' div.search-content-right div.search-result.firstChild p.search-result-message');
        $I->seeElement('div.search-content-right div.sus-search form#search_mini_form.firstChild input#search.firstChild.input-text-edit');
        $I->seeElement('div.search-content-left.firstChild');
        $I->seeElement('//img[@src="http://emedco.zeondemo.com/skin/frontend/enterprise/emedco/images/contact-ph-icon.jpg"]');
        $I->seeElement('//img[@src="http://emedco.zeondemo.com/skin/frontend/enterprise/emedco/images/contact-email-icon.jpg"]');
        $I->seeElement('//img[@src="http://emedco.zeondemo.com/skin/frontend/enterprise/emedco/images/contact-chat-icon.jpg"]');
    }

    // product page test is separated

}