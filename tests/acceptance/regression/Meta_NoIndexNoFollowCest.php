<?php
namespace regression;
use \WebGuy;

class Meta_NoIndexNoFollowCest
{
    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // tests
    public function bestPages(WebGuy $I)
    {
        $I->wantTo('get data from DB');
        $I->expectTo('see the best pages URLs with NOINDEX,NOFOLLOW');
        //$I->changeBaseURL('http://www.emedco.com/');
        $results = $I->getArrayFromDB('best_pages_remove_batch7','*',array());
        $n = 0;

        $dateTimeTested = date("m-d-Y H:i");
        $arr = explode(" ",$dateTimeTested);
        $dateTested = $arr[0];
        //$I->expectTo('see date tested '.$dateTested);
        /* LogResult($testType,$num,$result,$dateTimeTested,$dateTested) */
        $I->LogResult('BestPages','','',$dateTimeTested,$dateTested);
        //LogResult($testType,$result,$dateTimeTested,$dateTested)

        foreach ($results as $key=>$val) {
            //$text = $I->truncate($val['url']);
            $text = $val['url'];
            $n++;
            //FOR TESTING ONLY
            $I->expectTo('see url#'.$n.' ');
            $I->amOnPage($text);
            $I->wait(7);
            //$I->canSeeInPageSource('meta name="robots" content="NOINDEX, NOFOLLOW"');
            //$I->LogResult('BestPages',$n,$text,$dateTimeTested,$dateTested);
            if ($I->canStillSeeInPageSource('meta name="robots" content="NOINDEX, NOFOLLOW"')){

                //LogResult($testType,$num,$result,$dateTimeTested,$dateTested){
                $I->LogResult('BestPages',$n,$text,$dateTimeTested,$dateTested);
                //$fp = fopen('tests\_data\LogFile_'.$testType.'_'.$dateTested.'.txt', 'a+');

            }else {

                //LogResult($testType,$num,$result,$dateTimeTested,$dateTested){
                $I->LogResult('BestPagesFail',$n,$text,$dateTimeTested,$dateTested);
                //$fp = fopen('tests\_data\LogFile_'.$testType.'_'.$dateTested.'.txt', 'a+');

            }
        }
            //LogResult($testType,$result,$dateTimeTested,$dateTested){
            //$I->LogResult('bestPages','',$dateTimeTested,$dateTested);
            //$I->LogResult('bestPages','$n',$text,'','');
            /* LogResult($testType,$num,$result,$dateTimeTested,$dateTested) */
            //$I->LogResult('',$n,$text,'','');

            /* fwrite($fp, $num." : ".$result.''."\r\n"); /* 1 : best/ways_to_break_padlocks  */



            /************* WEBHELPER **************/

            /*
             public function LogResult($testType,$num,$result,$dateTimeTested,$dateTested){
              //$I->LogResult('',$n,$text,'','');
            try {

                $fp = fopen('tests\_data\LogFile_'.$testType.'_'.$dateTested.'.txt', 'a+');

                if ($dateTimeTested == ' '){
                    #fwrite($fp, "\r\n");
                    #fwrite($fp, $dateTimeTested.''."\r\n");
                    //fwrite($fp, $testType." : ".$result.''."\r\n");                   // 1 : best/ways_to_break_padlocks
                    fwrite($fp, $num." : ".$result.''."\r\n");                          // 1 : best/ways_to_break_padlocks
                    //$I->LogResult('Category Level 1 > '.self::$link_Homepage.self::$link_Category1, $pageResult, '', $dateTested);
                    fclose($fp);
                }else {
                    //fwrite($fp, "\r\n");
                    //fwrite($fp, $dateTimeTested.''."\r\n");  --->>>NA for bestPages
                    fwrite($fp, $num." : ".$result.''."\r\n");
                    //fwrite($fp, $testType." : here1".$result.''."\r\n");
                    //Category Level 1 > http://www.emedco.com/signs.html : 4.33s  //pagetest
                    //$result - $testType --> LogResult($url,$n)
                    //#1 - best/yellow_tag_fasteners
                    fclose($fp);
                }       //}
                //die();



            }catch (\PHPUnit_Framework_AssertionFailedError $e){

                $fp = fopen('tests\_data\TestLog_'.$testType.'_'.$dateTested.'.txt', 'a+');

                if ($dateTimeTested == ' '){
                    #fwrite($fp, "\r\n");
                    #fwrite($fp, $dateTimeTested.''."\r\n");
                    fwrite($fp, $testType." : ".$result.''."\r\n");
                    //Category Level 1 > http://www.emedco.com/signs.html : 4.33s
                    fclose($fp);
                }else {
                    //fwrite($fp, "\r\n");
                    fwrite($fp, $dateTimeTested.''."\r\n");
                    fwrite($fp, $testType." : ".$result.''."\r\n");
                    //Category Level 1 > http://www.emedco.com/signs.html : 4.33s
                    fclose($fp);
                }       //}

            }//try & catch
            return true;

        }
        */

        /************* WEBHELPER **************/

    }
}