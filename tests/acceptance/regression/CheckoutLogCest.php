<?php
namespace regression;
use \WebGuy;

class CheckoutLogCest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function SimpleProductTest(WebGuy $I,$scenario)
    {
        $testLimit = 0;
        while($testLimit<1000) {
            $dateTimeTested = date("m-d-Y H:i");
            $arr = explode(" ",$dateTimeTested);
            $dateTested = $arr[0];
            //$I->changeBaseURL('http://www.emedco.com/');
            $I->amOnPage(\ProductPage::$simpleProduct);
            $I = new WebGuy\CheckoutSteps($scenario);
            $I->expectTo('see product added to cart');
            $I->wait(5);
            $I->addProductToCart();
            $I->wait(7);
            $I->expectTo('do secure checkout ');
            $I->beginSecureCheckout();
            $I->wait(5);
            $I->expectTo('do checkout method as registered user');
            if ($I->seeElement('#btn btn-nsubmit-order')){
                $I->LogResult('Checkout Pass',$n++,$text,$dateTimeTested,$dateTested);
            } else{
                $I->LogResult('Checkout Fail',$n++,$text,$dateTimeTested,$dateTested);
            }
            $I->onePageChkout_returningCustomer();
            $I->wait(5);
            $I->expectTo('do secure checkout again after logging-in ');
            $I->beginSecureCheckout();
            $I->wait(5);
            if ($I->seeElement('#btn btn-nsubmit-order')){
                $I->LogResult('Checkout Pass',$n++,$text,$dateTimeTested,$dateTested);
            } else{
                $I->LogResult('Checkout Fail',$n++,$text,$dateTimeTested,$dateTested);
            }
            $I->expectTo('see shipping info');
            $I->onePageChkout_shippingInfo();
            $I->expectTo('do shipping method');
            $I->onePageChkout_shippingMethod();
            $I->wait(3);
            $I->expectTo('see payment info');
            $I->onePageChkout_paymentInfo();
            $I->wait(3);
            $I->expectTo('do payment method');
            $I->onePageChkout_paymentMethod();
            $I->expectTo('do order review');
            $I->onePageChkout_submitOrder();

            $testLimit++
        }
    }

}