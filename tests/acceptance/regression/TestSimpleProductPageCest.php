<?php
namespace regression;
use \WebGuy;

class TestSimpleProductPageCest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function simpleProductPage(WebGuy $I)
    {
        $I->wantToTest('test simple product page');
        $I->expectTo('see product page');
        $I->amOnPage(ProductPage::$simpleProduct);
        //UI
        $I->seeElement('.content.firstChild');
        $I->seeElement('.content.firstChild .prod-name.cf.firstChild');
        $I->seeElement('.content.firstChild .cf');
        $I->seeElement('.content.firstChild .prod-specs.cf');
        $I->see(ProductPage::$simpleProductName,'h1');
        $I->seeElement('.content.firstChild .prod-specs.cf');
        $I->seeElement('//img[@src="'.ProductPage::$simpleProductImage.'"]');
        $I->see('View Larger','.view-larger');
        $I->seeElement('.social-share');
        $I->see('+ Add to Wishlist','.social-share .add-wishlist.left.firstChild .firstChild');
        $I->seeElement('.social-share .mmsocial-icon.fb-link.left');
        $I->seeElement('.social-share .mmsocial-icon.tw-link.left');
        $I->seeElement('.social-share .mmsocial-icon.email-link.left');
        $I->seeElement('.social-share .mmsocial-icon.print-link.left');
        $I->seeElement('#product-sku');
        $I->see(ProductPage::$simpleProductSKU,'#product-sku');
        $I->seeElement('.mmprice-box');
        $I->see('$','.mmprice-box');
        $I->seeElement('.qtyform');
        $I->see('Qty','.qtyform');
        $I->seeElement('#btnaddtocart');
        $I->seeElement('.mmprod-desc');
        $I->seeElement('.compliances');
        $I->see('Specifications','.prod-specs');
    }
}