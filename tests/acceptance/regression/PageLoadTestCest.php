<?php
namespace regression;
use \WebGuy;

class PageLoadTestCest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    //static $link_Homepage    = 'http://emedco.zeondemo.com/';
    static $link_Homepage    = 'http://www.emedco.com/';
    static $link_Category1   = 'signs.html';
    static $link_Category2   = 'tape-barricades-cones/tapes.html';
    static $link_Category3   = 'labels/inventory-control-warehouse-id/color-coded.html';
    static $link_ProductPage = 'wash-hands-signs-32399br.html';
    static $link_CartPage    = 'checkout/cart/';


    // tests
    public function homePageLoadTest(WebGuy $I)
    {
        //Homepage
        $I->wantToTest('Homepage load time');
        $I->expectTo('see pingdom site testing the homepage of emedco');
        $I->changeBaseURL('http://tools.pingdom.com/fpt/');
        $I->amOnPage('/');
        $I->fillField('#urlinput',self::$link_Homepage);
        $I->click('.large');
        $I->wait(20);
        //$I->canSeeInPageSource('div id="summary" style="display: block');
        if ($I->seeElementInDOM('#summary')) {
        //if ($I->see('div id="summary" style="display: block')) {
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        } else {
            #$I->waitForElementVisible('#summary',50);
            $I->wait(30);
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        }

        $dateTimeTested = date("m-d-Y H:i");
        $arr = explode(" ",$dateTimeTested);
        $dateTested = $arr[0];
        $I->LogResult('Homepage         > '.self::$link_Homepage,$pageResult,$dateTimeTested,$dateTested);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Category Level 1
        $I->wantToTest('Category 1 load time');
        $I->expectTo('see pingdom site testing the category 1 of emedco');
        $I->fillField('#urlinput',self::$link_Homepage.self::$link_Category1 );
        $I->click('.large');
        $I->wait(20);
        if ($I->seeElementInDOM('#summary')) {
        #if ($I->canSeeInPageSource('div id="summary" style="display: block')) {
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        } else {
            #$I->waitForElementVisible('#summary',50);
            $I->wait(30);
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        }

        $dateTimeTested = ' ';
        $I->LogResult('Category Level 1 > '.self::$link_Homepage.self::$link_Category1,$pageResult,$dateTimeTested,$dateTested);


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Category Level 2
        $I->wantToTest('Category 2 load time');
        $I->expectTo('see pingdom site testing the category 2 of emedco');
        $I->fillField('#urlinput',self::$link_Homepage.self::$link_Category2);
        $I->click('.large');
        $I->wait(20);
        if ($I->seeElementInDOM('#summary')) {
        #if ($I->canSeeInPageSource('div id="summary" style="display: block')) {
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        } else {
            #$I->waitForElementVisible('#summary',50);
            $I->wait(30);
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        }

        $dateTimeTested = ' ';
        $I->LogResult('Category Level 2 > '.self::$link_Homepage.self::$link_Category2 ,$pageResult,$dateTimeTested,$dateTested);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Category Level 3
        $I->wantToTest('Category 3 load time');
        $I->expectTo('see pingdom site testing the category 3 of emedco');
        $I->fillField('#urlinput',self::$link_Homepage.self::$link_Category3);
        $I->click('.large');
        $I->wait(20);
        if ($I->seeElementInDOM('#summary')) {
        #if ($I->canSeeInPageSource('div id="summary" style="display: block')) {
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        } else {
            #$I->waitForElementVisible('#summary',50);
            $I->wait(30);
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        }

        $dateTimeTested = ' ';
        $I->LogResult('Category Level 3 > '.self::$link_Homepage.self::$link_Category3 ,$pageResult,$dateTimeTested,$dateTested);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Product Page
        $I->wantToTest('Product page load time');
        $I->expectTo('see pingdom site testing the product page of emedco');
        $I->fillField('#urlinput',self::$link_Homepage.self::$link_ProductPage);
        $I->click('.large');
        $I->wait(20);
        if ($I->seeElementInDOM('#summary')) {
        #if ($I->see('div id="summary" style="display: block')) {
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        } else {
            #$I->waitForElementVisible('#summary',50);
            $I->wait(30);
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        }

        $dateTimeTested = ' ';
        $I->LogResult('Product page     > '.self::$link_Homepage.self::$link_ProductPage ,$pageResult,$dateTimeTested,$dateTested);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Cart Page
        $I->wantToTest('Cart page load time');
        $I->expectTo('see pingdom site testing the cart page of emedco');
        $I->fillField('#urlinput',self::$link_Homepage.self::$link_CartPage);
        $I->click('.large');
        $I->wait(20);
        if ($I->seeElementInDOM('#summary')) {
        #if ($I->canSeeInPageSource('div id="summary" style="display: block')) {
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        } else {
            #$I->waitForElementVisible('#summary',50);
            $I->wait(30);
            $pageResult = $I->grabTextFrom('#rt_sumright > dl:nth-child(2) > dd:nth-child(2)');
            $I->see($pageResult);
        }

        $dateTimeTested = ' ';
        $I->LogResult('Cart page        > '.self::$link_Homepage.self::$link_CartPage ,$pageResult,$dateTimeTested,$dateTested);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  }


}